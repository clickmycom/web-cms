<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class insert_model extends CI_Model{
		
	public function __construct() {
			parent::__construct();
	}

	public function form_insert($data){

			return $this->db->insert('article', $data);
			
	}
	
	public function update_article($data){
			$this->db->where('id',$this->session->userdata('id_article_edit'));
			return $this->db->update('article', $data);
	}
	
	public function update_slides($data){
			$this->db->where('id',$this->session->userdata('id_edit_slides'));
			return $this->db->update('cms_slide',$data);
	}
	
	public function update_category($data){
			$this->db->where('id',$this->session->userdata('id_edit_category'));
			return $this->db->update('category', $data);
	}
	
	
	public function insert_category(){
				
			if($this->input->post("number_page") == null){
					$number_page = null;			
			}else{
				 	$number_page = $this->input->post("number_page");
			}
			
			if($this->input->post('is_singpage') == "No"){
					$number_page = null;	
			}
			
			$data_post = array(
					'name' 				=> $this->input->post("name"),
					'is_menu' 			=> $this->input->post("is_menu"),
					'is_single_page'	=> $this->input->post("is_singpage"),
					'tooltip'			=> $this->input->post('title'),
					'article_id'		=> $number_page,
					'Link' 				=> $this->input->post("Link"),
					'order_no' 			=> $this->input->post("oder_no"),
					'status' 			=> $this->input->post("status")
								
			);
				
			
	return $this->db->insert('category', $data_post);
			

	}

	
	
	public function insert_sub_category(){
			
			if($this->input->post("number_page") == null){
					$number_page = null;			
			}else{
				 	$number_page = $this->input->post("number_page");
			}
			
			if($this->input->post('is_singpage') == "No"){
					$number_page = null;	
			}
			
			$data_post = array(
					'category_id' 		=> $this->input->post("category"),
					'name' 				=> $this->input->post("name"),
					'link' 				=> $this->input->post("Link"),
					'tooltip'					=> $this->input->post('title'),
					'is_single_page'	=> $this->input->post('is_singpage'),	
					'article_id'		=> $number_page,			
					'is_menu' 			=> $this->input->post("is_menu"),
					'order_no' 			=> $this->input->post("oder_no"),
					'status' 			=> $this->input->post("status")
								
			);
				
				
			return $this->db->insert('sub_category', $data_post);
			
	}
	
	
	
	
	
	public function delete_sub_category(){
		
		
		
		$id	= $this->session->userdata('delete_sub_category');
		
		$this->db->where('id', $id);
		if($this->db->delete('sub_category')){
		    return true;
		}else{				
			return false;		
		}
	}
	
	
	public function delete_category(){
		$id	= $this->session->userdata('delete_category');
		
		$this->db->where('id', $id);
		return $this->db->delete('category');

	}
	

	public function delete_article(){
		
		
		$id	= $this->session->userdata('id_del_article');
		$img = $this->session->userdata('id_del_img_article');
		
		
		$this->db->where('id', $id);
		if($this->db->delete('article')){
			$file = "../public/upload_images/".$img;
			unlink($file);
			
		    return true;
		}else{				
			return false;		
		}
	}


	public function delete_slide_img(){
		$id	= $this->session->userdata('delete_slide_id');
		$img = $this->session->userdata('delete_slide_img');
		
		
		$this->db->where('id', $id);
		if($this->db->delete('cms_slide')){
			$file = "../public/upload_slide_images/".$img;
			unlink($file);
			
		    return true;
		}else{				
			return false;		
		}
	}

	public function config($data){
			
		$this->db->where('id',1);
		return $this->db->update('layouts_config', $data);
		
	}
	public function insert_template($data){
		
		$data2 = array(
			   'name' => $data
		);
			
		return $this->db->insert('template',$data2);
	}
	
}





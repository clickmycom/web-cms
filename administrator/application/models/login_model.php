<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model{
		
	public function __construct() {
			parent::__construct();
	}

	public function form_insert($data){
		

			$this->db->insert('bk_admin', $data);
			
			
	}
	
	public function check_login($user,$pass){
	    $this->load->config('config', TRUE );
        $salt = $this->config->item('encryption_key', 'config');    
	    $pass = md5($pass.$salt);
        //echo '    '.$pass; exit();
		$this->db->where('status',1);
		$this->db->where('username', $user);
		$this->db->where('password', $pass);
		// $this->db->where('password', MD5($pass));
		
		$this-> db->limit(1);
		
		$query = $this->db->get('bk_admin');
		
		if($query -> num_rows() == 1)
	    {
	     return $query->result();
	    }

	}
	
	
	public function check_user($user){
		
		$this->db->where('username', $user);
		$this-> db->limit(1);
		$query = $this->db->get('bk_admin');
	
		return $query->result();
		
		
		
	}
	
	
}


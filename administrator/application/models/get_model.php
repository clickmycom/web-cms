<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Get_model extends CI_Model {
    
    public function __construct()
    {
        parent::__construct();
        
    }
	
	public function layout_config(){
		$query = $this->db->get('layouts_config');
        return $query->result_array();
	}
    
    public function get_template()
    {
        $query = $this->db->get('layouts_config');
        return $query->row();
    }
	
	public function get_img_template($name_template){
		$this->db->where('name',$name_template);
		$query = $this->db->get('template');
        return $query->result_array();
	}
	
	
	
	public function get_category(){
		$this->db->order_by('id', "asc");
		$this->db->where('status',1);
		
		
		$query = $this->db->get('category');
		return $query->result_array();
	}
	
	
	public function get_sub_category(){
		
		
		$this->db->where('status',1);
		// $this->db->where('is_menu',1);
		$this->db->order_by('order_no', "asc");
		$query = $this->db->get('sub_category');
		 
		return $query->result_array();
	}
	
	public function change_sub_category(){
		$is_id=$this->session->userdata('id_cate');
		$this->db->where('category_id',$is_id);
		$this->db->where('status',1);
		// $this->db->where('is_menu',1);
		$this->db->order_by('order_no', "asc");
		$query = $this->db->get('sub_category');
		return $query->result_array();
	}
	
	
	
	
	public function gender_enums($table , $field){	 
		
		 $row = $this->db->query("SHOW COLUMNS FROM ".$table." LIKE '$field'")->row()->Type;  
		 $regex = "/'(.*?)'/";
         preg_match_all( $regex , $row, $enum_array );
         $enum_fields = $enum_array[1];
		 
         foreach ($enum_fields as $key=>$value)
         {
            $enums[$value] = $value; 
         }
         return $enums;
	}
	
	
	
	public function get_article(){
		
		//$this->db->or_where('sub_category_id',null);
		$this->db->select('category.name as category_name,sub_category.name as sub_category_name,article.article_name,article.showdate,article.enddate,bk_admin.name as bk_admin_name,article.order_no,article.id,article.article_image,article.status');
		$this->db->join('category','category.id = article.category_id');	
		$this->db->join('sub_category','sub_category.id = article.sub_category_id');
		$this->db->join('bk_admin','bk_admin.id = article.bk_admin_id');
		
		
		$query = $this->db->get('article');
		
		return $query->result_array();
		
		
	}	


	public function get_sub_category_table(){
		$this->db->select('category.name as category_name,sub_category.name,sub_category.link,sub_category.is_menu,sub_category.order_no,sub_category.status,sub_category.id');
		$this->db->join('category','category.id = sub_category.category_id');
		$query = $this->db->get('sub_category');
		return $query->result_array();
	}
	
	
	public function get_category_table(){
		$this->db->order_by('id', "asc");	
		$query = $this->db->get('category');
		return $query->result_array();
	}

	
	public function get_slides(){
		$query = $this->db->get('cms_slide');
		return $query->result_array();
	}
	
	
	
	public function check_login($user,$pass){
		$this->db->where('status',1);
		$this->db->where('username', $user);
		$this->db->where('password', $pass);
		// $this->db->where('password', MD5($pass));
		
		$this-> db->limit(1);
		
		$query = $this->db->get('bk_admin');
		
		if($query -> num_rows() == 1)
	    {
	     return $query->result();
	    }

	}
	
	public function template(){
		
		$query = $this->db->get('template');
		
		return $query->result_array();
	}
	
	
	
	
	
}
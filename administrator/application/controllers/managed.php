<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class managed extends CI_Controller {
    
    public function __construct()
    {
        parent::__construct();
        $this->template 				= 'templates/'.$this->get_model->get_template()->administrator_template.'/index.php';
        $this->template_paht 			= base_url().'public/templates/'.$this->get_model->get_template()->administrator_template.'/';
        $this->title 					= $this->get_model->get_template()->site_name;
		$this->color_footer				= $this->get_model->get_template()->footer;
		$this->get_club_id				= $this->get_model->get_template()->club_id;
		$this->get_url_site				= "http://192.168.1.123/sas/";
		
   }

	public function index()
	{
		
		 if($this->session->userdata('logged_in')){
			$session_data = $this->session->userdata('logged_in'); 
			
			// Link
			$data['dashboard']				= base_url()."home";
			$data['add']					= base_url()."home/add";
			$data['view_table']				= base_url()."home/article_view";
			$data['view_Article']			= base_url()."home/Article";
			$data['view_Category']			= base_url()."home/Category";
			$data['Sub_Category_menu']		= base_url()."/home/Sub_Category";
			$data['add_sub_category']		= base_url()."home/add_sub_category";
			$data['add_category']			= base_url()."home/add_category";			
			// Link
			
			
			$data['Category']				= $this->get_model->get_category_table();
			$data['template_paht'] 			= $this->template_paht;
			$data['username'] 				= $session_data['username'];
			$data['shoping_mode']			= $this->get_model->gender_enums('layouts_config','shoping_mode');
			//$data['is_homepage_status']		= $thi	s->get_model->gender_enums('article','status');
			$data['template']				= $this->get_model->template();
			//$data['layout_config']			= $this->get_model->layout_config();
			$data['color_footer']			= $this->color_footer;
			$data['success']				= '';
			$data['content']                = '/templates/admin_two/view_managed';
            $data['pagetitle']              = 'Managed';
			$data['active_menu']			= 6;
			
			$data['get_club_id']			= $this->get_club_id;
			$data['url_site']				= $this->get_url_site;
	
			$this->load->view('layouts/layout.php',$data);
		}else{
			redirect('login', 'refresh');
		}     
	
	}

	public function add_presonal(){
		 if($this->session->userdata('logged_in')){
			$session_data = $this->session->userdata('logged_in'); 
			
			// Link
			$data['dashboard']				= base_url()."home";
			$data['add']					= base_url()."home/add";
			$data['view_table']				= base_url()."home/article_view";
			$data['view_Article']			= base_url()."home/Article";
			$data['view_Category']			= base_url()."home/Category";
			$data['Sub_Category_menu']		= base_url()."/home/Sub_Category";
			$data['add_sub_category']		= base_url()."home/add_sub_category";
			$data['add_category']			= base_url()."home/add_category";			
			// Link
			
			
			$data['Category']				= $this->get_model->get_category_table();
			$data['template_paht'] 			= $this->template_paht;
			$data['username'] 				= $session_data['username'];
			$data['shoping_mode']			= $this->get_model->gender_enums('layouts_config','shoping_mode');
			//$data['is_homepage_status']		= $this->get_model->gender_enums('article','status');
			$data['template']				= $this->get_model->template();
			//$data['layout_config']			= $this->get_model->layout_config();
			$data['color_footer']			= $this->color_footer;
			$data['success']				= '';
			$data['content']                = '/templates/admin_two/view_add_managed';
            $data['pagetitle']              = 'Add Presonal';
			$data['active_menu']			= 6;
			$data['get_club_id']			= $this->get_club_id;
			$data['url_site']				= $this->get_url_site;
	
			$this->load->view('layouts/layout.php',$data);
		}else{
			redirect('login', 'refresh');
		}
	}
	
	public function add_team(){
		 if($this->session->userdata('logged_in')){
			$session_data = $this->session->userdata('logged_in'); 
			
			// Link
			$data['dashboard']				= base_url()."home";
			$data['add']					= base_url()."home/add";
			$data['view_table']				= base_url()."home/article_view";
			$data['view_Article']			= base_url()."home/Article";
			$data['view_Category']			= base_url()."home/Category";
			$data['Sub_Category_menu']		= base_url()."/home/Sub_Category";
			$data['add_sub_category']		= base_url()."home/add_sub_category";
			$data['add_category']			= base_url()."home/add_category";			
			// Link
			
			
			$data['Category']				= $this->get_model->get_category_table();
			$data['template_paht'] 			= $this->template_paht;
			$data['username'] 				= $session_data['username'];
			$data['shoping_mode']			= $this->get_model->gender_enums('layouts_config','shoping_mode');
			//$data['is_homepage_status']		= $this->get_model->gender_enums('article','status');
			$data['template']				= $this->get_model->template();
			//$data['layout_config']			= $this->get_model->layout_config();
			$data['color_footer']			= $this->color_footer;
			$data['success']				= '';
			$data['content']                = '/templates/admin_two/view_managed_add_team';
            $data['pagetitle']              = 'Add Team';
			$data['active_menu']			= 6;
			$data['get_club_id']			= $this->get_club_id;
			$data['url_site']				= $this->get_url_site;
	
			$this->load->view('layouts/layout.php',$data);
		}else{
			redirect('login', 'refresh');
		}
	}


	public function team(){
		
		 if($this->session->userdata('logged_in')){
			$session_data = $this->session->userdata('logged_in'); 
			
			// Link
			$data['dashboard']				= base_url()."home";
			$data['add']					= base_url()."home/add";
			$data['view_table']				= base_url()."home/article_view";
			$data['view_Article']			= base_url()."home/Article";
			$data['view_Category']			= base_url()."home/Category";
			$data['Sub_Category_menu']		= base_url()."/home/Sub_Category";
			$data['add_sub_category']		= base_url()."home/add_sub_category";
			$data['add_category']			= base_url()."home/add_category";			
			// Link
			
			
			
			$data['template_paht'] 			= $this->template_paht;
			$data['username'] 				= $session_data['username'];
			
			//$data['is_homepage_status']		= $this->get_model->gender_enums('article','status');
			$data['template']				= $this->get_model->template();
			//$data['layout_config']			= $this->get_model->layout_config();
			$data['color_footer']			= $this->color_footer;
			$data['success']				= '';
			$data['content']                = '/templates/admin_two/view_table_team';
            $data['pagetitle']              = 'Add Team';
			$data['active_menu']			= 6;
			$data['get_club_id']			= $this->get_club_id;
			$data['url_site']				= $this->get_url_site;
	
			$this->load->view('layouts/layout.php',$data);
		}else{
			redirect('login', 'refresh');
		}
		
	}

	public function edit_team(){
		if(@$_POST){
		    $arr['id_edit_team'] 			= $this->input->post('id_edit_team');
			
			$this->session->set_userdata($arr);
		}
		
		
		
		
		
		
		
		
		$session_data = $this->session->userdata('logged_in'); 
        $data['template_paht']          = $this->template_paht;
        $data['title']                  = $this->title;         
       
        
        $data['is_menu']                = $this->get_model->gender_enums('sub_category','is_menu');
        $data['is_homepage']            = $this->get_model->gender_enums('article','is_homepage');            
        $data['is_homepage_status']     = $this->get_model->gender_enums('article','status');
        $data['username']               = $session_data['username'];
        $data['success']                = '';
        $data['active_menu']			= 6;
        // Link
        $data['dashboard']              = base_url()."home";
        $data['add']                    = base_url()."home/add";
        $data['view_table']             = base_url()."home/article_view";
        $data['view_Article']           = base_url()."home/Article";
        $data['view_Category']          = base_url()."home/Category";
        $data['Sub_Category_menu']      = base_url()."home/Sub_Category";
        $data['add_sub_category']       = base_url()."home/add_sub_category";
        $data['add_category']           = base_url()."home/add_category";           
        $data['cancel']                 = base_url()."home/Article";    
        // Link
        $data['get_club_id']			= $this->get_club_id;
        $data['content']                = '/templates/admin_two/view_edit_team';
        $data['pagetitle']               = 'Edit Team';
			$data['url_site']				= $this->get_url_site;
		
    	$this->load->view('layouts/layout.php',$data);
			
		
		
	}


	public function edit_personnel(){
		if(@$_POST){
		    $arr['id_edit_personnel'] 			= $this->input->post('id_edit_personnel');
			$this->session->set_userdata($arr);
		}
		
		
		$session_data = $this->session->userdata('logged_in'); 
        $data['template_paht']          = $this->template_paht;
        $data['title']                  = $this->title;         
       
        
        $data['is_menu']                = $this->get_model->gender_enums('sub_category','is_menu');
        $data['is_homepage']            = $this->get_model->gender_enums('article','is_homepage');            
        $data['is_homepage_status']     = $this->get_model->gender_enums('article','status');
        $data['username']               = $session_data['username'];
        $data['success']                = '';
        $data['active_menu']			= 6;
        // Link
        $data['dashboard']              = base_url()."home";
        $data['add']                    = base_url()."home/add";
        $data['view_table']             = base_url()."home/article_view";
        $data['view_Article']           = base_url()."home/Article";
        $data['view_Category']          = base_url()."home/Category";
        $data['Sub_Category_menu']      = base_url()."home/Sub_Category";
        $data['add_sub_category']       = base_url()."home/add_sub_category";
        $data['add_category']           = base_url()."home/add_category";           
        $data['cancel']                 = base_url()."home/Article";    
        // Link
        $data['get_club_id']			= $this->get_club_id;
        $data['content']                = '/templates/admin_two/view_edit_personnel';
        $data['pagetitle']               = 'Edit Personnel';
		$data['url_site']				= $this->get_url_site;
		
    	$this->load->view('layouts/layout.php',$data);
			
			
			
		
		
	}
	
	
	
	
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class login extends CI_Controller {
    
    public function __construct()
    {
        parent::__construct();
       	
       	$this->login 					= 'templates/'.$this->get_model->get_template()->administrator_template;
        $this->template_paht 			= base_url().'public/templates/'.$this->get_model->get_template()->administrator_template.'/';
        $this->title 					= $this->get_model->get_template()->site_name;
		
    }

	public function index()
	{
		
			$data['template_paht'] 			= $this->template_paht;
			$data['login_failed']			= TRUE;
			
			if($this->input->post("btn_login")!=null){
				$UserName					=	$this->input->post("user");
				$Pass						=   $this->input->post("password");
                //echo $Pass;
				$result	= $this->login_model->check_login($UserName,$Pass);

		
				if($result){
			     	foreach($result as $row){
			       		$sess_array = array(
			         			'id' => $row->id,
			         			'username' => $row->username
			       		);
			       $this->session->set_userdata('logged_in', $sess_array);
			     }
				 	redirect('home', 'refresh');
				 
				}else{					
					$data['login_failed']	= FALSE;		       	
			     	$this->load->view("/templates/admin_two/login",$data);
			    }									
			}else{
					$this->load->view("/templates/admin_two/login",$data);
			}// end if

		

	}
	
	public function signup(){
			$pattern 					= 	'/^[a-zA-Z]*$/';
			$pattern2 					= 	'/^[a-zA-Z0-9]*$/';
			$data['name']				=	'';
			$data['Surname']			=	'';
			$data['email']				=	'';
			$data['user']				=	'';
			$data['pass']				=	'';
			$data['cc_pass']			=	'';
			$data['user_double']		=	'';
			$data['check_match']		= 	'ture'; 
			$data['template_paht']		=	$this->template_paht;	
			
		    $this->load->config('config', TRUE );
            $salt = $this->config->item('encryption_key', 'config');
			
		if($this->input->post("btnsave")!=null){
		    
			$Name						=	$this->input->post("Name");
			$Surname					=	$this->input->post("Surname");
			$E_mail						=	$this->input->post("E_mail");		
			$UserName					=	$this->input->post("UserName");
			$password					=	$this->input->post("password");
			$cc_password				=	md5($this->input->post("cc_password").$salt);
			
			$data['name']				=	$Name;
			$data['Surname']			=	$Surname;
			$data['email']				=	$E_mail;
			$data['user']				=	$UserName;
			
			$this->user_double 		    = 	$this->login_model->check_user($UserName);
			$userdouble				    =	$this->user_double;			
			
			 if($userdouble == null){
						 	if(preg_match($pattern, $Name)&&preg_match($pattern2, $UserName)&&preg_match($pattern, $Surname)){
								
								$data = array(
									'name' 					=> $Name,
									'surname' 				=> $Surname,
									'username' 				=> $UserName,
									'password' 				=> $cc_password,
									'email' 				=> $E_mail,
									'status' 				=> 1
								
								);
			
								$result	= $this->login_model->form_insert($data);											
								$result2	= $this->login_model->check_login($UserName,$password);
							
								if($result2){
							     	foreach($result2 as $row){
							       		$sess_array = array(
							         			'id' => $row->id,
							         			'username' => $row->username
							       		);
							       	$this->session->set_userdata('logged_in', $sess_array);
							     	}
									
								 redirect('home', 'refresh');						 
								}
								
																							
							}else{
								$data['check_match']	= 	FALSE;							
							}	
			}else{
				
				$data['user']				=	'';
			 	$data['user_double']		=	'double';
				
				
				
				
			}
			
			
		}else{
			$data['check_match']	= 	true;
		}
		
		
		$this->load->view($this->login.'/signup',$data);
		
	} // end function signup
	
	


	public function logout(){
	   
	   $this->session->unset_userdata('logged_in');
	   session_destroy();
	   redirect('login', 'refresh');

	}
	
	
	public function check_user_double(){

		 $user_name	=	$this->input->post('name');
		
		 $this->login_model->check_user($user_name);
		 
		 
	
	}

	
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Home extends CI_Controller {
    
    public function __construct()
    {
        parent::__construct();
        $this->template 				= 'templates/'.$this->get_model->get_template()->administrator_template.'/index.php';
        $this->template_paht 			= base_url().'public/templates/'.$this->get_model->get_template()->administrator_template.'/';
        $this->title 					= $this->get_model->get_template()->site_name;
		
		$this->category					= $this->get_model->get_category();
		$this->get_sub_category			= $this->get_model->get_sub_category();
		//$this->load->helper(array('form', 'url'));		
    }

	public function index()
	{
		
		
		 if($this->session->userdata('logged_in')){
		 	
			$session_data = $this->session->userdata('logged_in'); 
			
		    $data['template_paht'] 			= $this->template_paht;
	        $data['title'] 					= $this->title;	    	
			$data['category']				= $this->category;
			$data['sub_category']			= $this->get_sub_category;	
			$data['is_menu']				= $this->get_model->gender_enums('sub_category','is_menu');
			$data['is_homepage']			= $this->get_model->gender_enums('article','is_homepage');			  
			$data['is_homepage_status']		= $this->get_model->gender_enums('article','status');
	 		$data['username'] 				= $session_data['username'];
		    
			
			// Link
			$data['dashboard']				= base_url()."home";
			$data['add']					= base_url()."home/add";
			$data['view_table']				= base_url()."home/article_view";
			$data['view_Article']			= base_url()."home/Article";
			$data['view_Category']			= base_url()."home/Category";
			$data['Sub_Category_menu']		= base_url()."home/Sub_Category";
			$data['add_sub_category']		= base_url()."home/add_sub_category";
			$data['add_category']			= base_url()."home/add_category";			
			// Link
			
			$data['active_menu']			= $this->router->class;
			$data['pagetitle'] 				= 'Home';
		    $data['content'] 				= 'index.php';
			$data['active_menu']			= 1;
			
		    $this->load->view('layouts/layout.php',$data);
	
		 }
		 else{
		     redirect('login', 'refresh');
		 }
		 
		 
		 
   
  
	}
	
	
	public function add(){
		
		if($this->session->userdata('logged_in')){
		 	
			$session_data = $this->session->userdata('logged_in'); 
		    $data['template_paht'] 			= $this->template_paht;
	        $data['title'] 					= $this->title;	    	
			$data['category']				= $this->category;	
			$data['sub_category']			= $this->get_sub_category;	
			$data['is_menu']				= $this->get_model->gender_enums('sub_category','is_menu');
			$data['is_homepage']			= $this->get_model->gender_enums('article','is_homepage');			  
			$data['is_homepage_status']		= $this->get_model->gender_enums('article','status');
	 		$data['username'] 				= $session_data['username'];
		    $data['success']				= '';
			
			// Link
			$data['dashboard']				= base_url()."home";
			$data['add']					= base_url().$this->router->class."/add";
			$data['view_table']				= base_url().$this->router->class."/article_view";
			$data['view_Article']			= base_url().$this->router->class."/Article";
			$data['view_Category']			= base_url().$this->router->class."/Category";
			$data['Sub_Category_menu']		= base_url().$this->router->class."/Sub_Category";
			$data['add_sub_category']		= base_url().$this->router->class."/add_sub_category";
			$data['add_category']			= base_url().$this->router->class."/add_category";			
			// Link
		
			$data['active_menu']			= 2;
		    
		   //$this->load->view("/templates/admin_two/add",$data);
           $data['content']                = '/templates/admin_two/add';
           $data['pagetitle']               = 'Add Article';
            
           $this->load->view('layouts/layout.php',$data);
		  
		   
		 }
		 else{
		     redirect('login', 'refresh');
		 }
	}
	
	
	
	public function Article(){
				
		 if($this->session->userdata('logged_in')){
			$session_data = $this->session->userdata('logged_in'); 
			$data['article']				= $this->get_model->get_article();		
			
			// Link
			$data['dashboard']				= base_url()."home";
			$data['add']					= base_url()."home/add";
			$data['view_table']				= base_url()."home/article_view";
			$data['view_Article']			= base_url()."home/Article";
			$data['view_Category']			= base_url()."home/Category";
			$data['Sub_Category_menu']		= base_url()."home/Sub_Category";
			$data['add_sub_category']		= base_url()."home/add_sub_category";
			$data['add_category']			= base_url()."home/add_category";			
			// Link
					
			$data['template_paht'] 			= $this->template_paht;
			$data['username'] 				= $session_data['username'];
			
			$data['content']                = '/templates/admin_two/article_view';
            $data['pagetitle']               ='Article';
            $data['active_menu']			= 2;
			
            $this->load->view('layouts/layout.php',$data);
			
		 }
		 else{
		    
		     redirect('login', 'refresh');
		 }
	
	}

	public function Category(){
			$session_data = $this->session->userdata('logged_in'); 
			
			// Link
			$data['dashboard']				= base_url()."home";
			$data['add']					= base_url()."home/add";
			$data['view_table']				= base_url()."home/article_view";
			$data['view_Article']			= base_url()."home/Article";
			$data['view_Category']			= base_url()."home/Category";
			$data['Sub_Category_menu']		= base_url()."/home/Sub_Category";
			$data['add_sub_category']		= base_url()."home/add_sub_category";
			$data['add_category']			= base_url()."home/add_category";			
			// Link
			
			$data['active_menu']			= 2;
			$data['Category']				= $this->get_model->get_category_table();
			$data['template_paht'] 			= $this->template_paht;
			$data['username'] 				= $session_data['username'];
			
			
			$data['content']                = '/templates/admin_two/category_view';
            $data['pagetitle']               ='Category';
             
            $this->load->view('layouts/layout.php',$data);
			
		   
		    
			// $this->load->view("/templates/admin_two/category_view",$data);
			
			
	}
	
	public function Sub_Category(){
			$session_data = $this->session->userdata('logged_in'); 
			
		// Link
			$data['dashboard']				= base_url()."home";
			$data['add']					= base_url()."home/add";
			$data['view_table']				= base_url()."home/article_view";
			$data['view_Article']			= base_url()."home/Article";
			$data['view_Category']			= base_url()."home/Category";
			$data['Sub_Category_menu']		= base_url()."home/Sub_Category";
			$data['add_sub_category']		= base_url()."home/add_sub_category";
			$data['add_category']			= base_url()."home/add_category";			
			// Link
			
			$data['Sub_Category']			= $this->get_model->get_sub_category_table();
			$data['template_paht'] 			= $this->template_paht;
			$data['username'] 				= $session_data['username'];
		
			
			$data['content']                = '/templates/admin_two/sub_category_view';
            $data['pagetitle']               = 'Sub Category';
            $data['active_menu']			= 2;
           $this->load->view('layouts/layout.php',$data);
           
			
	}
	

	public function onchang_category(){
		if(@$_POST){
		    $arr['id_cate']             = $this->input->post('id_detail');
			$this->session->set_userdata($arr);
		}
			
			$this->change_sub			= $this->get_model->change_sub_category();
			
			$data['change_sub']			= $this->change_sub;
			
			if($this->change_sub == null){
				
			}else{
				?>
				<div class="controls">
			                            <div class="btn-group load_sub">                                             	
					                         <select name="sub_category" required>
					                             <option value="">Select-Sub Category</option>
				<?
					foreach ($data['change_sub'] as $value) {
				?>							
												<option value="<?=$value['id']?>"><?=$value['name']?></option>			 											
				<?
					}
				?>							      
												</select>										
			                            </div>
		         </div>	<!-- /controls -->	
							
				<?			
			}
			
			
			
			

	}
	

	public function save_userdata(){
		
	
			
			
			$session_data = $this->session->userdata('logged_in'); 
		    $data['template_paht'] 			= $this->template_paht;
	        $data['title'] 					= $this->title;	    	
			$data['category']				= $this->category;
			$data['sub_category']			= $this->get_sub_category;	
			$data['is_menu']				= $this->get_model->gender_enums('sub_category','is_menu');
			$data['is_homepage']			= $this->get_model->gender_enums('article','is_homepage');			  
			$data['is_homepage_status']		= $this->get_model->gender_enums('article','status');
	 		$data['username'] 				= $session_data['username'];
		    
			// Link
			$data['dashboard']				= base_url()."home";
			$data['add']					= base_url()."home/add";
			$data['view_table']				= base_url()."home/article_view";
			$data['view_Article']			= base_url()."home/Article";
			$data['view_Category']			= base_url()."home/Category";
			$data['Sub_Category_menu']		= base_url()."home/Sub_Category";
			$data['add_sub_category']		= base_url()."home/add_sub_category";
			$data['add_category']			= base_url()."home/add_category";			
			// Link
			
			
			$data['content']                = '/templates/admin_two/add';
            $data['pagetitle']               ='Add Article';
     	
			$data['active_menu']			= 2;
			
           
			
			if($sessce == TRUE){
					$data['success']				= TRUE;
			
			}
			
			$this->load->view('layouts/layout.php',$data);
			//redirect('home', 'refresh');

		
	}

	public function save_article(){
		
		$session_data = $this->session->userdata('logged_in'); 
		
	
		if(@$_POST){
			//Configure
			//set the path where the files uploaded will be copied. NOTE if using linux, set the folder to permission 777
			$config['upload_path'] = "../public/upload_images"; 
			
	    	// set the filter image types
			$config['allowed_types'] = 'gif|jpg|png';
			
			//load the upload library
			$this->load->library('upload', $config);
	    	
	    	$this->upload->initialize($config);
	    	
	    	$this->upload->set_allowed_types('*');
			
			$data['upload_data'] = '';
	        
			//if not successful, set the error message
			if (!$this->upload->do_upload('userfile')) {
				 $this->upload->display_errors();
				 $img = null;				
			} else { //else, set the success message
				 
				 $data=$this->upload->data();	
			     $img=$data['file_name'];		
				 
			}
			
			//print_r($_POST);
			// echo $this->input->post("name");
			// echo "<br>";
			// echo $this->input->post('comment');
			//echo "<br>".$img;
			//exit;

			
			$data = array(
				'category_id' 				=> $this->input->post("category"),
				'sub_category_id' 			=> $this->input->post("sub_category"),
				'article_image' 			=> $img,
				'article_name' 				=> $this->input->post("name"),
				'article_detail' 			=> $this->input->post('comment'),
				'is_homepage' 				=> $this->input->post("is_homepage"),
				'tooltip'					=> $this->input->post('title'),
				'hitz'						=> '0',
				'showdate' 					=> $this->input->post("show_date"),
				'enddate' 					=> $this->input->post("end_date"),
				'create_date' 				=> date('Y-m-d H:i:s'),
				'bk_admin_id'				=> $session_data['id'],
				'order_no' 					=> $this->input->post("oder_no"),
				'status' 					=> $this->input->post('status')
			);
			
			
			
			$sessce	=	$this->insert_model->form_insert($data);
			($sessce==TRUE?$opt='YES':$opt=$this->db->error()['code']);
			
			$arr['save_succeed']               = $opt;
			$this->session->set_userdata($arr);
				redirect('home/add', 'refresh');		
	       // echo "$opt";
		
				
		}
			
	
	}

	public function logout(){
	   
	   $this->session->unset_userdata('logged_in');
	   session_destroy();
	   redirect('login', 'refresh');
	
	}
	
	public function add_category(){
			
			$session_data = $this->session->userdata('logged_in'); 
			
			if($this->session->userdata('id_copy_article')!=null){
				$data['id_copy_article']	= $this->session->userdata('id_copy_article');
			}else{
				$data['id_copy_article']	= null;
			}
			
			
		    $data['template_paht'] 			= $this->template_paht;
	        $data['title'] 					= $this->title;	    	
	        
			$data['is_single_page']			= $this->get_model->gender_enums('category','is_single_page');	
			$data['is_menu']				= $this->get_model->gender_enums('category','is_menu');
			$data['is_homepage_status']		= $this->get_model->gender_enums('category','status');
			
	 		$data['username'] 				= $session_data['username'];
		    $data['success']				= '';
			$data['active_menu']			= 2;
			// Link
			$data['dashboard']				= base_url()."home";
			$data['add']					= base_url()."home/add";
			$data['view_table']				= base_url()."home/article_view";
			$data['view_Article']			= base_url()."home/Article";
			$data['view_Category']			= base_url()."home/Category";
			$data['Sub_Category_menu']		= base_url()."home/Sub_Category";
			$data['add_sub_category']		= base_url()."home/add_sub_category";
			$data['add_category']			= base_url()."home/add_category";
			$data['cancel']					= base_url()."home/Category";			
			// Link
			
			$data['content']                = '/templates/admin_two/add_category';
            $data['pagetitle']              = 'Add Category';
			
			
			$this->load->view('layouts/layout.php',$data);
		
		    
		   
	}

		public function save_category(){
			if(@$_POST){
				 $sessce = $this->insert_model->insert_category();
	             ($sessce==TRUE?$opt='YES':$opt=$this->db->error()['code']);
	             echo json_encode(array("opt"=>"$opt"));
			}
		}


	
	
		public function add_sub_category(){
			
			if($this->session->userdata('id_copy_article')!=null){
				$data['id_copy_article']	= $this->session->userdata('id_copy_article');
			}else{
				$data['id_copy_article']	= null;
			}

			$session_data = $this->session->userdata('logged_in'); 
		    $data['template_paht'] 			= $this->template_paht;
	        $data['title'] 					= $this->title;	    	
			$data['active_menu']			= 2;
			
			$data['is_single_page']			= $this->get_model->gender_enums('sub_category','is_single_page');
			$data['is_menu']				= $this->get_model->gender_enums('sub_category','is_menu');
			$data['is_homepage_status']		= $this->get_model->gender_enums('sub_category','status');
	 		$data['username'] 				= $session_data['username'];
		    $data['success']				= '';
			$data['category']				= $this->category;
			// Link
			$data['dashboard']				= base_url()."home";
			$data['add']					= base_url()."home/add";
			$data['view_table']				= base_url()."home/article_view";
			$data['view_Article']			= base_url()."home/Article";
			$data['view_Category']			= base_url()."home/Category";
			$data['Sub_Category_menu']		= base_url()."home/Sub_Category";
			$data['add_sub_category']		= base_url()."home/add_sub_category";
			$data['add_category']			= base_url()."home/add_category";
			$data['cancel']					= base_url()."home/Sub_Category";				
			// Link
			
			if($this->input->post("btnsave")!=null){
				
				$data_post = array(
					'category_id' 		=> $this->input->post("category"),
					'name' 				=> $this->input->post("name"),
					'link' 				=> $this->input->post("Link"),
					'is_menu' 			=> $this->input->post("is_menu"),
					'order_no' 			=> $this->input->post("oder_no"),
					'status' 			=> $this->input->post("status")
								
				);
				
				$sessce	=	$this->insert_model->insert_sub_category($data_post);
				if($sessce == TRUE){
					$data['success']				= TRUE;
				}
			   $this->load->view("/templates/admin_two/add_sub_category",$data);
			}else{
				$this->load->view("/templates/admin_two/add_sub_category",$data);
			} 
		    
		   
	}

	public function save_subcategory(){
		if(@$_POST){
			
			 $sessce = $this->insert_model->insert_sub_category();
             ($sessce==TRUE?$opt='YES':$opt=$this->db->error()['code']);
             echo json_encode(array("opt"=>"$opt"));
		}
		
	}





	public function article_edit(){
	    
	   
	   
		if(@$_POST){
		    $arr['id_article_edit']               = $this->input->post('value');
			$this->session->set_userdata($arr);
		}

		if($this->session->userdata('id_article_edit')!=null){
			
			$id 	=	$this->session->userdata('id_article_edit');
            $sql 	=	"Select * from article where id='$id'";
            $rs		=	$this->db->query($sql);
            
            if($rs->num_rows()==0){
                $data['rs']=array();
            }
            else{
                $data['rs']=$rs->row_array();

            }
        
            $session_data = $this->session->userdata('logged_in'); 
            $data['template_paht']          = $this->template_paht;
            $data['title']                  = $this->title;         
            $data['category']               = $this->category;  
            $data['sub_category']           = $this->get_sub_category;  
            $data['is_menu']                = $this->get_model->gender_enums('sub_category','is_menu');
            $data['is_homepage']            = $this->get_model->gender_enums('article','is_homepage');            
            $data['is_homepage_status']     = $this->get_model->gender_enums('article','status');
            $data['username']               = $session_data['username'];
            $data['success']                = '';
            $data['active_menu']			= 2;
            // Link
            $data['dashboard']              = base_url()."home";
            $data['add']                    = base_url()."home/add";
            $data['view_table']             = base_url()."home/article_view";
            $data['view_Article']           = base_url()."home/Article";
            $data['view_Category']          = base_url()."home/Category";
            $data['Sub_Category_menu']      = base_url()."home/Sub_Category";
            $data['add_sub_category']       = base_url()."home/add_sub_category";
            $data['add_category']           = base_url()."home/add_category";           
            $data['cancel']                 = base_url()."home/Article";    
            // Link
            
            $data['content']                = '/templates/admin_two/edit_article';
            $data['pagetitle']               = 'Edit Article';
			
        	$this->load->view('layouts/layout.php',$data);
			
		}else{
			$this->Article();
		}
		

		
	}

	public function update_edit_article(){
		$session_data = $this->session->userdata('logged_in'); 
		
		
	
		
		// $this->input->post("name");
		// $this->input->post("comment");
		// $this->input->post("category");
		// $this->input->post("sub_category");
		// $this->input->post("is_homepage");
		// $this->input->post("show_date");
		// $this->input->post("end_date");
		// $this->input->post("status");

			//Configure
			//set the path where the files uploaded will be copied. NOTE if using linux, set the folder to permission 777
			$config['upload_path'] = "../public/upload_images"; 
			
	    	// set the filter image types
			$config['allowed_types'] = 'gif|jpg|png';
			
			//load the upload library
			$this->load->library('upload', $config);
	    
	    	$this->upload->initialize($config);
	    	
	    	$this->upload->set_allowed_types('*');
	
			$data['upload_data'] = '';
		
			$img;
	
			//if not successful, set the error message
			if (!$this->upload->do_upload('userfile')) {
				//$this->upload->display_errors();
				$img = $this->input->post('old_img');
				
				
	
			} else { //else, set the success message
				$data=$this->upload->data();	
			    $img=$data['file_name'];			
			}
			
		
			if(@$_POST){
				$data_update = array(
					'category_id' 				=> $this->input->post("category"),
					'sub_category_id' 			=> $this->input->post("sub_category"),
					'article_image' 			=> $img,
					'article_name' 				=> $this->input->post("name"),
					'article_detail' 			=> $this->input->post("comment"),
					'tooltip'					=> $this->input->post('title'),
					'is_homepage' 				=> $this->input->post("is_homepage"),
					'hitz' 						=> '',
					'showdate' 					=> $this->input->post("show_date"),
					'enddate' 					=> $this->input->post("end_date"),
					'create_date' 				=> date('Y-m-d'),
					'bk_admin_id' 				=> $session_data['id'],
					'order_no' 					=> $this->input->post('oder_no'),
					'status' 					=> $this->input->post("status")
								
				);
				
				
				
				$sessce	=	$this->insert_model->update_article($data_update);
				($sessce==TRUE?$opt='YES':$opt=$this->db->error()['code']);
		        
				
				$arr['save_succeed_ud_article']               = $opt;
				$this->session->set_userdata($arr);
				
				
				
			// $data['content']                = '/templates/admin_two/add';
            // $data['pagetitle']               ='Add Article';
//              
            // $this->load->view('layouts/layout.php',$data);		
				
					
			    redirect('/home/Article', 'refresh');
			    
			}
	}


	public function delete_img(){
			
			//echo $this->input->post('id_article');
			$name_img = $this->input->post('name_img');
			
			$data = Array(
					'article_image' =>	null
			);
			
			$sessce = $this->insert_model->update_article($data);
			($sessce==TRUE?$opt='YES':$opt=$this->db->error()['code']);
		    
			$path = "../public/upload_images/$name_img";  
			
			if(@unlink($path)){
				$arr = Array(
					"opt"=>"$opt"
				);
				echo json_encode($arr);
			}else{
				$arr = Array(
					"opt"=>"ไม่พบไฟล์ภาพ"
				);
				echo json_encode($arr);
				
			}
			
			
	}

	public function del_article(){
		// $this->db->delete('article',array('id' => $id));
		// $this->Article();	
		
		
		
		
		if(@$_POST){
		    $arr['id_del_article']               	= $this->input->post('id');
			$arr['id_del_img_article']              = $this->input->post('id_img');
			$this->session->set_userdata($arr);
		}
		
		// echo $this->session->userdata('id_del_article');
		// echo $this->session->userdata('id_del_img_article');
		
		
		$succ = $this->insert_model->delete_article();
		if($succ)
        {
           	echo "TRUE";
           	
        }else
        {
            echo "FALSE";
        }
		
		
			
	}
	
	
	
	
	public function edit_category(){
		if(@$_POST){
		    $arr['id_edit_category']               = $this->input->post('id_category');
			$this->session->set_userdata($arr);
		}
		
		if($this->session->userdata('id_copy_article')!=null){
				$data['id_copy_article']	= $this->session->userdata('id_copy_article');
		}else{
				$data['id_copy_article']	= null;
		}
			
		if($this->session->userdata('id_edit_category')!=null){
			$session_data = $this->session->userdata('logged_in');

		    $data['template_paht'] 			= $this->template_paht;
	        $data['title'] 					= $this->title;	    	
			$data['is_single_page']			= $this->get_model->gender_enums('category','is_single_page');
			$data['is_menu']				= $this->get_model->gender_enums('category','is_menu');
			$data['is_homepage_status']		= $this->get_model->gender_enums('category','status');
	 		$data['username'] 				= $session_data['username'];
		    $data['success']				= '';
			$data['active_menu']			= 2;
			
			// Link
			$data['dashboard']				= base_url()."home";
			$data['add']					= base_url()."home/add";
			$data['view_table']				= base_url()."home/article_view";
			$data['view_Article']			= base_url()."home/Article";
			$data['view_Category']			= base_url()."home/Category";
			$data['Sub_Category_menu']		= base_url()."home/Sub_Category";
			$data['add_sub_category']		= base_url()."home/add_sub_category";
			$data['add_category']			= base_url()."home/add_category";	
			$data['cancel']					= base_url()."home/Category";			
			// Link
			
			$data['content']                = '/templates/admin_two/edit_category';
            $data['pagetitle']               = 'Edit Category';
			
			$id 	=	$this->session->userdata('id_edit_category');
			$sql = "Select *from category where id='$id'";
			$rs=$this->db->query($sql);
			
			if($rs->num_rows()==0){
				$data['rs']=array();
			}
			else{
				$data['rs']=$rs->row_array();	
			}
			
			$this->load->view('layouts/layout.php',$data);
			
		}else{
			$this->Category();
		}   
				
	
	}
	
	public function update_edit_category(){
			if(@$_POST){
				
				
				if($this->input->post("number_page") == null){
					$number_page = null;			
				}else{
					$number_page = $this->input->post("number_page");
				}
				if($this->input->post('is_singpage') == "No"){
					$number_page = null;	
				}
				
				
				$data_update = array(
					'name' 				=> $this->input->post("name"),
					'is_menu' 			=> $this->input->post("is_menu"),
					'is_single_page'	=> $this->input->post('is_singpage'),
					'tooltip'					=> $this->input->post('title'),
					'article_id'		=> $number_page,
					'Link' 				=> $this->input->post("Link"),
					'order_no' 			=> $this->input->post("oder_no"),
					'status' 			=> $this->input->post("status")
				);
				
				$this->db->where('id',$this->session->userdata('id_edit_category'));				
				$sessce = $this->db->update('category	', $data_update);
				
				($sessce==TRUE?$opt='YES':$opt=$this->db->error()['code']);
		         echo json_encode(array("opt"=>"$opt"));
				
			    
			    
			}
	}

	public function del_category(){
		
		
		if(@$_POST){
		    $arr['delete_category']               = $this->input->post('id_del_category');
			$this->session->set_userdata($arr);
		}

		$succ = $this->insert_model->delete_category();
		if($succ)
        {
           	echo "TRUE"; 	
        }else
        {
            echo  $this->db->error()['code'];
        }



			
	}
	


	
	
	
	public function edit_sub_category(){
		
		if(@$_POST){
		    $arr['id_sub_category']               = $this->input->post('id_sub_category');
			$this->session->set_userdata($arr);
		}
			
		if($this->session->userdata('id_sub_category')!=null){
			$session_data = $this->session->userdata('logged_in'); 
			
			// $arr['idedit_sub_category']     = $id;
			// $this->session->set_userdata($arr);
			
		    $data['template_paht'] 			= $this->template_paht;
	        $data['title'] 					= $this->title;	    	

			$data['is_single_page']			= $this->get_model->gender_enums('sub_category','is_single_page');
			$data['is_menu']				= $this->get_model->gender_enums('sub_category','is_menu');
			$data['is_homepage_status']		= $this->get_model->gender_enums('sub_category','status');
	 		$data['username'] 				= $session_data['username'];
		    $data['success']				= '';
			$data['category']				= $this->category;
			// Link
			$data['dashboard']				= base_url()."home";
			$data['add']					= base_url()."home/add";
			$data['view_table']				= base_url()."home/article_view";
			$data['view_Article']			= base_url()."home/Article";
			$data['view_Category']			= base_url()."home/Category";
			$data['Sub_Category_menu']		= base_url()."home/Sub_Category";
			$data['add_sub_category']		= base_url()."home/add_sub_category";
			$data['add_category']			= base_url()."home/add_category";		
			$data['cancel']					= base_url()."home/Sub_Category";		
			// Link
			
			$data['active_menu']			= 2;
			$data['content']                = '/templates/admin_two/edit_sub_category';
            $data['pagetitle']               = 'Sub Category';
            
         
		   
		    $id 	=	$this->session->userdata('id_sub_category');
			$sql 	= 	"Select *from sub_category where id='$id'";
			$rs=$this->db->query($sql);
			
			if($rs->num_rows()==0){
				$data['rs']=array();
			}
			else{
				$data['rs']=$rs->row_array();	
			}
			
			$this->load->view('layouts/layout.php',$data);
			
		}
		else{
			$this->Sub_Category();
		}	
			
			//$this->load->view("/templates/admin_two/edit_sub_category.php",$data);
			
				
	}

	public function update_edit_sub_category(){
		
			if(@$_POST){
				// echo $this->input->post("category");
				// echo $this->input->post("name");
				// echo $this->input->post("Link");
				// echo $this->input->post("is_menu");
				// echo $this->input->post("oder_no");
				// echo $this->input->post("status");
				
				
				
				if($this->input->post("number_page") == null){
					$number_page = null;			
				}else{
					$number_page = $this->input->post("number_page");
				}
				if($this->input->post('is_singpage') == "No"){
					$number_page = null;	
				}
				
				
				
				$data_update = array(
					'category_id' 		=> $this->input->post("category"),
					'name' 				=> $this->input->post("name"),
					'link' 				=> $this->input->post("Link"),
					'is_single_page'	=> $this->input->post("is_singpage"),
					'tooltip'			=> $this->input->post('title'),
					'article_id'		=> $number_page,
					'is_menu' 			=> $this->input->post("is_menu"),
					'order_no' 			=> $this->input->post("oder_no"),
					'status' 			=> $this->input->post("status")
								
				);
				
				$this->db->where('id',$this->session->userdata('id_sub_category'));
				$sessce = $this->db->update('sub_category', $data_update);
				($sessce==TRUE?$opt='YES':$opt=$this->db->error()['code']);
		        echo json_encode(array("opt"=>"$opt"));
			    
			}
	}
	
	public function del_sub_category(){
		
		if(@$_POST){
		    $arr['delete_sub_category']               = $this->input->post('id_del');
			$this->session->set_userdata($arr);
		}
		
		$succ = $this->insert_model->delete_sub_category();
		if($succ)
        {
           	echo "TRUE"; 	
        }else
        {
            echo  $this->db->error()['code'];
        }
		 
	}
	public function add_template(){
		
		
		$config['upload_path'] = '../public/upload_templates/';
		$config['allowed_types'] = 'zip';
		$this -> load -> library('upload', $config);
		$this->upload->initialize($config);
		$this->upload->set_allowed_types('*');
		$data['upload_data'] = '';
		$file_name = '';
		
		$file_folder_name = null;
		if(!$this -> upload -> do_upload('file1')){
			//print_r($error = array('error' => $this -> upload -> display_errors()));
			//$this -> load -> view('upload_form_view', $error);
		}else{
			$upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
			$file_folder_name = $upload_data['file_name'];
			$new_file_name = explode(".",$file_folder_name);
			
			$dir = "../public/templates/$file_name";
			$dirNew  = "../application/views/templates/$file_name";
			
			$data = array('upload_data' => $this -> upload -> data());
			$zip = new ZipArchive;
			$file = $data['upload_data']['full_path'];
			
			chmod($file, 0777);
			if ($zip -> open($file) === TRUE) {
				//$zip -> extractTo('../public/templates/');
				$zip -> extractTo("../public/templates/$new_file_name[0]");
				$zip -> close();
				//mkdir($dir,0777,TRUE);
				echo 'ss';
				
			}else{
				//echo 'ผมรับแต่ไฟล์นามสกุล zip นะครับ';
				echo 1;
				return false;
			}
		}
		
		
		
		
		// move files
		
		$aryChkFiles 				= 	array('category_view','detail_view','index','category_single_view');
		$aryChkFilesLayout 			= 	array('copyright_view','footer_view','layout','menu_view');
		$aryChkFilesImport_files	=	array('category','detail','index');
		$aryChkFileThumbnail		=	array('template_article','template_category','template_copyright','template_main','template_menu');
		
		$aryChkFilesAmt 			= 	count($aryChkFiles);
		$countFileLayout			=	count($aryChkFilesLayout);
		$countFileInport_files		=	count($aryChkFilesImport_files);
		
		
		$new_file_name = explode(".",$file_folder_name);
		
		
		//******************//
		// Start Import_file
		//******************//
		$pathUp_layout_import_file			=	"../public/templates/$new_file_name[0]/page/layout/import_files/";
		$pathUp_layout_Paste_import_file	=	"../application/views/templates/$new_file_name[0]/layout/import_files";
		$aryFilesName	=	array();
		
		//echo $pathUp_layout_import_file;
		 //echo 'ok;;';return false;
		 
		if(!@opendir($pathUp_layout_import_file)){
			//echo 'ไม่พบ Folder import_files';
			echo 2;	
			return false;
		}else{
			$import_file_f 	= opendir($pathUp_layout_import_file);
			//echo "เจอไฟล์ import";
		}
	
				
		//*$import_file_f 	= opendir($pathUp_layout_import_file);
		
		$aryFilesName_import_file	=	array();
		//echo $import_file_f ? 'open':'not open';
		//return false;	
		
		while (false !== ($entry = readdir($import_file_f))) {
				//echo $entry."<br>";
				
				if ($entry != "." && $entry != "..") {
			
					$file_name = explode('.',$entry);
					if(isset($file_name[0]) && in_array($file_name[0],$aryChkFilesImport_files)){
						$aryFilesName[] = $entry;
						$countFileInport_files--;
					}
				}
				

				if($entry == "category.php"){
					echo 10;
				}
				if($entry == "detail.php"){
					echo 11;
				}
				if($entry == "index.php"){
					echo 12;
				}
				
		}
		
		
		if($countFileInport_files==0){
			mkdir("../application/views/templates/$new_file_name[0]/layout/import_files",0777,TRUE);
			foreach($aryFilesName as $item_file){
				$success_import = 0;
				copy($pathUp_layout_import_file.$item_file,$pathUp_layout_Paste_import_file.'/'.$item_file);
				$success_import = 1;
			}	
		}else{
			//echo 'Please rechecking files all';
			return false;
			//redirect('home', 'refresh');
			
		}
		
		//******************//
		// End Import_file
		//******************//
		
		
		
		
		//******************//
		// Start Layout
		//******************//
		$pathUp_layout			=	"../public/templates/$new_file_name[0]/page/layout/";
		$pathUp_layout_Paste	=	"../application/views/templates/$new_file_name[0]/layout";
		$aryFilesName_layout	=	array();
		
		
		if(!@opendir($pathUp_layout)){
			//echo 'ไม่พบ Folder layout';
			echo 3;	
			return false;
		}else{
			$import_file_layout_f 	= opendir($pathUp_layout);
		}
		
		//$import_file_layout_f 	= opendir($pathUp_layout);
		$aryFilesName_import_file	=	array();
		
		
		while (false !== ($entry_layout = readdir($import_file_layout_f))) {
				//echo $entry_layout."<br>";
				if ($entry_layout != "." && $entry_layout != "..") {
					$file_name = explode('.',$entry_layout);
					if(isset($file_name[0]) && in_array($file_name[0],$aryChkFilesLayout)){
						$aryFilesName_layout[] = $entry_layout;
						$countFileLayout--;
					}
				}
				
				if($entry_layout == "copyright_view.php"){
					echo 13;
				}
				if($entry_layout == "footer_view.php"){
					echo 14;
				}
				if($entry_layout == "layout.php"){
					echo 15;
				}
				if($entry_layout == "menu_view.php"){
					echo 16;
				}
		}
		
		if($countFileLayout==0){
			$success_layout = 0;
			foreach($aryFilesName_layout as $item_file){
				copy($pathUp_layout.$item_file,$pathUp_layout_Paste.'/'.$item_file);
			}
			$success_layout = 1;			
		}else{
			//echo 'Please rechecking files all';
			return false;
		}
		
		//******************//
		// End Layout
		//******************//
		
		
	
		
		//******************//
		// Start Site
		//******************//
		
		$import_file_site			=	"../public/templates/$new_file_name[0]/page/site/";
		$export_file_site			=	"../application/views/templates/$new_file_name[0]/site";
		$array_file_site			=	array();
		
		
		if(!@opendir($import_file_site)){
			//echo 'ไม่พบ Folder site';
			echo 4;	
			return false;
		}else{
			$read_file_site 			= 	opendir($import_file_site);
		}
		

		while (false !== ($entry_site = readdir($read_file_site))){
				//echo $entry_site."<br>";
				if ($entry_site != "." && $entry_site != "..") {
					$file_name = explode('.',$entry_site);
					if(isset($file_name[0]) && in_array($file_name[0],$aryChkFiles)){
						$array_file_site[] = $entry_site;
						$aryChkFilesAmt--;
					}
				}
				
				if($entry_site == "category_view.php"){
					echo 17;
				}
				
				if($entry_site == "detail_view.php"){
					echo 19;
				}
				
				if($entry_site == "index.php"){
					echo 21;
				}
				if($entry_site == "category_single_view.php"){
					echo 22;
				}
				
				
				
		}
		
		
		if($aryChkFilesAmt==0){
			mkdir("../application/views/templates/$new_file_name[0]/site",0777,TRUE);
			foreach($array_file_site as $item_file){
				$success_site = 0;
				copy($import_file_site.$item_file,$export_file_site.'/'.$item_file);
				$success_site = 1;
			}			
		}else{
			echo 'ee';
			return false;		
		}
		
		
		//******************//
		// End Site
		//******************//
		
		
		
		
		
		//******************//
		// Start Thumbnail
		//******************//
		
		$import_file_thum			=	"../public/templates/$new_file_name[0]/thumbnail/";
		//$export_file_thum			=	"../application/views/templates/$new_file_name[0]/site";
		$array_file_thum			=	array();
		
		$check_file_thum			=	array();
		
		if(!@opendir($import_file_thum)){
			//echo 'ไม่พบ Folder site';
			echo 4;	
			return false;
		}else{
			$read_file_site 			= 	opendir($import_file_thum);
		}
		

		while (false !== ($entry_thum = readdir($read_file_site))){
				//echo $entry_thum."<br>";
				if ($entry_thum != "." && $entry_thum != "..") {
					$file_name = explode('.',$entry_thum);
					if(isset($file_name[0]) && in_array($file_name[0],$aryChkFileThumbnail)){
						$array_file_thum[] = $entry_thum;
						$aryChkFilesAmt--;
					}
				}
				
				
				if($entry_thum == "template_article.jpg"){
					echo 24;
					$check_file_thum[0] = '1';
					
				}
				if($entry_thum == "template_category.jpg"){
					echo 25;
					$check_file_thum[1] = '1';
	
				}
				if($entry_thum == "template_copyright.jpg"){
					echo 26;
					$check_file_thum[2] = '1';
				}
				if($entry_thum == "template_main.jpg"){
					echo 27;
					$check_file_thum[3] = '1';
					
				}
				if($entry_thum == "template_menu.jpg"){
					echo 28;
					$check_file_thum[4] = '1';
				}
		}
		
		
		if(count($check_file_thum) == 5){
			$success_thum=1;
		}else{
			echo 'ee';
			return false;
		}
		
		//******************//
		// End Thumbnail
		//******************//
		
		
		//if(($success_import == 1 && $success_thum == 1) && ($success_site == 1 && $success_layout == 1)){
		//		echo "mm";  // บอกว่าอัพโหลดสำเร็จแล้ว
		//}
		

		$sessce = $this->insert_model->insert_template($new_file_name[0]);
		
		($sessce==TRUE?$opt='mm':'de');
        echo json_encode(array("opt"=>"$opt"));
		 
		 	 
			 
		
	}
	
	public function copy_id_ar(){
		
		
		if(@$_POST){
			
			$arr['id_copy_article'] 				=  $this->input->post('id');
		    $this->session->set_userdata($arr);
			
			$sessce = $this->session->userdata('id_copy_article');
			($sessce!=null?$opt='Yes':'No');
        	echo json_encode(array("opt"=>"$opt"));
		
		
		}	
		
		
		
	}
	
}		
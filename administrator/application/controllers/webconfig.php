<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class webconfig extends CI_Controller {
    
    public function __construct()
    {
        parent::__construct();
        $this->template 				= 'templates/'.$this->get_model->get_template()->administrator_template.'/index.php';
        $this->template_paht 			= base_url().'public/templates/'.$this->get_model->get_template()->administrator_template.'/';
        $this->title 					= $this->get_model->get_template()->site_name;
		$this->color_footer				= $this->get_model->get_template()->footer;
		
		
	
	
		
    }

	public function index()
	{
		
		 if($this->session->userdata('logged_in')){
			$session_data = $this->session->userdata('logged_in'); 
			
			// Link
			$data['dashboard']				= base_url()."home";
			$data['add']					= base_url()."home/add";
			$data['view_table']				= base_url()."home/article_view";
			$data['view_Article']			= base_url()."home/Article";
			$data['view_Category']			= base_url()."home/Category";
			$data['Sub_Category_menu']		= base_url()."/home/Sub_Category";
			$data['add_sub_category']		= base_url()."home/add_sub_category";
			$data['add_category']			= base_url()."home/add_category";			
			// Link
			$data['article']				= $this->get_model->get_article();		
			
			$data['Category']				= $this->get_model->get_category_table();
			$data['style_bg']				= $this->get_model->get_template()->backgroud_color;
			
			$data['template_paht'] 			= $this->template_paht;
			$data['username'] 				= $session_data['username'];
			$data['shoping_mode']			= $this->get_model->gender_enums('layouts_config','shoping_mode');
			//$data['is_homepage_status']		= $this->get_model->gender_enums('article','status');
			$data['template']				= $this->get_model->template();
			//$data['layout_config']			= $this->get_model->layout_config();
			$data['color_footer']			= $this->color_footer;
			$data['success']				= '';
			$data['content']                = '/templates/admin_two/view_webconfig';
            $data['pagetitle']              = 'Web Config';
			$data['active_menu']			= 5;
			
			
			
			
			$sql 	= 	"Select *from layouts_config";
			$rs=$this->db->query($sql);
			
			if($rs->num_rows()==0){
				$data['rs']=array();
			}
			else{
				$data['rs']=$rs->row_array();	
			}

		
			

			
			
			
			$this->load->view('layouts/layout.php',$data);
            if(@$_POST){
                
        
                if($sessce == TRUE){
                    $data['success']                = 'TRUE';
                     echo json_encode(array("name"=>"John","time"=>"2pm"));
                }else{
                    $data['success']                = $this->db->error()['code'];
                }
                unset ($_POST);
            }
          }else{
          	redirect('login', 'refresh');
          }
	
	}


    function testpost(){
    		
				
				if($this->input->post("value_bg") == null){
					//Configure
					//set the path where the files uploaded will be copied. NOTE if using linux, set the folder to permission 777
					$config['upload_path'] = "../public/upload_images"; 
					
			    	// set the filter image types
					$config['allowed_types'] = 'gif|jpg|png';
					
					//load the upload library
					$this->load->library('upload', $config);
			    
			    	$this->upload->initialize($config);
			    	
			    	$this->upload->set_allowed_types('*');
			
					$data['upload_data'] = '';
				
					$img;
					
					//if not successful, set the error message
					if (!$this->upload->do_upload('userfile')) {
						//$this->upload->display_errors();
						$img = $this->input->post('old_img');
			
					} else { //else, set the success message
						$data=$this->upload->data();	
					    $img=$data['file_name'];			
					}
					
					
					if($img != null){
						$background = $img;
					}
				}else{
						$background = $this->input->post("value_bg");
				}
		
				
		
			
			
			
			
			$db = array(
					'site_name' 				=> $this->input->post("site_name"),
					'template' 					=> $this->input->post("template"),
					'category_view' 			=> $this->input->post("Category"),
					'article_view' 				=> $this->input->post("Article"),
					'contact_view' 				=> $this->input->post("Contact"),
					'menu' 						=> $this->input->post("menu_view"),
					'footer' 					=> $this->input->post("border_color"),
					'copy_right' 				=> $this->input->post("Contact"),
					'index_text' 				=> $this->input->post("Index"),
					'shoping_mode' 				=> $this->input->post("shopping_mode"),
					'show_nextmatch'			=> $this->input->post("match_mode"),
					'backgroud_color'			=> @$background
			);
		
		$sessce = $this->insert_model->config($db);
		($sessce==TRUE?$opt='YES':$opt=$this->db->error()['code']);
		
		
	 	$arr['save_config']               = $opt;
		$this->session->set_userdata($arr);
		
         
		redirect('/webconfig', 'refresh');
    }
	
	
	
	

	public function change_template_index(){
			$str	= str_replace("administrator","public/templates/".$this->input->post("name_img"),base_url()).'thumbnail/template_main.jpg';
			
		?>		
				<img class="img-responsive fadeInRight-animated "  width="100%" src="<?=$str?>">
		<?php
			
			
	}
	
	public function change_template_category(){

			$str	= str_replace("administrator","public/templates/".$this->input->post("name_img"),base_url()).'thumbnail/template_category.jpg';
			
		?>		
				<img class="img-responsive fadeInRight-animated "  width="100%" src="<?=$str?>">
		<?php
			
			
	}
	
	public function change_template_article(){
			$str	= str_replace("administrator","public/templates/".$this->input->post("name_img"),base_url()).'thumbnail/template_article.jpg';
			
		?>		
				<img class="img-responsive fadeInRight-animated "  width="100%" src="<?=$str?>">
		<?php
			
			
	}
	
	
	public function change_template_menu(){
		$str	= str_replace("administrator","public/templates/".$this->input->post("name_img"),base_url()).'thumbnail/template_menu.jpg';
			
		?>		
				<img class="img-responsive fadeInRight-animated "  width="100%" src="<?=$str?>">
		<?php
	}
	
	public function change_template_footer(){
		$str	= str_replace("administrator","public/templates/".$this->input->post("name_img"),base_url()).'thumbnail/template_footer.jpg';
			
		?>		
				<img class="img-responsive fadeInRight-animated "  width="100%" src="<?=$str?>">
		<?php
	}
	
	public function change_template_contact(){
		$str	= str_replace("administrator","public/templates/".$this->input->post("name_img"),base_url()).'thumbnail/template_copyright.jpg';
			
		?>		
				<img class="img-responsive fadeInRight-animated "  width="100%" src="<?=$str?>">
		<?php
	}
	
	
	
	public function slide(){
		
		$session_data = $this->session->userdata('logged_in'); 
		
		
		$data['slides']					= $this->get_model->get_slides();
	    $data['template_paht'] 			= $this->template_paht;
        $data['title'] 					= $this->title;	    	
        
		$data['is_single_page']			= $this->get_model->gender_enums('category','is_single_page');	
		$data['is_menu']				= $this->get_model->gender_enums('category','is_menu');
		$data['is_homepage_status']		= $this->get_model->gender_enums('category','status');
		
 		$data['username'] 				= $session_data['username'];
	    $data['success']				= '';
		$data['active_menu']			= 7;
		// Link
		$data['dashboard']				= base_url()."home";
		$data['add']					= base_url()."home/add";
		$data['view_table']				= base_url()."home/article_view";
		$data['view_Article']			= base_url()."home/Article";
		$data['view_Category']			= base_url()."home/Category";
		$data['Sub_Category_menu']		= base_url()."home/Sub_Category";
		$data['add_sub_category']		= base_url()."home/add_sub_category";
		$data['add_category']			= base_url()."home/add_category";
		$data['cancel']					= base_url()."home/Category";			
		// Link
		
		$data['content']                = '/templates/admin_two/view_upload_slide';
        $data['pagetitle']              = 'Add Slide Images';
		
		$this->load->view('layouts/layout.php',$data);
		
	}

	public function add_slide(){
		
		$session_data = $this->session->userdata('logged_in'); 
		
		
		$data['slides']					= $this->get_model->get_slides();
	    $data['template_paht'] 			= $this->template_paht;
        $data['title'] 					= $this->title;	    	
        
		$data['is_single_page']			= $this->get_model->gender_enums('category','is_single_page');	
		$data['is_menu']				= $this->get_model->gender_enums('category','is_menu');
		$data['is_homepage_status']		= $this->get_model->gender_enums('category','status');
		
 		$data['username'] 				= $session_data['username'];
	    $data['success']				= '';
		$data['active_menu']			= 7;
		// Link
		$data['dashboard']				= base_url()."home";
		$data['add']					= base_url()."home/add";
		$data['view_table']				= base_url()."home/article_view";
		$data['view_Article']			= base_url()."home/Article";
		$data['view_Category']			= base_url()."home/Category";
		$data['Sub_Category_menu']		= base_url()."home/Sub_Category";
		$data['add_sub_category']		= base_url()."home/add_sub_category";
		$data['add_category']			= base_url()."home/add_category";
		$data['cancel']					= base_url()."home/Category";			
		// Link
		
		$data['content']                = '/templates/admin_two/view_add_slide';
        $data['pagetitle']              = 'Add Slide Images';
		
		$this->load->view('layouts/layout.php',$data);
		
	}



	public function upload_multifiles(){
		
		
		
		$config['upload_path'] = "../public/upload_slide_images"; 
					
    	// set the filter image types
		$config['allowed_types'] = 'gif|jpg|png';
		
		//load the upload library
		$this->load->library('upload', $config);
    
    	$this->upload->initialize($config);
    	
    	$this->upload->set_allowed_types('*');

		$data['upload_data'] = '';
	
		$img;
		
		//if not successful, set the error message
		if (!$this->upload->do_upload('file_img')) {
			$this->upload->display_errors();
		} else { //else, set the success message
			$data=$this->upload->data();	
		    $img=$data['file_name'];
		 
			
			$data = array(
				'src_paht'		=>	$img,
				'link'			=>  $this->input->post('link'),
				'order_no'		=>	$this->input->post('oder_no'),
				'tooltip'		=>	$this->input->post('title'),
				'status'		=>	$this->input->post('status')
			);
			
			 $sessce = $this->db->insert('cms_slide',$data);

		 	($sessce==TRUE?$opt='YES':$opt=$this->db->error()['code']);
		
			$arr['save_slide']               = $opt;
			$this->session->set_userdata($arr);
		 	redirect('/webconfig/slide', 'refresh');
			
			
		}
		
	}


	public function del_slide_img(){
		if(@$_POST){
		    $arr['delete_slide_id']               = $this->input->post('id_del');
			$arr['delete_slide_img']               = $this->input->post('img');
			$this->session->set_userdata($arr);
		}

		$succ = $this->insert_model->delete_slide_img();
		if($succ)
        {
           	echo "TRUE"; 	
        }else
        {
            echo  $this->db->error()['code'];
        }
	}
	
	
	public function edit_slide(){
		
		$session_data = $this->session->userdata('logged_in'); 
		$data['slides']					= $this->get_model->get_slides();
	    $data['template_paht'] 			= $this->template_paht;
        $data['title'] 					= $this->title;	    	
        
		$data['is_single_page']			= $this->get_model->gender_enums('category','is_single_page');	
		$data['is_menu']				= $this->get_model->gender_enums('category','is_menu');
		$data['is_status']				= $this->get_model->gender_enums('cms_slide','status');
		
 		$data['username'] 				= $session_data['username'];
	    $data['success']				= '';
		$data['active_menu']			= 7;
		// Link
		$data['dashboard']				= base_url()."home";
		$data['add']					= base_url()."home/add";
		$data['view_table']				= base_url()."home/article_view";
		$data['view_Article']			= base_url()."home/Article";
		$data['view_Category']			= base_url()."home/Category";
		$data['Sub_Category_menu']		= base_url()."home/Sub_Category";
		$data['add_sub_category']		= base_url()."home/add_sub_category";
		$data['add_category']			= base_url()."home/add_category";
		$data['cancel']					= base_url()."home/Category";			
		// Link
		
		$data['content']                = '/templates/admin_two/view_edit_slide';
        $data['pagetitle']              = 'Edit Slide Images';
		
		
		
		
		if(@$_POST){
			$arr['id_edit_slides']               = $this->input->post('id');
			$this->session->set_userdata($arr);
		}
		
		if($this->session->userdata('id_edit_slides')){
			$id 	=	$this->session->userdata('id_edit_slides');
			$sql	= 	"Select * from cms_slide where id='$id'";
			$rs=$this->db->query($sql);
			
			if($rs->num_rows()==0){
				$data['rs']=array();
			}
			else{
				$data['rs']=$rs->row_array();	
			}
		}
		
		
		$this->load->view('layouts/layout.php',$data);
		
	}

	
	public function update_edit_slide(){
		
	
		$config['upload_path'] = "../public/upload_slide_images"; 
					
    	// set the filter image types
		$config['allowed_types'] = 'gif|jpg|png';
		
		//load the upload library
		$this->load->library('upload', $config);
    
    	$this->upload->initialize($config);
    	
    	$this->upload->set_allowed_types('*');

		$data['upload_data'] = '';	
		
		$img;
		
		//if not successful, set the error message
		if (!$this->upload->do_upload('file_img')) {
			//$this->upload->display_errors();
			$img = $this->input->post('old_img');
		} else {
			$data=$this->upload->data();	
		    $img=$data['file_name'];
		}
		
		
		$data = array(
				'src_paht'		=>	$img,
				'link'			=>  $this->input->post('link'),
				'order_no'		=>	$this->input->post('oder_no'),
				'tooltip'		=>	$this->input->post('title'),
				'status'		=>	$this->input->post('status')
			);
			
			 $sessce = $this->insert_model->update_slides($data);

		 	($sessce==TRUE?$opt='YES':$opt=$this->db->error()['code']);
		
			$arr['save_slide']               = $opt;
			$this->session->set_userdata($arr);
		 	redirect('/webconfig/slide', 'refresh');
			
		
		
	}
	
	
	
		
		
}

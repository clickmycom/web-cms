<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Title</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">


<link href="<?=$template_paht?>css/bootstrap.min.css" rel="stylesheet">
<link href="<?=$template_paht?>css/Copy of bootstrap.min.css" rel="stylesheet">
<link href="<?=$template_paht?>css/bootstrap-responsive.min.css" rel="stylesheet">

        
<link href="<?=$template_paht?>font/400italic,600italic,400,600" rel="stylesheet">
        
<link href="<?=$template_paht?>css/font-awesome.css" rel="stylesheet">
<link href="<?=$template_paht?>css/style.css" rel="stylesheet">
<!-- <link href="<?=$template_paht?>css/pages/dashboard.css" rel="stylesheet"> -->
<link rel="stylesheet"  type="text/css" href="<?=$template_paht?>easyui/css/easyui.css">
<link rel="stylesheet"  type="text/css" href="<?=$template_paht?>easyui/css/icon.css">
<link href="<?=$template_paht?>easyui/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />

<script type="text/javascript" 	src="<?=$template_paht?>easyui/js/jquery.min.js"></script>
<script type="text/javascript" 	src="<?=$template_paht?>easyui/js/jquery.easyui.min.js"></script>
<!-- <script type="text/javascript" 	src="<?=$template_paht?>easyui/js/bootstrap-validator.js"></script> -->
<script src="<?=$template_paht?>tinymce/tinymce.min.js"></script>

<script src="<?=$template_paht?>easyui/js/fileinput.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?=$template_paht?>js/bootstrap.min.js"></script>

<link rel="stylesheet"  		type="text/css" href="<?=$template_paht?>sweetalert/sweetalert.css">
<script type="text/javascript"  src="<?=$template_paht?>sweetalert/sweetalert.min.js"></script>


<link rel="stylesheet" href="<?=$template_paht?>datatable/css/bootstrap.min.css" />
<link rel="stylesheet" href="<?=$template_paht?>datatable/css/dataTables.bootstrap.css" /> 
<link rel="stylesheet" href="<?=$template_paht?>datatable/css/dataTables.responsive.css" /> 

<script type="text/javascript" src="<?=$template_paht?>js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?=$template_paht?>datatable/js/jquery.js"></script>
<script type="text/javascript" src="<?=$template_paht?>datatable/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?=$template_paht?>datatable/js/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="<?=$template_paht?>datatable/js/dataTables.responsive.js"></script>

<script src="<?=$template_paht?>js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?=$template_paht?>js/bootstrap-number-input.js"></script>
<script type="text/javascript" src="<?=$template_paht?>js/jquery.form.js"></script>


<link rel="stylesheet" href="<?=$template_paht?>color_picker/1.2.3/css/pick-a-color-1.2.3.min.css">	

<script src="<?=$template_paht?>date_picker/mootools-core.js" type="text/javascript"></script>
<script src="<?=$template_paht?>date_picker/mootools-more.js" type="text/javascript"></script>

<script src="<?=$template_paht?>date_picker/Locale.en-US.DatePicker.js" type="text/javascript"></script>
<script src="<?=$template_paht?>date_picker/Picker.js" type="text/javascript"></script>
<script src="<?=$template_paht?>date_picker/Picker.Attach.js" type="text/javascript"></script>
<script src="<?=$template_paht?>date_picker/Picker.Date.js" type="text/javascript"></script>


<link href="<?=$template_paht?>date_picker/datepicker_bootstrap/datepicker_bootstrap.css" rel="stylesheet">
	
<script type="text/javascript" src="<?=$template_paht?>multiselect/js/bootstrap-multiselect.js"></script>
<link rel="stylesheet" href="<?=$template_paht?>multiselect/css/bootstrap-multiselect.css" type="text/css"/>


</head>
	
<body>
<script type="text/javascript">
$(document).ready(function() {
    $('#example').DataTable( {
        responsive: true
    } );
} );
</script>

<? $this->load->view('layouts/header.php')?> <!-- header.php -->
<? $this->load->view('layouts/menu.php')?>  <!-- menu.php -->

    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3><?=$pagetitle?></h3> 
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						<? $this->load->view("$content")?>	
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
   
<? $this->load->view('layouts/lower_panel.php');?> <!-- lower_panel.php -->
<? $this->load->view('layouts/footer.php');?><!-- footer.php -->

<script src="<?=$template_paht?>js/bootstrap.js"></script>
<script src="<?=$template_paht?>js/base.js"></script>

<script>

// Remember set you events before call bootstrapSwitch or they will fire after bootstrapSwitch's events
$("[name='checkbox2']").change(function() {
	if(!confirm('Do you wanna cancel me!')) {
		this.checked = true;
	}
});

$('#after').bootstrapNumber();
$('#colorful').bootstrapNumber({
	upClass: 'success',
	downClass: 'danger'
});
</script>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

</body>
</html>

<div class="subnavbar">

	<div class="subnavbar-inner">
	
		<div class="container">
		
			<ul class="mainnav">
			
				<li class=" 
				
				<?php
					if($active_menu == 1){
						echo "active";
					}
				
				?>

				">
				<a href="<?=base_url()?>home">
				<i class="icon-dashboard"></i>
				<span>Dashboard</span>
				</a>	    				
				</li>

				
				
				<li class="dropdown 
				<?php
					if($active_menu == 2){
						echo "active";
					}
				
				?>">					
				
					<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-list-alt"></i>
						<span >Content Manage</span>
						<b class="caret"></b>
					</a>	
				
					<ul class="dropdown-menu">
                        
					  	<li class="dropdown-submenu">
							<a href="<?=$view_Article?>">Article</a>
							<ul class="dropdown-menu">
								<li>
									<a tabindex="-1" href="<?=$add?>">Add</a>
								</li>
								<li>
									<a tabindex="-1" href="<?=$view_Article?>">View Data</a>
								</li>							
							</ul>
						</li>
						
						
					  	<li class="dropdown-submenu">
							<a href="<?=$view_Category?>">Category</a>
							<ul class="dropdown-menu">
								<li>
									<a tabindex="-1" href="<?=$add_category?>">Add</a>
								</li>
								<li>
									<a tabindex="-1" href="<?=$view_Category?>">View Data</a>
								</li>	
								
							</ul>
						</li>
						
						<li class="dropdown-submenu">
							<a href="<?=$Sub_Category_menu?>">Sub Category</a>
							<ul class="dropdown-menu">
								<li>
									<a tabindex="-1" href="<?=$add_sub_category?>">Add</a>
								</li>
								<li>
									<a tabindex="-1" href="<?=$Sub_Category_menu?>">View Data</a>
								</li>	
								
							</ul>
						</li>						
						
						
					</ul>
				</li>
													

				
				
				
				
				
			
				
		
				<li class="
				<?php
					if($active_menu == 5){
						echo "active";
					}
				
				?>
				
				">					
				<a href="<?=base_url()?>webconfig">
				<i class="icon-bar-chart"></i>
				<span>Web Config</span>
				</a>  									
				</li>
				
				
				
				
				
				
				
				
				
				
				
				
				<li class="dropdown 
				<?php
					if($active_menu == 6){
						echo "active";
					}
				
				?>">					
					<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-list-alt"></i>
						<span >Managed Team</span>
						<b class="caret"></b>
					</a>	
				
					<ul class="dropdown-menu">
						
						
						<li class="dropdown-submenu">
							<a href="<?=base_url()?>managed/team">Team</a>
							<ul class="dropdown-menu">
								<li>
									<a tabindex="-1" href="<?=base_url()?>managed/add_team">Add Team</a>
								</li>
								<li>
									<a tabindex="-1" href="<?=base_url()?>managed/team">View Data</a>
								</li>	
								
							</ul>
						</li>
						
						
						
                        
					  	<li class="dropdown-submenu">
							<a href="<?=base_url()?>managed">Personal</a>
							<ul class="dropdown-menu">
								<li>
									<a tabindex="-1" href="<?=base_url()?>managed/add_presonal">Add Personal</a>
								</li>
								<li>
									<a tabindex="-1" href="<?=base_url()?>managed">View Data</a>
								</li>							
							</ul>
						</li>
					  	
								
						
						
					</ul>
				</li>
				

				<li class="
				<?php
					if($active_menu == 7){
						echo "active";
					}
				
				?>
				
				">					
				<a href="<?=base_url()?>webconfig/slide">
				<i class="icon-bar-chart"></i>
				<span>Slide Show</span>
				</a>  									
				</li>
				
				

			</ul>
		
		</div> <!-- /container -->
	
	</div> <!-- /subnavbar-inner -->

</div> <!-- /subnavbar -->

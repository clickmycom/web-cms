<nav class=" subnavbar navbar-inverse navbar-static-top" role="navigation" style="margin-bottom: 0px;background-color: #00ba8b !important;">
	<div class="container-fluid  ">
    <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>

          </button>
          <a class="navbar-brand" href="#" style="color: #fff;">Top Bar Title</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
          
          
           
            
          </ul>
           <ul class="nav navbar-nav navbar-right" >
  
         	 <li><a href="<?=str_replace("administrator/","",base_url())?>" target="_blank"><span style="color:#fff;" class="glyphicon icon-list-alt"></span>&nbsp;&nbsp;Front End</a></li>
	        <li><a href=""><span class="glyphicon glyphicon-user"></span>&nbsp;&nbsp;<?=$username?></a></li>
	        <li><a href="<?=base_url();?>login/logout"><span class="glyphicon glyphicon-log-in"></span>&nbsp;&nbsp;Logout</a></li>
	      </ul>
        </div><!-- /.navbar-collapse -->
	</div>
</nav>

<!DOCTYPE html>
<html class="no-js sidebar-large">

<head>
    <!-- BEGIN META SECTION -->
    <meta charset="utf-8">

    <!-- END META SECTION -->
    <!-- BEGIN MANDATORY STYLE -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="<?=$template_paht?>css/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=$template_paht?>css/assets/css/plugins.min.css" rel="stylesheet">
    <link href="<?=$template_paht?>css/assets/css/style.min.css" rel="stylesheet">
  	


<link rel="stylesheet"  		type="text/css" href="<?=$template_paht?>sweetalert/sweetalert.css">
<script type="text/javascript"  src="<?=$template_paht?>sweetalert/sweetalert.min.js"></script>
    <!-- END  MANDATORY STYLE -->

</head>


<script>

	
	
	function myFunction() {
		document.getElementById('user').value = null;
		
	}
	
	function myFunction2(){
	 	document.getElementById('pass').value = null;
	}
	
</script>

<body>

	<?
		if($login_failed==FALSE){
	?>		
			<script>
				sweetAlert("Oops...", "ไม่พบ Username หรือ Password ของท่าน", "error");				
			</script>
			
	<?	
		}
	?>
    <!-- BEGIN LOGIN BOX -->
    <div class="container" id="login-block">
        <div class="row">
            <div class="col-sm-6 col-md-4 col-sm-offset-3 col-md-offset-4">
                <div class="login-box clearfix animated flipInY">
                    <div class="page-icon animated bounceInDown">
                        <img src="<?=$template_paht?>css/assets/img/account/user-icon.png" alt="Key icon">
                    </div>
                    <div class="login-logo">
                        <a href="#?login-theme-3">
                            <img src="<?=$template_paht?>css/assets/img/account/login-logo.png" alt="Company Logo">
                        </a>	
                    </div>
                    <hr>
                    <div class="login-form">
                      
                       <?php echo form_open('login'); ?>
                        	
                            <input type="text" placeholder="Username" 	ondblclick="myFunction()" id="user" name="user" class="input-field form-control user"  required/>
                            <input type="password" placeholder="Password"  id="pass" ondblclick="myFunction2()" name ="password" class="input-field form-control password" required/>
                            <input type="submit" name="btn_login" class="btn_submit btn btn-login ladda-button" data-style="expand-left" value="Submit">
                            
                     	<?php echo form_close(); ?>
                     	
                        <div class="login-links">
                            <a href="<?=base_url();?>login/signup">Don't have an account? <strong>Sign Up</strong></a>
                        </div>
                    </div>
                </div>
               
            </div>
        </div>
    </div>
    <!-- END LOCKSCREEN BOX -->


  
</body>

</html>

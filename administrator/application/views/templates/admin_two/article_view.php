
<script src="<?=base_url()?>public/templates/admin_two/clipboard/clipboard.js"></script>


<script>
	
	function id_article(value){
		
		$.post("<?=base_url().$this->router->class.'/article_edit/'?>",{'value':value}).done(function(){
			window.location = "<?=base_url().$this->router->class.'/article_edit/'?>";
		});	
		
	}
	
	function myFunction_del_Article(id,id_img){
		swal({   title: "Are you sure?",   text: "You will not be able to recover this imaginary file!",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, 
			
		function(){   				
			$.post("<?=base_url().$this->router->class.'/del_article/'?>",{'id':id,'id_img':id_img}).done(
						function(data){
						    //console.log(data);		
							if(data=="TRUE"){
								swal("Deleted!", "Your imaginary file has been deleted.", "success");								
								setTimeout("alertWelcome();",1000);										
							}else{
								
								sweetAlert("Oops...", "Error "+data+" Something went wrong!", "error");
							}						
						}
			);
		});
	}
</script>

<!-- <script>
$(function(){
	
	
	//ส่ง id ของ article	
	$('.id_article').on('click',function(){
		
		value		= $(this).data('value');
		
		$.post("<?=base_url().$this->router->class.'/article_edit/'?>",{'value':value}).done(function(){
			window.location = "<?=base_url().$this->router->class.'/article_edit/'?>";
		});		

	})
	//ส่ง id ของ article	

	
	
	$('.myFunction_del_Article').on('click',function(){
			
			
		id		= $(this).data('id_del');
		id_img	= $(this).data('img_article');
		
		
		swal({   title: "Are you sure?",   text: "You will not be able to recover this imaginary file!",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, 
			
		function(){   				
			$.post("<?=base_url().$this->router->class.'/del_article/'?>",{'id':id,'id_img':id_img}).done(
						function(data){
						    //console.log(data);
		
							if(data=="TRUE"){
								swal("Deleted!", "Your imaginary file has been deleted.", "success");								
								setTimeout("alertWelcome();",1000);										
							}else{
								
								sweetAlert("Oops...", "Error "+data+" Something went wrong!", "error");
							}							
						}
			);
		});
		
		
			
	});
	
	
	
	
});
	
</script> -->

<script>

	function copyToClipboard(data) {
		clipboard.copy(data.toString()).then(function() {
			var x = data;
			
			$.post("<?=base_url().$this->router->class.'/copy_id_ar/'?>",{'id':data}).done(
						function(data){
							
							var number = jQuery.parseJSON(data);
						   	if(number.opt=="Yes"){
						   		swal({   title: "คัดลอก ไอดี : "+x,   text: "ระบบทำการ คัดลอกไปยัง clipboard เรียบร้อยแล้ว",  timer: 2000 ,showConfirmButton: false });
						   	}		
												
						}
			);
			
			//console.log("success");
		
		}, function(err) {
			sweetAlert("Oops...", "เงิบ Error ง่่ะ", "error");
			console.log("failure", err);
		});
	}
	
	
	
	

</script>


<script>
	function alertWelcome(){

		window.location = "<?=base_url().$this->router->class.'/Article/'?>";

	}
	
	$(document).ready(function() {
    	$('#example2').DataTable( {
        	"order": [[ 0, "desc" ]]
    	} );
	});

</script>






	<?php
		 if($this->session->userdata('save_succeed_ud_article') == "YES"){
		 	$this->session->unset_userdata('save_succeed_ud_article');
	?>
			<script>
				swal({   
                        title: "บันทึกข้อมูลเรียบร้อยแล้ว", 
                        text: "ผมกำลังปิดตัวเองใน  2 วินาที.",  
                        type: "success",   
                        showConfirmButton: false,                                    
                        timer: 2000 
                     }                   
                  );

			</script>
	<?php
			 
		 }else if($this->session->userdata('save_succeed_ud_article')!=null){
		 	
	?>
		 	<script>
		 		sweetAlert("Oops..<?=$this->session->userdata('save_succeed')?>", "Something went wrong!", "error");
			</script>
		 	
	<?php	$this->session->unset_userdata('save_succeed_ud_article');
		 }
		 	
	?>
	
	
	

                    <div class="widget-content">
                        <section>
                    		<ul>
                    			<center>
                    			
                    				<a href="<?=base_url().$this->router->class?>/add" class="btn btn-info btn-md">
									    <span class="glyphicon glyphicon-plus"></span> Add Article
									</a>	
									
								</center>
								
                    		</ul>

                    	</section>

                                <table id="example2" class="table  hover table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                    <tr>
                                    	<th>Id</th>
                                        <th>Category</th>
                                        <th>Sub Category</th>
                                        <th><center>Article Name</center></th>
                                        <th>Show Date</th>
                                        <th>End Date</th>
                                         <th style="width:14px;"><center>Order</center></th>
                                        <th>Status</th>
                                       
                                        <th>Edit</th>
                                        <th style="width:1px;"><center>Delete</center></th>
                                    </tr>
                                </thead>

                                        <tbody>
                                            
                                             <?php
                                        
                                            foreach ($article as $value) {
                                    ?>
                                            <tr>
                                            	<td  data-label="ID:" onclick="copyToClipboard(<?=$value['id']?>)"><?=$value['id']?></label></td>
                                                <td  data-label="Category:"><?=$value['category_name']?></label></td>
                                                <td  data-label="Sub Category:"><?=$value['sub_category_name']?></td>               
                                                <td  data-label="Article Name:"><?=$value['article_name']?></td>    
                                                <td  data-label="Start Date:"><?=$value['showdate']?></td>
                                                <td  data-label="End Date:" ><?=$value['enddate']?></td>
                                              
                                                <td  data-label="Order On:" width="8px;"><center><?=$value['order_no']?></center></td>
                                                <?php
                                                	if($value['status'] == "Active"){
                                                		$bg= "66CC99";
                                                	}else{
                                                		$bg= "BEBEBE";
                                                	}
												?>
                                                <td  data-label="Status:" style="background: #<?=$bg?>;" width="80px;"><center><b><?=$value['status']?></b></center></td>
                                                <td data-label="Edit:" style="background: #FFFFCC;" onclick="id_article(<?=$value['id']?>)" title="แก้ไข">
                                                    <center>
                                                        <img href="#" src="https://cdn3.iconfinder.com/data/icons/fatcow/16/application_form_edit.png"    data-value="<?=$value['id']?>"/>
                                                        
                                                   </center>
                                                </td>
                                                <?php
                                                		$a= $value['id'];
														$b= $value['article_image'];
                                                	
                                                	?>
                                                <!-- <td data-label="Delete:"><center><? echo anchor("home/del_article/".$value['id'],'<img src="https://cdn3.iconfinder.com/data/icons/softwaredemo/PNG/16x16/Close_Box_Red.png" />',array("onclick"=>"javascript:return confirm('คุณต้องการลบใช่หรือไม่ ?');"));?></center></td> -->
                                            	<td data-label="Delete:" style="background: #FF9999;" onclick="myFunction_del_Article(<?=$a?>,'<?=$b?>')" title="ลบข้อมูล">
                                                	<!-- <center>
                                                		<? echo anchor("home/del_sub_category/".$value['id'],'<img src="https://cdn3.iconfinder.com/data/icons/softwaredemo/PNG/16x16/Close_Box_Red.png" />',array("onclick"=>"javascript:return confirm('คุณต้องการลบใช่หรือไม่ ?');"));?>
                                                	</center> -->
                                                	
                                                	<center>
                                                        <img src="https://cdn3.iconfinder.com/data/icons/softwaredemo/PNG/16x16/Close_Box_Red.png"   data-img_article="<?=$value['article_image']?>"  data-id_del="<?=$value['id']?>"/>
                                                   </center>
                                                	
                                                	
                                                </td>    
                                                
                                            
                                            </tr>   
                                    <?php   }
                                        
                                        
                                        
                                    ?>
                                          
                                          
                                          
                                        </tbody>
                                </table>
        
                    </div> <!-- /pricing-plans -->      
   






<script>
	
	function id_category(id_category){
		
		$.post("<?=base_url().$this->router->class.'/edit_category/'?>",{'id_category':id_category}).done(function(){
			window.location = "<?=base_url().$this->router->class.'/edit_category/'?>";
		});	
		
	}
	
	
	function myFunction_del_Category(id_del_category){
		
		swal({   	title: "Are you sure?",   
					text: "You will not be able to recover this imaginary file!",   
					type: "warning",   
					showCancelButton: true,   
					confirmButtonColor: "#DD6B55",   
					confirmButtonText: "Yes, delete it!",   
					closeOnConfirm: false }, 
			
		function(){   				
			$.post("<?=base_url().$this->router->class.'/del_category/'?>",{'id_del_category':id_del_category}).done(
						function(data){
							if(data=="TRUE"){
								swal("Deleted!", "Your imaginary file has been deleted.", "success");								
								setTimeout("alertWelcome();",1000);										
							}else{
								
								sweetAlert("Oops...Error "+data+"", "Something went wrong!", "error");
							}
							
						}
			);

		});
		
	}
	
	function alertWelcome(){

		window.location = "<?=base_url().$this->router->class.'/Category/'?>";

	}
	
	
	$(document).ready(function() {
    	$('#example2').DataTable( {
        	"order": [[ 0, "desc" ]]
    	} );
	});


</script>






                    
                    <div class="widget-content">
                        <section>
                    		<ul>
                    			<center>
                    			
                    				<a href="<?=base_url().$this->router->class?>/add_category" class="btn btn-info btn-md">
									    <span class="glyphicon glyphicon-plus"></span> Add Category
									</a>	
									
								</center>
								
                    		</ul>

                    	</section>

                                <table id="example2" class="table  hover table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                    <tr>
                                    	<th><center>ID</center></th>
                                        <th><center>Name</center></th>
                                        <th><center>Is Menu</center></th>
                                        <th><center>ink</center></th>
                                        <th><center>Order No</center></th>
                                        <th><center>Status</center></th>
                                         <th>Edit</th>
                                          <th><center>Delete</center></th>
                                    </tr>
                                </thead>

                                        <tbody>
                                            
                                    <?php
                                        
                                            foreach ($Category as $value) {
                                    ?>
                                            <tr>
                                            	<td  data-label="ID:" width="20px;"><?=$value['id']?></td>
                                                <td  data-label="Name:"><?=$value['name']?></td>
                                                <td  data-label="Is Menu:"><?=$value['is_menu']?></td>               
                                                <td  data-label="Link:"><?=$value['link']?></td>    
                                                <td  data-label="Order No:" width="80px;"><center><?=$value['order_no']?></center></td>
                                                
                                                <?php
                                                	if($value['status'] == "Active"){
                                                		$bg= "66CC99";
                                                	}else{
                                                		$bg= "BEBEBE";
                                                	}
                                                
                                                ?>
                                                <td  data-label="Status:" style="background: #<?=$bg?>;" width="80px;"><center><b><?=$value['status']?></b></center></td>
                                                
                                                
                                                 <td data-label="Edit:" style="background: #FFFFCC;"  width="60px"  onclick="id_category(<?=$value['id']?>)">
                                                    <center>
                                                        <img src="https://cdn3.iconfinder.com/data/icons/fatcow/16/application_form_edit.png"   data-id_sub_category="<?=$value['id']?>"/>
                                                   </center>
                                                </td>
                                                
                                                <td data-label="Delete:" style="background: #FF9999;" onclick="myFunction_del_Category(<?=$value['id']?>)" width="60px">
                                                	<!-- <center>
                                                		<? echo anchor("home/del_sub_category/".$value['id'],'<img src="https://cdn3.iconfinder.com/data/icons/softwaredemo/PNG/16x16/Close_Box_Red.png" />',array("onclick"=>"javascript:return confirm('คุณต้องการลบใช่หรือไม่ ?');"));?>
                                                	</center> -->
                                                	<center>
                                                        <img src="https://cdn3.iconfinder.com/data/icons/softwaredemo/PNG/16x16/Close_Box_Red.png"  />
                                                   </center>
                                                	
                                                	
                                                </td>                                          
                                            
                                            </tr>   
                                    <?php   }
                                        
                                        
                                        
                                    ?>
                                          
                                          
                                          
                                        </tbody>
                                </table>
        
                    </div> <!-- /pricing-plans -->      
    
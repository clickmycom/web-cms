<script src="http://malsup.github.com/jquery.form.js"></script>

<style>
@media ( max-width: 585px ) {
    .input-group span.input-group-btn,.input-group input,.input-group button{
        display: block;
        width: 100%;
        border-radius: 0;
        margin: 0;
    }
    .input-group {
        position: relative;   
    }
    .input-group span.data-up{
        position: absolute;
        top: 0;
    }
    .input-group span.data-dwn{
        position: absolute;
        bottom: 0;
    }
    .form-control.text-center {
        margin: 34px 0;
    }
    .input-group-btn:last-child>.btn, .input-group-btn:last-child>.btn-group{
        margin-left:0;
    }

}
</style>
			


<script type="text/javascript">
	tinymce.init({
	    selector: "textarea",
	    plugins: [
	        "advlist autolink lists link image charmap print preview anchor",
	        "searchreplace visualblocks code fullscreen",
	        "insertdatetime media table contextmenu paste"
	    ],
	    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
	});
</script>



<script>
	$(function(){
		
		$('form').submit(function(){
		         
		        $.post('<?=base_url().$this->router->class.'/update_edit_category'?>',$("form").serialize(),function( data ){
		             if(data.opt=='YES'){
		                 swal({   
		                                    title: "บันทึกข้อมูลเรียบร้อยแล้ว", 
		                                    text: "ผมกำลังปิดตัวเองใน  2 วินาที.",  
		                                    type: "success",   
		                                    showConfirmButton: false,                                    
		                                    timer: 2000 
		                                    }                   
		                 );
		                 setTimeout("redirect_page();",1500);	
		             }else{
		                sweetAlert("Oops..."+data.opt, "Something went wrong!", "error");
		             }	             
		        },"json");
		        return false;  
		    });
		    // end form
		    
		    
		var no = $('#is_singpage').children(":selected").text();    
		if(no == "Yes"){
			$('#number_page').slideDown();
			$('#ar_id').prop('required',true);
		}else{
			$('#ar_id').prop('required',false);
			$('#number_page').slideUp();
			
			
		}    
		    
		$('#is_singpage').change(function(){
			 var ch = $(this).children(":selected").text();
			 if(ch=="Yes"){
			 	 $('#number_page').slideDown();
			 	 $('#link').val("category_single");
			 	 $('#link').attr('readonly', true);
			 	 $('#ar_id').prop('required',true);
			 	
			 	 
			 }else{
			 	$('#number_page').slideUp();
			 	$('#link').attr('readonly', false);
			 	$('#link').val(null);
			 	$('#ar_id').prop('required',false);
			 	$('#ar_id').val(null);
			 }
		}); 
		
		
	});
	
	
	function redirect_page(){
		window.location = "<?=base_url().$this->router->class.'/Category'?>";
	}
		
</script>




					
					<div class="widget-content">
						
						
						
					<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						
						    <a href="#formcontrols" data-toggle="tab">Category Form</a>
						  </li>
						</ul>
						<br>
						
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols edit-profile form-horizontal">		
															
							
							 
								<!-- <?php echo  form_open('home/update_edit_category')?> -->
								<form method="POST">
									<fieldset id="edit-profile" class="form-horizontal">
										<!-- Start combobox Category -->
										
										
										
										<br>	
		
										<!-- Start Is Edittor  -->
										<div class="control-group">											
											<label class="control-label" for="username">Name</label>
											<div class="controls ">
												<input class="form-control" id="username" name="name" type="text" class="span6 form-control" id="firstname" value="<?=$rs['name']?>" >																					
											</div> <!-- /controls -->																
										</div> <!-- /control-group -->
										<!-- End Is Edittor  -->
										
											<!-- Start Is Edittor  -->
										<div class="control-group">											
											<label class="control-label" >Title</label>
											<div class="controls ">
												<input placeholder="คำอธิบาย" class="form-control" id="title" name="title" type="text" class="span6 form-control" value="<?=$rs['tooltip']?>" >																					
												
											</div> <!-- /controls -->																
										</div> <!-- /control-group -->
										<!-- End Is Edittor  -->
										
										
										<div class="control-group">											
											<label class="control-label" for="radiobtns">Is Menu</label>
											
                                            <div class="controls">
	                                              <div class="btn-group">          	                                   	
			                                             <select class="category" name="is_menu"  >			                                              	
			                                              			  <option><?=$rs['is_menu']?></option>
														
																		<?php
																			foreach ($is_menu as $value) {
																				if($value!=$rs['is_menu']){
																		?>				
																					<option ><?=$value?></option>
																		<?		}		
																			}																		
																		?>			                                              	
														</select>										
	                                            	</div>
                                             </div>	<!-- /controls -->			
										</div> <!-- /control-group -->
										<!-- End combobox Category -->
										
										
										<div class="control-group">											
											<label class="control-label" for="radiobtns">Is Single Page</label>
											
                                            <div class="controls">
	                                              <div class="btn-group">          	                                   	
			                                             <select class="category" name="is_singpage" id="is_singpage">
														  				<option><?=$rs['is_single_page']?></option>

														  	<?php
			                                              			foreach ($is_single_page as $value) {
			                                              				if($value!=$rs['is_single_page']){
			                                              				
															?>																														
																		<option ><?=$value?></option>
																			 											
															<?php	
																		}	  
																	}
			                                              	
			                                              	?>
			                                              	
														</select>										
	                                            	</div>
                                             </div>	<!-- /controls -->			
										</div> <!-- /control-group -->
										<!-- End combobox Category -->
										
										<div id="number_page" class="control-group" style="display: none;">
											<label class="control-label" for="username">Article ID </label>
											<div class="controls ">
												
												<?php
													if($rs['article_id']!=null){
														$id_ar = $rs['article_id'];
													}else if($id_copy_article !=null){
														$id_ar = $id_copy_article;
													}else{
														$id_ar = "b";
													}
												
												?>
												
												<input class="form-control" name="number_page" type="text" class="span6 form-control" id="ar_id" value="<?=$id_ar?>" style="width: 80px;">																					
											
											</div> <!-- /controls -->	
										</div>
										
										
									
										<!-- Start Is Edittor  -->
										<div class="control-group">											
											<label class="control-label" for="username">Link</label>
											<div class="controls ">
												<input class="form-control" id="link" name="Link" type="text" class="span6 form-control" id="firstname" value="<?=$rs['link']?>" >																					
											</div> <!-- /controls -->																
										</div> <!-- /control-group -->
										<!-- End Is Edittor  -->
										
										
								
										<!-- Sprintner -->
										<div class="control-group">											
											<label class="control-label" >Order NO</label>
											<div class="controls ">

																<div class="input-group col-xs-2">
																	<input id="colorful" class="form-control" type="number" name="oder_no" value="<?=$rs['order_no']?>" min="1" max="1000000" />
																</div>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										<!-- Sprintner-->
										
								
										
										<!-- Start Is Homepage Sub-Category -->
										<div class="control-group">											
											<label class="control-label" for="radiobtns">Status</label>
											
                                            <div class="controls">
	                                              <div class="btn-group">                                             	
			                                             <select name="status" >
														  				<option><?=$rs['status']?></option>
														
																		<?php
																			foreach ($is_homepage_status as $value) {
																				if($value!=$rs['status']){
																		?>				
																					<option ><?=$value?></option>
																		<?		}		
																			}																		
																		?>					 											
														
														</select>										
	                                            	</div>
                                             </div>	<!-- /controls -->			
										</div> <!-- /control-group -->
										<!-- End Is Homepage Sub-Category -->
											
										
										
										
										
										
										
										
										<input style="visibility: hidden;" type="text" id="url1" value="<?php echo base_url(); ?>"/>
										<div class="form-actions">
											<input type="submit" name="btnsave" class="btn btn-primary" value="Submit"> 
											<input type="submit" href="<?=$cancel?>" name="btnsave" class="btn btn-default" value="Cancel">  
										</div> <!-- /form-actions -->
										<hr>
									</fieldset>
								</form>

								</div>

							</div> 
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
		

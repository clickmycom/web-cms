


    <script>

    window.addEvent('domready', function(){
        new Picker.Date($('.wat'), {
            
            positionOffset: {x: 5, y: 0},
            pickerClass: 'datepicker_bootstrap',
            useFadeInOut: !Browser.ie
        });
    });

    </script>
    <script src="http://malsup.github.com/jquery.form.js"></script>
    

<script src="<?=base_url().'public/ajaxForm.js'?>"></script>

<script type="text/javascript">
        function myformatter(date){
            var y = date.getFullYear();
            var m = date.getMonth()+1;
            var d = date.getDate();
            return y+'-'+(m<10?('0'+m):m)+'-'+(d<10?('0'+d):d);
        }
        function myparser(s){
            if (!s) return new Date();
            var ss = (s.split('-'));
            var y = parseInt(ss[0],10);
            var m = parseInt(ss[1],10);
            var d = parseInt(ss[2],10);
            if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
                return new Date(y,m-1,d);
            } else {
                return new Date();
            }
        }
</script>
    




<script>
    
$(function(){
     $('.role').change(function(){
            if(this.value != "1"){
                 $('#Show_Football_Player').slideUp();
            }else{
                 $('#Show_Football_Player').slideDown();
            }
            
      });   
      
      document.getElementById("show_edit_img").style.display = "none";
      
       $('.remove_img').click(function(){
            
            document.getElementById("myDIV").style.display = "none";
            document.getElementById("show_edit_img").style.display = "inline";
            
      });
      
      first_nation = 0;
      second_nation = 0;
      $.post('<?=$url_site.'rest/services/getContentEdit'?>',{'id':<?=$this->session->userdata('id_edit_personnel')?>,'table':'personnel'}).done(function(data){
            
            //consloe.log(data);
            temp = jQuery.parseJSON(data); 
            console.log(temp);
            //console.log(temp.first_nationality_id);
            //console.log(temp.role_id);
            
            //op = document.getElementById('first_nationality').options[2].text;
            
            //console.log(temp);
            $("#personnel_id").val(temp.id);
            $("#address").val(temp.address);
            $("#birthdate").val(temp.birthdate);
            $("#club_id").val(temp.club_id);
            $("#country_id").val(temp.country_id);
            $("#email").val(temp.email);
            $("#first_nationality_id").val(temp.first_nationality_id);
            //$("#first_position_id").val(temp.first_position_id);
            //$("#game_status_id").val(temp.game_status_id);
            $("#gender").val(temp.gender);
            $("#hight").val(temp.hight);
            $("#id").val(temp.id);
            $("#is_asia_squad").val(temp.is_asia_squad);
            $("#lineid").val(temp.lineid);
            $("#name").val(temp.name);
            $("#passport_expired_date").val(temp.passport_expired_date);
            $("#passport_no").val(temp.passport_no);
            $("#picture").val(temp.picture);
            $("#role_id").val(temp.role_id);
            //$("#second_nationality_id").val(temp.second_nationality_id);
            //$("#second_position_id").val(temp.second_position_id);
            $("#status").val(temp.status);
            $("#surname").val(temp.surname);
            $("#telephone_mobile").val(temp['telephone/mobile']);
            $("#unifrom_number").val(temp.unifrom_number);
            $("#visa_expired_date").val(temp.visa_expired_date);
            $("#visa_no").val(temp.visa_no);
            $("#weight").val(temp.weight);
            $("#zipcode").val(temp.zipcode);
                
            $("#name_img").attr("src", '<?echo "$url_site";?>'+temp.picture);
            
            first_nation        = temp.first_nationality_id;
            country             = temp.country_id;
            role_id             = temp.role_id;
            gender              = temp.gender;
            team                = temp.teams;
            
            first_position_id   = temp.first_position_id;
            //game_status_id
            game_status         = temp.game_status_id;
            second_position     = temp.second_position_id;
            
        });
      
      $.post("<?=$url_site?>rest/services/getContentPersonnel",{club_id:<?=$get_club_id?>}).done(function(data){
          if(data!=null){
              temp = jQuery.parseJSON(data); 
              console.log(temp);
              genSelection('role',temp.role,'role');
              genSelection('first_nationality',temp.nationality,'name');
              genSelection('second_nationality',temp.nationality,'name');
              genSelection('country',temp.country,'name');
              genSelection('team',temp.team,'name');
              genSelection('second_position',temp.position,'position');
              genSelection('first_position_id',temp.position,'position');
              genSelection('game_status_id',temp.game_status,'name');
          }
      }).fail(function(){
            
          sweetAlert("Oops..Internet disconnect", "Please try again", "error");
         
          window.location = window.location;
      });
      //$('.mono').multiselect();
      
      
      function genSelection(name,data,f_name){
        html = '';
        //console.log("NAME::::"+name);
        data.each(function(d){
            
            selected = '';
            if(name == 'first_nationality'){
                
                if(first_nation != 0 && d.id == first_nation){selected = 'selected';}
            }
            else if(name == 'second_nationality'){
                if(first_nation != 0 && d.id == first_nation){selected = 'selected';}
            }
            else if(name == 'country'){
                if(country != 0 && d.id == country){selected = 'selected';}
            }
            else if(name == 'role'){
                if(role_id != 0 && d.id == role_id){selected = 'selected'; if(d.id == '1'){ $('#Show_Football_Player').show();}}
            }
            else if(name == 'team'){
                team.each(function(t){
                    if(d.id == t.team_id){selected = 'selected';}
                });
            }
            else if(name == 'second_position'){
                //console.log("Function D::::"+second_position);
                //console.log("/n");
                if(second_position != 0 && d.id == second_position){selected = 'selected';}
            }
            else if(name == 'first_position_id'){
                if(first_position_id != 0 && d.id == first_position_id){selected = 'selected';}
            }
            
            else if(name == 'game_status_id'){
                console.log("NAME::::"+game_status);
                if(game_status_id != 0 && d.id == game_status){selected = 'selected';}
            }
            
            html += "<option value='"+d.id+"' "+selected+">"+d[f_name]+"</option>";
        });
        //console.log($('#'+name));
        $('#'+name).append(html);
        
        if(name=='team'){
            $('.mono').multiselect({
                includeSelectAllOption: true    
            });
        }
        
      }
      
      
     
      $('form').submit(function(){
            url  = $(this).attr('action');
            type = $(this).attr('method');
            $.ajax({
                url : url,
                type : type,
                data : new FormData(this),
                cache:false,
                contentType: false,
                processData: false,
                success:function(data,status){
                    
                
                }
            }).done(function(data){
                temp = jQuery.parseJSON(data);
                console.log(temp.opt);
                
                if(temp.opt== "YES"){
                    swal({   
                                            title: "บันทึกข้อมูลเรียบร้อยแล้ว", 
                                            text: "ผมกำลังปิดตัวเองใน  2 วินาที.",  
                                            type: "success",   
                                            showConfirmButton: false,                                    
                                            timer: 2000 
                                            }                   
                         );
                         setTimeout("redirect_page();",1500);
                }else{
                        sweetAlert("Oops..."+data.opt, "Something went wrong!", "error");
                }   
                
            });
            return false; 
        });
      
 
});


function errImage(ele){
        ele.src= "https://www.zombeewatch.org/static/aboutus/noimage.png";
        //alert(ele.src);
}

function redirect_page(){
        window.location = "<?=base_url().$this->router->class?>";
}
      
</script>

    

                    <div class="widget-content">
                        
                        
                        
                    <div class="tabbable">
                        
                        
                        
                        <ul class="nav nav-tabs">
                          <li>
                            <a href="#formcontrols" data-toggle="tab">Manage Form </a>
                          </li>
                        </ul>
                        <br>
                        

                        <div class="tab-content" id='zone-form'>
                                <div class="tab-pane active" id="formcontrols edit-profile form-horizontal">        
                                                            
                                <form enctype="multipart/form-data" action="<?=$url_site?>rest/services/setPersonnel" id='from_personnel' method="post">
                                    <fieldset id="edit-profile" class="form-horizontal">
                                        
                                        
                                        <!-- Start combobox Category -->
                                        <div class="control-group">                                         
                                            <label class="control-label" for="radiobtns">Team</label>
                                            
                                            <div class="controls">
                                                    <div class="btn-group">                                                                                                     
                                                         <select id="team" class="mono" multiple="multiple"  name="team[]"  >
                                                                
                                                        </select>                                                                                   
                                                    </div>
                                             </div> <!-- /controls -->          
                                        </div> <!-- /control-group -->
                                        <!-- End combobox Category -->
                                        
                                        
                                        
                                        <!-- Start Is Edittor  -->
                                        <div class="control-group">                                         
                                            <label class="control-label" for="username">Name</label>
                                            <div class="controls ">
                                                <input class="form-control" id="name" name="name" type="text" class="span6 form-control" id="name"  >                                                                                   
                                            </div> <!-- /controls -->       
                                                                                                    
                                        </div> <!-- /control-group -->
                                        <!-- End Is Edittor  --
                                            
                                        <!-- Start Is Edittor  -->
                                        <div class="control-group">                                         
                                            <label class="control-label" for="username">Surname</label>
                                            <div class="controls ">
                                                <input class="form-control " id="surname" name="surname" type="text" class="span6 form-control"   >                                                                                 
                                            </div> <!-- /controls -->       
                                                                                                    
                                        </div> <!-- /control-group -->
                                        <!-- End Is Edittor  -- >
                                        
                                        
                                        
                                        <!-- Start combobox Category -->
                                        <div class="control-group">                                         
                                            <label class="control-label" for="radiobtns">Gender </label>
                                            
                                            <div class="controls">
                                                    <div class="btn-group">                                                 
                                                         <select id="gender" name="gender"  >
                                                            <option  value="">Select Gender</option>                                                        
                                                             <option  value="ชาย">ชาย</option>
                                                              <option  value="หญิง">หญิง</option>
                                                        </select>                                       
                                                    </div>
                                             </div> <!-- /controls -->          
                                        </div> <!-- /control-group -->
                                        <!-- End combobox Category -->
                                        
                                        
                                        
                                        
                                
                                        
                                        
                                        <div id="show_edit_img">
                                        <div class="control-group">
                                                                                                                    
                                                <label class="control-label" for="firstname">Images</label>
                                                <div class="controls">                                                  
                                                    <input type="file" class="file" accept="image/*" name="picture" />
                                                </div>          
                                            </div> 
                                        </div>
                                        
                                        
                                        <div id="myDIV">
                                        <div class="control-group ">
                                                                                    
                                            <label class="control-label" for="firstname">Images</label>
                                            <div class="controls">
                                                <input type="text" class="form-control" readonly name="picture" id="picture" name="picture" value=""/>  
                                                <img class="img-responsive fadeInRight-animated thumbnail"  accept="image/*" width="30%"  onerror="errImage(this)" id="name_img" src="">
                                                    
                                                    <button type="button" class="btn btn-danger remove_img">Remove</button>
                                            </div>          
                                        </div> 
                                        </div>
                                        
                                        
                                        
                                        
                                        
      
                                        
                                        
                                        <!-- Start combobox Category -->
                                        <div class="control-group">                                         
                                            <label class="control-label" for="radiobtns">Bridth Day </label>                                        
                                                <div class="controls">
                                                    <div class="btn-group">
                                                        <div class='input-group date' id='datetimepicker2'>
                                                            <input type='text' id="birthdate" name="birthdate" class="wat form-control" />
                                                            <span class="input-group-addon">
                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>          
                                        </div> <!-- /control-group -->
                                        <!-- End combobox Category -->
                                        
                                    
                                        <!-- Start Is Edittor  -->
                                        <div class="control-group">                                         
                                            <label class="control-label" for="username">Weight</label>
                                            <div class="controls ">
                                                <input class="form-control" id="weight" name="weight" type="text" class="span6 form-control" id="weight" value="" >                                                                                 
                                            </div> <!-- /controls -->       
                                                                                                    
                                        </div> <!-- /control-group -->
                                        <!-- End Is Edittor  -- >
                                            
                                            
                                        
                                        <!-- Start Is Edittor  -->
                                        <div class="control-group">                                         
                                            <label class="control-label" for="username">Hight</label>
                                            <div class="controls ">
                                                <input class="form-control" id="hight" name="hight" type="text" class="span6 form-control" id="hight" value="" >                                                                                    
                                            </div> <!-- /controls -->       
                                                                                                    
                                        </div> <!-- /control-group -->
                                        <!-- End Is Edittor  -- >
                                            
                                            
                                            
                                        <!-- Start combobox Category -->
                                        <div class="control-group">                                         
                                            <label class="control-label" for="radiobtns">First Nationality</label>
                                            
                                            <div class="controls">
                                                    <div class="btn-group">                                                 
                                                         <select class="first_nationality_id" id='first_nationality' name="first_nationality_id"  >
                                                            <option value="">Select-First Nationality</option>  
                                                        </select>                                       
                                                    </div>
                                             </div> <!-- /controls -->          
                                        </div> <!-- /control-group -->
                                        <!-- End combobox Category -->
                                        
                                        
                                        
                                        <!-- Start combobox Category -->
                                        <div class="control-group">                                         
                                            <label class="control-label" for="radiobtns">Second Nationality</label>
                                            
                                            <div class="controls">
                                                    <div class="btn-group">   
                                                                                                    
                                                         <select class="second_nationality" id='second_nationality' name="nationality"  >
                                                             <option value="">Select-Second Nationality</option>    
                                                        </select>   
                                                                                            
                                                    </div>
                                             </div> <!-- /controls -->          
                                        </div> <!-- /control-group -->
                                        <!-- End combobox Category -->
                                        
                                            
                                        <!-- Start combobox Category -->
                                        <div class="control-group">                                         
                                            <label class="control-label" for="radiobtns">Country</label>
                                            
                                            <div class="controls">
                                                    <div class="btn-group">                                                 
                                                         <select class="country" id="country" name="country"  >
                                                             <option value="">Select-Country</option>                                                                                       
                                                        
                                                        </select>                                       
                                                    </div>
                                             </div> <!-- /controls -->          
                                        </div> <!-- /control-group -->
                                        <!-- End combobox Category -->
                                        
                                        <!-- Start Is Edittor  -->
                                        <div class="control-group">                                         
                                            <label class="control-label" for="username">Zipcode</label>
                                            <div class="controls ">
                                                <input class="form-control" id="zipcode" name="zipcode" type="text" class="span6 form-control" >                                                                                    
                                            </div> <!-- /controls -->       
                                        </div> <!-- /control-group -->
                                        <!-- End Is Edittor  -- >
                                            
                                        <!-- Start Is Edittor  -->
                                        <div class="control-group">                                         
                                            <label class="control-label" for="username">Mobile</label>
                                            <div class="controls ">
                                                <input class="form-control" id="telephone_mobile" name="telephone_mobile" type="text" class="span6 form-control"   >                                                                                    
                                            </div> <!-- /controls -->       
                                                                                                    
                                        </div> <!-- /control-group -->
                                        <!-- End Is Edittor  -- >
                                                
                                                
                                        <!-- Start Is Edittor  -->
                                        <div class="control-group">                                         
                                            <label class="control-label" for="username">Address</label>
                                            <div class="controls ">
                                                <textarea class="form-control" rows="5" id="address" name="address"></textarea>                                                                             
                                            </div> <!-- /controls -->       
                                                                                                    
                                        </div> <!-- /control-group -->
                                        <!-- End Is Edittor  -- >
                                                    
                                                
                                        <!-- Start Is Edittor  -->
                                        <div class="control-group">                                         
                                            <label class="control-label" for="username">Email</label>
                                            <div class="controls ">
                                                <input class="form-control" id="email" name="email" type="text" class="span6 form-control" id="firstname" value="" >                                                                                    
                                            </div> <!-- /controls -->       
                                                                                                    
                                        </div> <!-- /control-group -->
                                        <!-- End Is Edittor  -- >
                                            
                                            
                                            
                                        <!-- Start Is Edittor  -->
                                        <div class="control-group">                                         
                                            <label class="control-label" for="username">Line id</label>
                                            <div class="controls ">
                                                <input class="form-control" id="lineid" name="lineid" type="text" class="span6 form-control" id="lineid" value="" >                                                                                 
                                            </div> <!-- /controls -->       
                                                                                                    
                                        </div> <!-- /control-group -->
                                        <!-- End Is Edittor  -- >   
                                            
                                            
                                                
                                        <!-- Start combobox Category -->
                                        <div class="control-group">                                         
                                            <label class="control-label" for="radiobtns">Role</label>
                                            
                                            <div class="controls">
                                                    <div class="btn-group">                                                 
                                                         <select class="role" id='role' name="role"  >
                                                             <option value="">Select-Role</option>  
                                                        </select>                                       
                                                    </div>
                                             </div> <!-- /controls -->          
                                        </div> <!-- /control-group -->
                                        <!-- End combobox Category -->
                                        
                                            
                                        
                                        <div id="Show_Football_Player" style='display:none;'>
                                                <!-- Start combobox Category -->
                                                <div class="control-group">                                         
                                                    <label class="control-label" for="radiobtns">Game Status</label>
                                                    
                                                    <div class="controls">
                                                            <div class="btn-group">                                                 
                                                                 <select class="role" id='game_status_id' name="game_status_id"  >
                                                                     <option value="">Select-Game Status</option>   
                                                                </select>                                       
                                                            </div>
                                                     </div> <!-- /controls -->          
                                                </div> <!-- /control-group -->
                                                <!-- End combobox Category -->
                                                
                                            
                                                
                                                
                                                <!-- Start combobox Category -->
                                                <div class="control-group">                                         
                                                    <label class="control-label" for="radiobtns">Is Asia Squad</label>
                                                    
                                                    <div class="controls">
                                                            <div class="btn-group">                                                 
                                                                 <select class="role" id='is_asia_squad' name="is_asia_squad"  >
                                                                     <option value="">Select-Is Asia Squad</option>
                                                                     <option value="1">Yes</option> 
                                                                     <option value="0">No</option>      
                                                                </select>                                       
                                                            </div>
                                                     </div> <!-- /controls -->          
                                                </div> <!-- /control-group -->
                                                <!-- End combobox Category -->
                                                
                                                <!-- Start combobox Category -->
                                                <div class="control-group">                                         
                                                    <label class="control-label" for="radiobtns">Uniform Number</label>
                                                    
                                                    <div class="controls">
                                                            <input class="form-control" id="unifrom_number" name="unifrom_number" type="text" class="span6 form-control"  value="" >                                                                                    
                                                     </div> <!-- /controls -->          
                                                </div> <!-- /control-group -->
                                                <!-- End combobox Category -->
                                                
                                                
                                                <!-- Start combobox Category -->
                                                <div class="control-group">                                         
                                                    <label class="control-label" for="radiobtns">Frist Position</label>
                                                    
                                                    <div class="controls">
                                                            <div class="btn-group">                                                 
                                                                 <select class="role" id='first_position_id' name="first_position_id"  >
                                                                        <option value="">Select-First Position</option> 
                                                                </select>                                       
                                                            </div>
                                                     </div> <!-- /controls -->          
                                                </div> <!-- /control-group -->
                                                <!-- End combobox Category -->
                                                
                                                
                                                <!-- Start combobox Category -->
                                                <div class="control-group">                                         
                                                    <label class="control-label" for="radiobtns">Second Position</label>
                                                    
                                                    <div class="controls">
                                                            <div class="btn-group">                                                 
                                                                 <select class="role" id='second_position' name="second_position"  >
                                                                        <option value="">Select-Second Position</option>    
                                                                </select>                                       
                                                            </div>
                                                     </div> <!-- /controls -->          
                                                </div> <!-- /control-group -->
                                                <!-- End combobox Category -->
                                                
                                                
                                                
                                                
                                                
                                        </div>
                                        
                                        
                                        
                                        <!-- Start Is Edittor  -->
                                        <div class="control-group">                                         
                                            <label class="control-label" for="username">Passport NO</label>
                                            <div class="controls ">
                                                <input class="form-control" id="passport_no" name="passport_no" type="text" class="span6 form-control"  value="" >                                                                                  
            
                                            </div> <!-- /controls -->                                                               
                                        </div> <!-- /control-group -->
                                        <!-- End Is Edittor  -->
                                        
                                        
                                            <!-- Start Is Edittor  -->
                                        <div class="control-group">                                         
                                            <label class="control-label" for="username">Passport Expired Date</label>
                                            <div class="controls ">
                                                <div class='input-group date' id='datetimepicker2'>
                                                            <input type='text' name="passport_expired_date" id="passport_expired_date" class="wat form-control" />
                                                            <span class="input-group-addon">
                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>
                                                        </div>
                                            </div> <!-- /controls -->                                                               
                                        </div> <!-- /control-group -->
                                        <!-- End Is Edittor  -->
                                        
                                        
                                        
                                        
                                        
                                        <!-- Start Is Edittor  -->
                                        <div class="control-group">                                         
                                            <label class="control-label" for="username">Visa NO</label>
                                            <div class="controls ">
                                                <input class="form-control" id="visa_no" name="visa_no" type="text" class="span6 form-control"  value="" >                                                                                  
            
                                            </div> <!-- /controls -->                                                               
                                        </div> <!-- /control-group -->
                                        <!-- End Is Edittor  -->
                                        
                                        <!-- Start Is Edittor  -->
                                        <div class="control-group">                                         
                                            <label class="control-label" for="username">Visa Expired Date</label>
                                            <div class="controls ">
                                                <div class='input-group date' id='datetimepicker2'>
                                                            <input type='text' name="visa_expired_date" id="visa_expired_date" class="wat form-control" />
                                                            <span class="input-group-addon">
                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>
                                                        </div>
                                            </div> <!-- /controls -->                                                               
                                        </div> <!-- /control-group -->
                                        <!-- End Is Edittor  -->
                                        
                                    
                                    
                                        <!-- Start combobox Category -->
                                        <div class="control-group">                                         
                                            <label class="control-label" for="radiobtns">Status</label>
                                            
                                            <div class="controls">
                                                    <div class="btn-group">                                                 
                                                         <select class="status" name="status"  id="status" >
                                                             <option value="">Select-Status</option>                                                                                        
                                                             <option  value="1">Active</option>
                                                             <option  value="0">Un Active</option>
                                                        </select>                                       
                                                    </div>
                                             </div> <!-- /controls -->          
                                        </div> <!-- /control-group -->
                                        <!-- End combobox Category -->
                                            
                                    
                                        
                                        <input class="form-control" id="club_id" name="club_id" type="hident" class="span6 form-control"  value="<?=$get_club_id?>"  style="display: none;">
                                        
                                        <input class="form-control" id="personnel_id"  name="personnel_id"  class="span6 form-control"  style="display: none;"> 
                                        <div class="form-actions">
                                            <input type="submit" class="btn btn-primary" value="Submit">
                                            
                                        </div> <!-- /form-actions -->
                                        
                                    </fieldset>
                                </form>
                                        
                                </div>

                            </div> 
                          
                          
                        </div>
                        
                        
                        
                        
                        
                    </div> <!-- /widget-content -->
    
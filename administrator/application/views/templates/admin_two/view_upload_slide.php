	<?php
		 if($this->session->userdata('save_slide') == "YES"){
		 	$this->session->unset_userdata('save_slide');
	?>
			<script>
				swal({
                        title: "บันทึกข้อมูลเรียบร้อยแล้ว", 
                        text: "ผมกำลังปิดตัวเองใน  2 วินาที.",  
                        type: "success",   
                        showConfirmButton: false,                                    
                        timer: 2000 
                     }                   
                  );

			</script>
	<?php
			 
		 }else if($this->session->userdata('save_slide')!=null){
		 	
	?>
		 	<script>
		 		sweetAlert("Oops..<?=$this->session->userdata('save_slide')?>", "Something went wrong!", "error");
			</script>
		 	
	<?php	$this->session->unset_userdata('save_slide');
		 }
		 	
	?>
	
	
<script>
	
	function id_category(id){
		
		$.post("<?=base_url().$this->router->class.'/edit_slide/'?>",{'id':id}).done(function(){
			window.location = "<?=base_url().$this->router->class.'/edit_slide/'?>";
		});	
		
	}
	
	
	function myFunction_del(id_del,img){
		
		swal({   	title: "Are you sure?",   
					text: "You will not be able to recover this imaginary file!",   
					type: "warning",   
					showCancelButton: true,   
					confirmButtonColor: "#DD6B55",   
					confirmButtonText: "Yes, delete it!",   
					closeOnConfirm: false }, 
			
		function(){   				
			$.post("<?=base_url().$this->router->class.'/del_slide_img/'?>",{'id_del':id_del,'img':img}).done(
						function(data){
							if(data=="TRUE"){
								swal("Deleted!", "Your imaginary file has been deleted.", "success");								
								setTimeout("alertWelcome();",1000);										
							}else{
								sweetAlert("Oops...Error "+data+"", "Something went wrong!", "error");
							}
							
						}
			);

		});
		
	}
	
	function alertWelcome(){

		window.location = "<?=base_url().$this->router->class.'/slide/'?>";

	}
	
	
	$(document).ready(function() {
    	$('#example2').DataTable( {
        	"order": [[ 3, "asc" ]]
    	} );
	});

</script>

                    <div class="widget-content">
                        
						<section>
                    		<ul>
                    			<center>
                    			
                    				<a href="<?=base_url().$this->router->class?>/add_slide" class="btn btn-info btn-md">
									    <span class="glyphicon glyphicon-plus"></span> Add Slide Images
									</a>	
									
								</center>
								
                    		</ul>
                    	</section>
                    	
                                <table id="example2" class="table  hover table-bordered" ALIGN=CENTER  cellspacing="0" width="100%">
                                        <thead>
                                    <tr>
                                    	<th><center>Preview</center></th>
                                        <th><center>Name</center></th>
                                        <th><center>Link</center></th>
                                        <th><center>Order No</center></th>
                                        <th><center>Status</center></th>
                                        <th><center>Edit</center></th>
                                        <th><center>Delete</center></th>
                                    </tr>
                                </thead>

                                        <tbody>
                                            
                                    <?php
                                        
                                            foreach ($slides as $value) {
                                    ?>
                                            <tr>
                                            	  
                                            	
                                                <td  data-label="Images:" style="width:150px;">
                                                	<br><img  style="width:150px;height:55px;" class='thumbnail' u='image' src='<?php echo base_url().'../public/upload_slide_images/'.$value['src_paht'];?>' style="width:200px;"/>
                                                </td>
                                                <td  data-label="Name:" style="width:350px;"><center><br><br><?=$value['src_paht']?></center></td>    
                                                <td  data-label="Link:" ><center><br><br><?=$value['link']?></center></td>    
                                                <td  data-label="Order No:" style="width:80px;"><center><br><br><?=$value['order_no']?></center></td>
                                                  <?php
                                                	if($value['status'] == "Active"){
                                                		$bg= "66CC99";
                                                	}else{
                                                		$bg= "BEBEBE";
                                                	}
												?>
                                                <td  data-label="Status:" style="background: #<?=$bg?>; width:50px;" ><center><br><br><?=$value['status']?></center></td>
                                                <td data-label="Edit:" style="width:70px; background: #FFFFCC;" onclick="id_category(<?=$value['id']?>);">
                                                    <center>
                                                        <br><br><img src="https://cdn3.iconfinder.com/data/icons/fatcow/16/application_form_edit.png" />
                                                    </center>
                                                </td>
                                                <?php
                                                		$a= $value['id'];
														$b= $value['src_paht'];
                                                	
                                                ?>
                                                <!-- <td data-label="Edit:"><center><? echo anchor("home/del_category/".$value['id'],'<img src="https://cdn3.iconfinder.com/data/icons/softwaredemo/PNG/16x16/Close_Box_Red.png" />',array("onclick"=>"javascript:return confirm('คุณต้องการลบใช่หรือไม่ ?');"));?></center></td> --> 
                                            	<td data-label="Delete:" style="width:20px; background: #FF9999;" onclick="myFunction_del(<?=$a?>,'<?=$b?>')" >
                                                	<!-- <center>
                                                		<? echo anchor("home/del_sub_category/".$value['id'],'<img src="https://cdn3.iconfinder.com/data/icons/softwaredemo/PNG/16x16/Close_Box_Red.png" />',array("onclick"=>"javascript:return confirm('คุณต้องการลบใช่หรือไม่ ?');"));?>
                                                	</center> -->
                                                	
                                                	
                                                	<center>
                                                        <br><br><img src="https://cdn3.iconfinder.com/data/icons/softwaredemo/PNG/16x16/Close_Box_Red.png">
                                                   </center>
                                                </td>     
                                            
                                            </tr>   
                                    <?php   }
                                        
                                        
                                        
                                    ?>
                                          
                                        </tbody>
                                </table>
        
                    </div> <!-- /pricing-plans -->      
    
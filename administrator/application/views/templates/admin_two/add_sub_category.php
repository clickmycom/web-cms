<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Title</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
    <link href="<?=$template_paht?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=$template_paht?>css/Copy of bootstrap.min.css" rel="stylesheet">
<link href="<?=$template_paht?>css/bootstrap-responsive.min.css" rel="stylesheet">
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600"
        rel="stylesheet">
<link href="<?=$template_paht?>css/font-awesome.css" rel="stylesheet">
<link href="<?=$template_paht?>css/style.css" rel="stylesheet">
<link href="<?=$template_paht?>css/pages/dashboard.css" rel="stylesheet">
<link rel="stylesheet"  type="text/css" href="<?=$template_paht?>easyui/css/easyui.css">
<link rel="stylesheet"  type="text/css" href="<?=$template_paht?>easyui/css/icon.css">
<link href="<?=$template_paht?>easyui/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />

<script type="text/javascript" 	src="<?=$template_paht?>easyui/js/jquery.min.js"></script>
<script type="text/javascript" 	src="<?=$template_paht?>easyui/js/jquery.easyui.min.js"></script>
<script type="text/javascript" 	src="<?=$template_paht?>easyui/js/bootstrap-validator.js"></script>
<script src="<?=$template_paht?>js/tinymce/tinymce.min.js"></script>

<script src="<?=$template_paht?>easyui/js/fileinput.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?=$template_paht?>js/bootstrap.min.js"></script>


<link rel="stylesheet"  		type="text/css" href="<?=$template_paht?>sweetalert/sweetalert.css">
<script type="text/javascript"  src="<?=$template_paht?>sweetalert/sweetalert.min.js"></script>

<script type="text/javascript" src="<?=$template_paht?>js/bootstrap-number-input.js"></script>


</head>
	
<body>

<style>

@media ( max-width: 585px ) {
    .input-group span.input-group-btn,.input-group input,.input-group button{
        display: block;
        width: 100%;
        border-radius: 0;
        margin: 0;
    }
    .input-group {
        position: relative;   
    }
    .input-group span.data-up{
        position: absolute;
        top: 0;
    }
    .input-group span.data-dwn{
        position: absolute;
        bottom: 0;
    }
    .form-control.text-center {
        margin: 34px 0;
    }
    .input-group-btn:last-child>.btn, .input-group-btn:last-child>.btn-group{
        margin-left:0;
    }

}
</style>
			

<script type="text/javascript">
	tinymce.init({
	    selector: "textarea",
	    plugins: [
	        "advlist autolink lists link image charmap print preview anchor",
	        "searchreplace visualblocks code fullscreen",
	        "insertdatetime media table contextmenu paste"
	    ],
	    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
	});
</script>


<script>
	$(function(){
		
		$('form').submit(function(){
		         
		        $.post('<?=base_url().$this->router->class.'/save_subcategory'?>',$("form").serialize(),function( data ){
		             if(data.opt=='YES'){
		                 swal({   
		                                    title: "บันทึกข้อมูลเรียบร้อยแล้ว", 
		                                    text: "ผมกำลังปิดตัวเองใน  2 วินาที.",  
		                                    type: "success",   
		                                    showConfirmButton: false,                                    
		                                    timer: 2000 
		                                    }                   
		                 );
		                 setTimeout("redirect_page();",1500);	
		             }else{
		                sweetAlert("Oops..."+data.opt, "Something went wrong!", "error");
		             }	             
		        },"json");
		        return false;  
		    });
		    // end form
		    
		 
		 $('#is_singpage').change(function(){
			 var ch = $(this).children(":selected").text();
			 if(ch=="Yes"){
			 	 $('#number_page').slideDown();
			 	 $('#link').val("category_single");
			 	 $('#link').attr('readonly', true);
			 	 $('#ar_id').prop('required',true);
			 	 
			 }else{
			 	$('#number_page').slideUp();
			 	$('#link').attr('readonly', false);
			 	$('#link').val(null);
			 	$('#ar_id').prop('required',false);
			 }
		});
		
		
	});
	
	
	function redirect_page(){
		window.location = "<?=base_url().$this->router->class.'/add_sub_category'?>";
	}
		
</script>

<? $this->load->view('layouts/header.php')?> <!-- header.php -->
<? $this->load->view('layouts/menu.php')?>  <!-- menu.php -->
    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Add Sub Category</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
					<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						
						      <a href="#formcontrols" data-toggle="tab">Sub Category Form</a>
						  </li>
						</ul>
						<br>
						
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols edit-profile form-horizontal">		
															
							
							 
								<form method="post">
									<fieldset id="edit-profile" class="form-horizontal">
										<!-- Start combobox Category -->
										
										<!-- Start combobox Category -->
										<div class="control-group">											
											<label class="control-label" for="radiobtns">Category</label>
											
                                            <div class="controls">
	                                              <div class="btn-group">          	                                   	
			                                             <select class="category" name="category"  required>
														  				<option value="">Select-Category</option>

														  	<?php
			                                              			foreach ($category as $value) {
															?>																														
																		<option id="<?=$value['id']?>" value="<?=$value['id']?>"><?=$value['name']?></option>
																			 											
															<?php		  
																	}
			                                              	
			                                              	?>
			                                              	
														</select>										
	                                            	</div>
                                             </div>	<!-- /controls -->			
										</div> <!-- /control-group -->
										<!-- End combobox Category -->
										
										
										
										<!-- Start Is Edittor  -->
										<div class="control-group">											
											<label class="control-label" for="username">Name</label>
											<div class="controls ">
												<input class="form-control" id="username" name="name" type="text" class="span6 form-control" id="firstname" value="" required>																					
											</div> <!-- /controls -->																
										</div> <!-- /control-group -->
										<!-- End Is Edittor  -->
										
										
											<!-- Start Is Edittor  -->
										<div class="control-group">											
											<label class="control-label" >Title</label>
											<div class="controls ">
												<input placeholder="คำอธิบาย" class="form-control" id="title" name="title" type="text" class="span6 form-control"  >																					
												
											</div> <!-- /controls -->																
										</div> <!-- /control-group -->
										<!-- End Is Edittor  -->
										
										<div class="control-group">											
											<label class="control-label" for="radiobtns">Is Menu</label>
											
                                            <div class="controls">
	                                              <div class="btn-group">          	                                   	
			                                             <select class="category" name="is_menu"  required>
														  				<option value="">Select Is Menu</option>

														  	<?php
			                                              			foreach ($is_menu as $value) {
															?>																														
																		<option ><?=$value?></option>
																			 											
															<?php		  
																	}
			                                              	
			                                              	?>
			                                              	
														</select>										
	                                            	</div>
                                             </div>	<!-- /controls -->			
										</div> <!-- /control-group -->
										<!-- End combobox Category -->
										
										
											<div class="control-group">											
											<label class="control-label" for="radiobtns">Is Single Page</label>
											
                                            <div class="controls">
	                                              <div class="btn-group">          	                                   	
			                                             <select class="category" name="is_singpage" id="is_singpage" required>
														  				<option value="">Select Is Single Page</option>

														  	<?php
			                                              			foreach ($is_single_page as $value) {
															?>																														
																		<option ><?=$value?></option>
																			 											
															<?php		  
																	}
			                                              	
			                                              	?>
			                                              	
														</select>										
	                                            	</div>
                                             </div>	<!-- /controls -->			
										</div> <!-- /control-group -->
										<!-- End combobox Category -->
										
										
										<div id="number_page" class="control-group" style="display: none;">
											<label class="control-label" for="username">Article ID</label>
											<div class="controls ">
												<input class="form-control" name="number_page" type="text" class="span6 form-control" value="<?=$id_copy_article?>" id="ar_id" style="width: 80px;">																					
											</div> <!-- /controls -->	
										</div>	
									
									
									
										<!-- Start Is Edittor  -->
										<div class="control-group">											
											<label class="control-label" for="username">Link</label>
											<div class="controls ">
												<input class="form-control" id="link" name="Link" type="text" class="span6 form-control" id="firstname" style="width: 210px;" >																					
											</div> <!-- /controls -->																
										</div> <!-- /control-group -->
										<!-- End Is Edittor  -->
										
										
								
										<!-- Sprintner -->
										<div class="control-group">											
											<label class="control-label" >Order NO</label>
											<div class="controls ">

																<div class="input-group col-xs-2">
																	<input id="colorful" class="form-control" type="number" name="oder_no" value="1" min="1" max="1000000" />
																</div>

											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										<!-- Sprintner-->
										
								
										
										<!-- Start Is Homepage Sub-Category -->
										<div class="control-group">											
											<label class="control-label" for="radiobtns">Status</label>
											
                                            <div class="controls">
	                                              <div class="btn-group">                                             	
			                                             <select name="status" required>
														  				<option value="">Select-Status</option>
														
																		<?php
																			foreach ($is_homepage_status as $value) {
																		?>
																			<option ><?=$value?></option>
																		<?php		
																			}
																		
																		?>			 											
														
														</select>										
	                                            	</div>
                                             </div>	<!-- /controls -->			
										</div> <!-- /control-group -->
										<!-- End Is Homepage Sub-Category -->
										
										
										
										
										
										
										
										
										<input style="visibility: hidden;" type="text" id="url1" value="<?php echo base_url(); ?>"/>
										<div class="form-actions">
											<input type="submit" name="btnsave" class="btn btn-primary" value="Submit"> 
										
										</div> <!-- /form-actions -->
										<hr>
									</fieldset>
								</form>

								</div>

							</div> 
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    

<? $this->load->view('layouts/lower_panel.php');?> <!-- lower_panel.php -->
<? $this->load->view('layouts/footer.php');?><!-- footer.php -->


<script>

// Remember set you events before call bootstrapSwitch or they will fire after bootstrapSwitch's events
$("[name='checkbox2']").change(function() {
	if(!confirm('Do you wanna cancel me!')) {
		this.checked = true;
	}
});

$('#after').bootstrapNumber();
$('#colorful').bootstrapNumber({
	upClass: 'success',
	downClass: 'danger'
});
</script>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<script src="<?=$template_paht?>js/bootstrap.js"></script>
<script src="<?=$template_paht?>js/base.js"></script>


</body>
</html>

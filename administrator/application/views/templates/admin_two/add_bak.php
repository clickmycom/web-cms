<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Title</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
    <link href="<?=$template_paht?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=$template_paht?>css/Copy of bootstrap.min.css" rel="stylesheet">
<link href="<?=$template_paht?>css/bootstrap-responsive.min.css" rel="stylesheet">
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600"
        rel="stylesheet">
<link href="<?=$template_paht?>css/font-awesome.css" rel="stylesheet">
<link href="<?=$template_paht?>css/style.css" rel="stylesheet">
<link href="<?=$template_paht?>css/pages/dashboard.css" rel="stylesheet">
<link rel="stylesheet"  type="text/css" href="<?=$template_paht?>easyui/css/easyui.css">
<link rel="stylesheet"  type="text/css" href="<?=$template_paht?>easyui/css/icon.css">
<link href="<?=$template_paht?>easyui/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />

<script type="text/javascript" 	src="<?=$template_paht?>easyui/js/jquery.min.js"></script>
<script type="text/javascript" 	src="<?=$template_paht?>easyui/js/jquery.easyui.min.js"></script>
<script type="text/javascript" 	src="<?=$template_paht?>easyui/js/bootstrap-validator.js"></script>
<script src="<?=$template_paht?>js/tinymce/tinymce.min.js"></script>

<script src="<?=$template_paht?>easyui/js/fileinput.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?=$template_paht?>js/bootstrap.min.js"></script>


<link rel="stylesheet"  		type="text/css" href="<?=$template_paht?>sweetalert/sweetalert.css">
<script type="text/javascript"  src="<?=$template_paht?>sweetalert/sweetalert.min.js"></script>

</head>
	
<body>

<style>
	
.dropdown-submenu {
    position: relative;
}

.dropdown-submenu>.dropdown-menu {
    top: 0;
    left: 100%;
    margin-top: -6px;
    margin-left: -1px;
    -webkit-border-radius: 0 6px 6px 6px;
    -moz-border-radius: 0 6px 6px;
    border-radius: 0 6px 6px 6px;
}

.dropdown-submenu:hover>.dropdown-menu {
    display: block;
}

.dropdown-submenu>a:after {
    display: block;
    content: " ";
    float: right;
    width: 0;
    height: 0;
    border-color: transparent;
    border-style: solid;
    border-width: 5px 0 5px 5px;
    border-left-color: #ccc;
    margin-top: 5px;
    margin-right: -10px;
}

.dropdown-submenu:hover>a:after {
    border-left-color: #fff;
}

.dropdown-submenu.pull-left {
    float: none;
}

.dropdown-submenu.pull-left>.dropdown-menu {
    left: -100%;
    margin-left: 10px;
    -webkit-border-radius: 6px 0 6px 6px;
    -moz-border-radius: 6px 0 6px 6px;
    border-radius: 6px 0 6px 6px;
}
</style>
			

<script type="text/javascript">
	tinymce.init({
	    selector: "textarea",
	    plugins: [
	        "advlist autolink lists link image charmap print preview anchor",
	        "searchreplace visualblocks code fullscreen",
	        "insertdatetime media table contextmenu paste"
	    ],
	    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
	});
</script>


<script type="text/javascript">
		function myformatter(date){
			var y = date.getFullYear();
			var m = date.getMonth()+1;
			var d = date.getDate();
			return y+'-'+(m<10?('0'+m):m)+'-'+(d<10?('0'+d):d);
		}
		function myparser(s){
			if (!s) return new Date();
			var ss = (s.split('-'));
			var y = parseInt(ss[0],10);
			var m = parseInt(ss[1],10);
			var d = parseInt(ss[2],10);
			if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
				return new Date(y,m-1,d);
			} else {
				return new Date();
			}
		}
</script>
	

<script>
	
$(function(){


	
	
	 $('.category').change(function(){
	 	 var id_detail = $(this).children(":selected").attr("id");
            	
                    var url1=$('#url1').val()+'index.php/home/onchang_category';
                      $('#load_sub').load(url1,{
                        	id_detail:id_detail                       
                      });
      });
	
	
		
});
	
</script>
	
<?
		if($success == TRUE){
?>
			<script>		
				swal({   
									title: "บันทึกข้อมูลเรียบร้อยแล้ว", 
									text: "ผมกำลังปิดตัวเองใน  2 วินาที.",  
									type: "success",
									timer: 2000,   
									showConfirmButton: false						 
									
									}					
						);
			</script>
<?		}
			
?>	
	
<? $this->load->view('layouts/header.php')?> <!-- header.php -->
<? $this->load->view('layouts/menu.php')?>  <!-- menu.php -->
    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Add Article</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
					<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Article Form</a>
						  </li>
						</ul>
						<br>
						
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols edit-profile form-horizontal">		
															
							
							 
								<?php echo  form_open_multipart('home/save_userdata')?>
									<fieldset id="edit-profile" class="form-horizontal">
										<!-- Start combobox Category -->
										<div class="control-group">											
											<label class="control-label" for="radiobtns">Category</label>
											
                                            <div class="controls">
	                                              <div class="btn-group">          	                                   	
			                                             <select class="category" name="category"  required>
														  				<option value="">Select-Category</option>

														  	<?php
			                                              			foreach ($category as $value) {
															?>																														
																		<option id="<?=$value['id']?>" value="<?=$value['id']?>"><?=$value['name']?></option>
																			 											
															<?php		  
																	}
			                                              	
			                                              	?>
			                                              	
														</select>										
	                                            	</div>
                                             </div>	<!-- /controls -->			
										</div> <!-- /control-group -->
										<!-- End combobox Category -->
										
										
											
																				
										<!-- Start combobox Sub-Category -->
										<div class="control-group">											
											<label class="control-label" for="radiobtns">Sub Category</label>
											<div id="load_sub">	
	                                            <div class="controls">
		                                              <div class="btn-group load_sub">                                             	
				                                             <select name="sub_category" required>
				                                             	
															  				<option value="">Select-Sub Category</option>
															  				
															  					
															  	<?php
				                                              			foreach ($sub_category as $value) {
																?>												
																		
																			<option value="<?=$value['id']?>"><?=$value['name']?></option>			 											
																<?php		  
																		}
				                                              	
				                                              	?>
				                                             
															</select>										
		                                            	</div>
	                                             </div>	<!-- /controls -->	
                                             </div>		
										</div> <!-- /control-group -->
										<!-- End combobox Sub-Category -->
										
										
										<div class="control-group">											
											<label class="control-label" for="firstname">Images</label>
											<div class="controls">
												
												<input type="file" class="file" name="userfile" required/>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										<!-- Start Is Edittor  -->
										<div class="control-group">											
											<label class="control-label" for="username">Name</label>
											<div class="controls ">
												<input class="form-control" id="username" name="name" type="text" class="span6 form-control" id="firstname" value="" required>																					
													
													    <textarea class="form-control" name="comment" style="width:100%;height: 100%;" ></textarea>
													
											</div> <!-- /controls -->																
										</div> <!-- /control-group -->
										<!-- End Is Edittor  -->
										

									

										<!-- Start Is Homepage Sub-Category -->
										<div class="control-group">											
											<label class="control-label" for="radiobtns">Is Homepage</label>
											
                                            <div class="controls">
	                                              <div class="btn-group">                                             	
			                                             <select name="is_homepage" required>
														  				<option value="">Select-Is Homepage</option>
														
																		<?php
																			foreach ($is_homepage as $value) {
																		?>
																			<option ><?=$value?></option>
																		<?php		
																			}
																		
																		?>			 											
														
														</select>										
	                                            	</div>
                                             </div>	<!-- /controls -->			
										</div> <!-- /control-group -->
										<!-- End Is Homepage Sub-Category -->
										
									
										
										<div class="control-group">											
											<label class="control-label" for="firstname">Show Date</label>
											<div class="controls">
												<input name="show_date" class="easyui-datebox" data-options="formatter:myformatter,parser:myparser"></input>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										
										<div class="control-group">											
											<label class="control-label" for="firstname">End Date</label>
											<div class="controls">
												<input name="end_date" class="easyui-datebox" data-options="formatter:myformatter,parser:myparser" ></input>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										

										
										<!-- Start Is Homepage Sub-Category -->
										<div class="control-group">											
											<label class="control-label" for="radiobtns">Status</label>
											
                                            <div class="controls">
	                                              <div class="btn-group">                                             	
			                                             <select name="status" required>
														  				<option value="">Select-Status</option>
														
																		<?php
																			foreach ($is_homepage_status as $value) {
																		?>
																			<option ><?=$value?></option>
																		<?php		
																			}
																		
																		?>			 											
														
														</select>										
	                                            	</div>
                                             </div>	<!-- /controls -->			
										</div> <!-- /control-group -->
										<!-- End Is Homepage Sub-Category -->
										
										
										
										
										
										
										
										
										<input style="visibility: hidden;" type="text" id="url1" value="<?php echo base_url(); ?>"/>
										<div class="form-actions">
											<button type="submit" class="btn btn-primary">Save</button> 
											
										</div> <!-- /form-actions -->
										<hr>
									</fieldset>
								<?php echo form_close();?>

								</div>

							</div> 
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    

<? $this->load->view('layouts/lower_panel.php');?> <!-- lower_panel.php -->
<? $this->load->view('layouts/footer.php');?><!-- footer.php -->


	
<script src="<?=$template_paht?>js/bootstrap.js"></script>
<script src="<?=$template_paht?>js/base.js"></script>


</body>
</html>

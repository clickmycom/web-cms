<style>

@media ( max-width: 585px ) {
    .input-group span.input-group-btn,.input-group input,.input-group button{
        display: block;
        width: 100%;
        border-radius: 0;
        margin: 0;
    }
    .input-group {
        position: relative;   
    }
    .input-group span.data-up{
        position: absolute;
        top: 0;
    }
    .input-group span.data-dwn{
        position: absolute;
        bottom: 0;
    }
    .form-control.text-center {
        margin: 34px 0;
    }
    .input-group-btn:last-child>.btn, .input-group-btn:last-child>.btn-group{
        margin-left:0;
    }

}
</style>
			
		
<script type="text/javascript">
	tinymce.init({
	    selector: "textarea",
	    plugins: [
	        "advlist autolink lists link image charmap print preview anchor",
	        "searchreplace visualblocks code fullscreen",
	        "insertdatetime media table contextmenu paste"
	    ],
	    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
	});
</script>



<script>
	$(function(){
		
		$('form').submit(function(){        
		        $.post('<?=$url_site.'rest/services/setContentTeam'?>',$("form").serialize(),function( data ){
		        	
		        	if(data.opt=="YES"){
		        		 swal({   
                                title: "บันทึกข้อมูลเรียบร้อยแล้ว", 
                                text: "ผมกำลังปิดตัวเองใน  2 วินาที.",  
                                type: "success",   
                                showConfirmButton: false,                                    
                                timer: 2000 
                                }                   
		                 );
		                   setTimeout("redirect_page();",1500);	
		        	}else{
		        		sweetAlert("Oops..."+data.opt, "Something went wrong!", "error");
		        	}
		        },"json");
		        return false;  
		    });
		    // end form
		
	});
	
	function redirect_page(){
		window.location = "<?=base_url().$this->router->class.'/add_team'?>";
	}
		
</script>



					<div class="widget-content">
						
						
						
					<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						
						    <a href="#formcontrols" data-toggle="tab">Add Team Form</a>
						  </li>
						</ul>
						<br>
						
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols edit-profile form-horizontal">		
															
							
							 
								<form method="post">
								
									<fieldset id="edit-profile" class="form-horizontal">
										<!-- Start combobox Category -->
										
										
										
										<br>	
		
										<!-- Start Is Edittor  -->
										<div class="control-group">											
											<label class="control-label" for="username">Name</label>
											<div class="controls ">
												<input class="form-control" id="username" name="name" type="text" class="span6 form-control" id="firstname" value="" required>																					
											</div> <!-- /controls -->																
										</div> <!-- /control-group -->
										<!-- End Is Edittor  -->
										
										
										
									
										<!-- Start Is Edittor  -->
										<div class="control-group">											
											<label class="control-label" for="username">Nick Name</label>
											<div class="controls ">
												<input class="form-control" id="username" name="nickname" type="text" class="span6 form-control" id="firstname" value="" >																					
											</div> <!-- /controls -->																
										</div> <!-- /control-group -->
										<!-- End Is Edittor  -->
										
										
										
							<input class="form-control" id="club_id" name="club_id" type="hident" class="span6 form-control" id="firstname" value="<?=$get_club_id?>" style="display: none;">																					
											
										
										
										
										
								
										
								
										
										<!-- Start Is Homepage Sub-Category -->
										<div class="control-group">											
											<label class="control-label" for="radiobtns">Status</label>
											
                                            <div class="controls">
	                                              <div class="btn-group">                                             	
			                                             <select name="status" required>
														  				<option value="">Select-Status</option>														
																			<option value="1">Active</option>
																			<option value="0">Unactive</option>
														</select>										
	                                            	</div>
                                             </div>	<!-- /controls -->			
										</div> <!-- /control-group -->
										<!-- End Is Homepage Sub-Category -->
																		
										<input style="visibility: hidden;" type="text" id="url1" value="<?php echo base_url(); ?>"/>
										<div class="form-actions">
											<input type="submit" name="btnsave" class="btn btn-primary" value="Submit"> 
											
										</div> <!-- /form-actions -->
										<hr>
									</fieldset>
								</form>

								</div>
							</div> 
						</div>

					</div> <!-- /widget-content -->
						
	
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js sidebar-large lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js sidebar-large lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js sidebar-large lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js sidebar-large"> <!--<![endif]-->

<head>
    <!-- BEGIN META SECTION -->
    <meta charset="utf-8">
    <title>Pixit - Responsive Boostrap3 Admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="" name="description" />
    <meta content="themes-lab" name="author" />
    <link rel="shortcut icon" href="assets/img/favicon.png">
    <!-- END META SECTION -->
    <!-- BEGIN MANDATORY STYLE -->
    <link href="<?=$template_paht?>css/assets/css/icons/icons.min.css" rel="stylesheet">
    <link href="<?=$template_paht?>css/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=$template_paht?>css/assets/css/plugins.css" rel="stylesheet">
    <link href="<?=$template_paht?>css/assets/plugins/bootstrap-loading/lada.min.css" rel="stylesheet">
    <link href="<?=$template_paht?>css/assets/css/style.min.css" rel="stylesheet">
    <!-- END  MANDATORY STYLE -->
    <script src="<?=$template_paht?>css/assets/modernizr/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    
    <link rel="stylesheet"  		type="text/css" href="<?=$template_paht?>sweetalert/sweetalert.css">
	<script type="text/javascript"  src="<?=$template_paht?>sweetalert/sweetalert.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.js"></script>

</head>

<body >


<script>
	
	
	function ValidatePassword(P1, P2)
	{
		  if (P1.value != P2.value ||
		    P1.value == "" ||
		    P2.value == "")
		  {
		   P2.setCustomValidity(
		     "The Password Is Incorrect");
		  }
		  else
		  {
		   P2.setCustomValidity("");
		  }
	}

</script>
	<?
			if($check_match==FALSE){
	?>
			<script>
				sweetAlert("Oops...", "รูปแบบไม่ถูกต้อง กรุณาระบุเฉพาะตัวอักษร เท่านั้น", "error");
			</script>
	<?		}
		
	?>
	
	
	<?
		if($user_double == "double"){
	?>
			<script>		  
				sweetAlert("Oops...", "บัญชีนี้มีผู้ใช้งานแล้ว กรุณาตั้งชื่อบัญชีผู้ใช้งานของท่านใหม่อีกครั้ง", "error");
			</script>
			
	<?	}
	?>


    <!-- START SIGNUP BOX -->
    <div class="container" id="login-block">
        <div class="row">
            <div class="col-sm-6 col-md-4 col-sm-offset-3 col-md-offset-4">
                <div class="login-box clearfix animated flipInY">
                    <div class="page-icon animated bounceInDown">
                        <img src="<?=$template_paht?>css/assets/img/account/register-icon.png" alt="Register icon" />
                    </div>
                    <div class="login-logo">
                        <a href="#">
                            <img src="<?=$template_paht?>css/assets/img/account/login-logo.png" alt="Company Logo" />
                        </a>
                    </div>
                    <hr>
                    <div class="login-form">
                        
                        <form action="signup" method="post"  class="pure-form" onsubmit="return myFunction()">
                            <input id="name" type="text" placeholder="Name" 		name="Name" class="input-field"  	value="<? echo $name;?>" required/>
                            <input type="text" placeholder="Surname" 	name="Surname"	class="input-field" value="<? echo $Surname;?>" required/>
                            <input type="email" placeholder="E-mail" 	name="E_mail"	class="input-field" value="<? echo $email;?>" required/>
                            <input type="text" placeholder="UserName" 	id="username" name="UserName"	class="input-field" value="<? echo $user;?>" required/>
                            <input id="P1" type="password" name="password"	placeholder="Password" 			value="<? echo $pass;?>" required/>
       		 				<input id="P2" type="password" name="cc_password" placeholder="Confirm Password" value="<? echo $cc_pass;?>" required onfocus="ValidatePassword( document.getElementById('P1'), this);" oninput="ValidatePassword( document.getElementById('P1'), this);" />
                            <input type="submit" class="btn btn-login ladda-button" name="btnsave" data-style="expand-left" value="Sign Up">
                        </form>
                        <div class="login-links">
                           
                            <a href="<?=base_url();?>login">Already have an account? <strong>Sign In</strong></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
 
    <!-- END SIGNUP BOX -->
    <!-- BEGIN MANDATORY SCRIPTS -->
    <script src="assets/plugins/jquery-1.11.js"></script>
    <script src="assets/plugins/jquery-migrate-1.2.1.js"></script>
    <script src="assets/plugins/jquery-ui/jquery-ui-1.10.4.min.js"></script>
    <script src="assets/plugins/jquery-mobile/jquery.mobile-1.4.2.js"></script>
    <script src="assets/plugins/bootstrap/bootstrap.min.js"></script>
    <script src="assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
    <!-- END MANDATORY SCRIPTS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="assets/plugins/backstretch/backstretch.min.js"></script>
    <script src="assets/plugins/bootstrap-loading/lada.min.js"></script>
    <script src="assets/js/account.js"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
</body>

</html>

<script>
	
$(function(){
	
	$.post('<?=$url_site?>rest/services/getContentPersonnel_all',{club_id:<?=$get_club_id?>}).done(function(data){
      	  if(data!=null){
	      	  temp = jQuery.parseJSON(data); 
	      	 // console.log(temp);
	      	  getPersonnel(temp.personnel);
	      		
      	  }
    }).fail(function(){
      		
      	  sweetAlert("Oops..Internet disconnect", "Please try again", "error");
      	 
	      window.location = window.location;
    });
      

       function getPersonnel(data){
       		console.log(data);
       		
		    $('#example').DataTable().destroy();
      		data.each(function(d){
      			
      			if(d.status==1){
      				$x = "Active";
      				$bg2= "66CC99";
      			}else{
      				$x = "Unactive";
      				$bg2= "BEBEBE";
      			}

      			$pic = '<?=$url_site?>'+d.picture;

		      	html = "<tr >";
		      	html += "<td style='text-align: center;' class='names'  data-label='Images:'> <img width='50px;' src='"+$pic+"' onerror='errImage(this)'></td>";
		      	html += "<td style='text-align: center;' class='names'  data-label='Name:'>"+d.name+"</td>";
				html += "<td style='text-align: center;' class='names'  data-label='Surname:'>"+d.surname+"</td>";
				html += "<td style='text-align: center;' data-label='Club:'>"+d.club_id+"</td>";
				html += "<td style='text-align: center;' data-label='Role:'>"+d.role_id+"</td>";
				html += "<td style='text-align: center; background: #"+$bg2+"' data-label='Status:'>"+$x+"</td>";
				html += "<td data-label='Edit:' style='background: #FFFFCC;'>";
				html += "<center>";
				html += "<img src='https://cdn3.iconfinder.com/data/icons/fatcow/16/application_form_edit.png' class='edit_team' onclick='edit_personnel("+d.id+")'/>";		
				html += "</center>";
				html += "</td>";	
				html += "<td data-label='Delete:' style='background: #FF9999;'>";
				html += "<center>";
				html += "<img src='https://cdn3.iconfinder.com/data/icons/softwaredemo/PNG/16x16/Close_Box_Red.png' class='del_personnel' onclick='del_personnel("+d.id+")'/>";		
				html += "</center>";
				html += "</td>";
						
				html += "</tr>";								
		    	$('#example').append(html);		      
	      	});	      	
	      	$('#example').DataTable();
      }
});
	 function errImage(ele){
	 	ele.src= "https://www.zombeewatch.org/static/aboutus/noimage.png";
      	//alert(ele.src);
      }
      
      
</script>


<script>
	
	function edit_personnel(id_edit_personnel){
		
		$.post('<?=base_url().$this->router->class.'/edit_personnel'?>',{'id_edit_personnel':id_edit_personnel}).done(function(){
			window.location = "<?=base_url().$this->router->class.'/edit_personnel/'?>";
		});
		
	}
	
	
	
	
	
	
	function del_personnel(personnel_id){

		swal({   title: "Are you sure?",   text: "You will not be able to recover this imaginary file!",   type: "warning",   showCancelButton: true,   confirmButtonColor: "#DD6B55",   confirmButtonText: "Yes, delete it!",   closeOnConfirm: false }, 
			
		function(){
			$.post('<?=$url_site?>rest/services/delContentPersonnel',{'personnel_id':personnel_id}).done(
						function(data){
						    
							if(data.opt=='YES'){
																
							}else{
								swal("Deleted!", "Your imaginary file has been deleted.", "success");								
								setTimeout("alertWelcome();",1000);	
							}
						}
			);
		});
		
		
		
	}
	
	

</script>
<script>
	function alertWelcome(){

		window.location = "<?=base_url().$this->router->class?>";

	}
</script>




                    <div class="widget-content">
                    	<section>
                    		<ul>
                    			<center>
                    			
                    				<a href="<?=base_url()?>managed/add_presonal" class="btn btn-info btn-md">
									    <span class="glyphicon glyphicon-plus"></span> Add Personal
									</a>	
									
								</center>
								
                    		</ul>

                    	</section>
                     	
                         <table id="example" class="table  hover table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                    <tr>
                                        <th>Images </th>
                                        <th>Name</th>
                                        <th>Surname</th>
                                        <th>Club</th>
                                        <th>Role</th>
                                        <th>Status</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                        
                                    </tr>
                                </thead>

                                        <tbody>
                                     		
                                        </tbody>
                        </table>
        
                    </div> <!-- /pricing-plans -->      
   
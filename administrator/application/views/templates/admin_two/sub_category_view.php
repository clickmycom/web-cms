
<script>

	function id_sub_category(id_sub_category){
		
		$.post("<?=base_url().$this->router->class.'/edit_sub_category/'?>",{'id_sub_category':id_sub_category}).done(function(){
			window.location = "<?=base_url().$this->router->class.'/edit_sub_category/'?>";
		});	
		
	}
	
	
	function myFunction_del(id_del){
		
		swal({   	title: "Are you sure?",   
					text: "You will not be able to recover this imaginary file!",   
					type: "warning",   
					showCancelButton: true,   
					confirmButtonColor: "#DD6B55",   
					confirmButtonText: "Yes, delete it!",   
					closeOnConfirm: false }, 
			
		function(){   				
			$.post("<?=base_url().$this->router->class.'/del_sub_category/'?>",{'id_del':id_del}).done(
						function(data){
							if(data=="TRUE"){
								swal("Deleted!", "Your imaginary file has been deleted.", "success");								
								setTimeout("alertWelcome();",1000);										
							}else{
								
								sweetAlert("Oops...Error "+data+"", "Something went wrong!", "error");
							}
							
						}
			);

		});
		
	}
	
	function alertWelcome(){

		window.location = "<?=base_url().$this->router->class.'/Sub_Category/'?>";

	}
	
	$(document).ready(function() {
    	$('#example2').DataTable( {
        	"order": [[ 0, "desc" ]]
    	} );
	});

</script>



<script>
	
	
	
	
	
</script>




<script>
	$(function(){
		
		$('form').submit(function(){
		         
		        $.post('<?=base_url().$this->router->class.'/update_edit_category'?>',$("form").serialize(),function( data ){
		             if(data.opt=='YES'){
		                 swal({   
		                                    title: "บันทึกข้อมูลเรียบร้อยแล้ว", 
		                                    text: "ผมกำลังปิดตัวเองใน  2 วินาที.",  
		                                    type: "success",   
		                                    showConfirmButton: false,                                    
		                                    timer: 2000 
		                                    }                   
		                 );
		                 setTimeout("redirect_page();",1500);	
		             }else{
		                sweetAlert("Oops..."+data.opt, "Something went wrong!", "error");
		             }	             
		        },"json");
		        return false;  
		    });
		    // end form
		
	});
	
	
	function redirect_page(){
		window.location = "<?=base_url().$this->router->class.'/Category'?>";
	}
		
</script>




                    
                    <div class="widget-content">
                    	<section>
                    		<ul>
                    			<center>
                    			
                    				<a href="<?=base_url().$this->router->class?>/add_sub_category" class="btn btn-info btn-md">
									    <span class="glyphicon glyphicon-plus"></span> Add Sub Category
									</a>	
									
								</center>
								
                    		</ul>
                    	</section>
                    	
                          <table id="example2" class="table  hover table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                    <tr>
                                    	<th>ID</th>
                                        <th>Category</th>
                                        <th>Name</th>
                                        <th>Link</th>
                                        <th>Is Menu</th>
                                        <th>Order No</th>
                                        
                                        <th>Status</th>
                                         <th>Edit</th>
                                      	 <th>Delete</th>
                                    </tr>
                                </thead>
                                        <tbody>
                                            
                                    <?php
                                        
                                            foreach ($Sub_Category as $value) {
                                    ?>
                                            <tr>
                                            	<td  data-label="ID:"><?=$value['id']?></td>
                                                <td  data-label="Category:"><?=$value['category_name']?></td>
                                                <td  data-label="Sub Category:"><?=$value['name']?></td>               
                                                <td  data-label="Article Name:"><?=$value['link']?></td>    
                                                <td  data-label="End Date:"><?=$value['is_menu']?></td>
                                                <td  data-label="Order:" width="80px"><center><?=$value['order_no']?></center></td>
                                                <?php
                                                	if($value['status'] == "Active"){
                                                		$bg= "66CC99";
                                                	}else{
                                                		$bg= "BEBEBE";
                                                	}
                                                
                                                ?>
                                                <td  data-label="Status:" style="background: #<?=$bg?>;" width="80px;"><center><b><?=$value['status']?></b></center></td>
                                                
                                                <td data-label="Edit:" style="background: #FFFFCC;" onclick="id_sub_category(<?=$value['id']?>)" width="60px">
                                                    <center>
                                                        <img src="https://cdn3.iconfinder.com/data/icons/fatcow/16/application_form_edit.png"   data-id_sub_category="<?=$value['id']?>"/>
                                                   </center>
                                                </td>
                                                
                                                <td data-label="Delete:" style="background: #FF9999;" onclick="myFunction_del(<?=$value['id']?>)" width="60px">
                                                	<!-- <center>
                                                		<? echo anchor("home/del_sub_category/".$value['id'],'<img src="https://cdn3.iconfinder.com/data/icons/softwaredemo/PNG/16x16/Close_Box_Red.png" />',array("onclick"=>"javascript:return confirm('คุณต้องการลบใช่หรือไม่ ?');"));?>
                                                	</center> -->
                                                	<center>
                                                        <img src="https://cdn3.iconfinder.com/data/icons/softwaredemo/PNG/16x16/Close_Box_Red.png"  />
                                                   </center>
                                                </td>                                        
                                            </tr>   
                                    <?php   }                                                                                                                    
                                    ?>
   
                                        </tbody>
                           </table>
        
                    </div> <!-- /pricing-plans -->      
       
<script src="http://malsup.github.com/jquery.form.js"></script>
<style>
	
.dropdown-submenu {
    position: relative;
}

.dropdown-submenu>.dropdown-menu {
    top: 0;
    left: 100%;
    margin-top: -6px;
    margin-left: -1px;
    -webkit-border-radius: 0 6px 6px 6px;
    -moz-border-radius: 0 6px 6px;
    border-radius: 0 6px 6px 6px;
}

.dropdown-submenu:hover>.dropdown-menu {
    display: block;
}

.dropdown-submenu>a:after {
    display: block;
    content: " ";
    float: right;
    width: 0;
    height: 0;
    border-color: transparent;
    border-style: solid;
    border-width: 5px 0 5px 5px;
    border-left-color: #ccc;
    margin-top: 5px;
    margin-right: -10px;
}

.dropdown-submenu:hover>a:after {
    border-left-color: #fff;
}

.dropdown-submenu.pull-left {
    float: none;
}

.dropdown-submenu.pull-left>.dropdown-menu {
    left: -100%;
    margin-left: 10px;
    -webkit-border-radius: 6px 0 6px 6px;
    -moz-border-radius: 6px 0 6px 6px;
    border-radius: 6px 0 6px 6px;
}
</style>
			

<script type="text/javascript">
	tinymce.init({
	    selector: "textarea",
	    plugins: [
	        "advlist autolink lists link image charmap print preview anchor",
	        "searchreplace visualblocks code fullscreen",
	        "insertdatetime media table contextmenu paste"
	    ],
	    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image code"
	});
</script>


<script type="text/javascript">
		function myformatter(date){
			var y = date.getFullYear();
			var m = date.getMonth()+1;
			var d = date.getDate();
			return y+'-'+(m<10?('0'+m):m)+'-'+(d<10?('0'+d):d);
		}
		function myparser(s){
			if (!s) return new Date();
			var ss = (s.split('-'));
			var y = parseInt(ss[0],10);
			var m = parseInt(ss[1],10);
			var d = parseInt(ss[2],10);
			if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
				return new Date(y,m-1,d);
			} else {
				return new Date();
			}
		}
</script>
	



<script>
	
$(function(){


	
	
	 $('.category').change(function(){
	 	 var id_detail = $(this).children(":selected").attr("id");
            	
            		var url1="<?=base_url().$this->router->class.'/onchang_category'?>";
                    
                      $('#load_sub').load(url1,{
                        	id_detail:id_detail                       
                      });
      });
      
      
       $('.change_img').click(function(){
       	
	 	 	//document.getElementById("myDIV").style.display = "none";
	 	 	//document.getElementById("show_edit_img").style.display = "inline";
	 	 	
	 	 	$( "#myDIV" ).slideUp();
	 	 	$( "#show_edit_img" ).slideDown();
	 	 
      });
      
      $('.remove_img').click(function(){
      	
      		var id_article 	= 	"<?=$this->session->userdata('id_article_edit')?>";
      		var name_img	=	$("#old_img").val();
      		
      		if(id_article!=null){
      			
	      		swal({   	title: "คุณต้องการลบรูปภาพ ?",   
						text: "คลิกปุ่ม Yes, delete it! เพื่อทำการลบ",   
						type: "warning",   
						showCancelButton: true,   
						confirmButtonColor: "#DD6B55",   
						confirmButtonText: "Yes, delete it!",   
						closeOnConfirm: false }, 
				
				function(){   				
					$.post("<?=base_url().$this->router->class.'/delete_img/'?>",{'id_article':id_article,'name_img':name_img}).done(
								function(data){
									var obj = jQuery.parseJSON(data);
									
									if(obj.opt=="YES"){
										$( "#myDIV" ).slideUp();
										swal("Deleted!", "ภาพของคุณถูกลบเรียบร้อยแล้ว", "success");								
										$("#old_img").val(null);
	 	 								$( "#show_edit_img" ).slideDown();

										//setTimeout("alertWelcome();",1000);										
									}else{
										sweetAlert("Oops...Error "+obj.opt+"", "Something went wrong!", "error");
									}
								}
					);
		
				});
			}
			
      });
      
     
	
	
		
});
	
</script>

	<?php
		 if($this->session->userdata('save_succeed_ud_article') == "YES"){
		 	$this->session->unset_userdata('save_succeed_ud_article');
	?>
			<script>
				swal({   
                        title: "บันทึกข้อมูลเรียบร้อยแล้ว", 
                        text: "ผมกำลังปิดตัวเองใน  2 วินาที.",  
                        type: "success",   
                        showConfirmButton: false,                                    
                        timer: 2000 
                     }                   
                  );

			</script>
	<?php
			 
		 }else if($this->session->userdata('save_succeed_ud_article')!=null){
		 	
	?>
		 	<script>
		 		sweetAlert("Oops..<?=$this->session->userdata('save_succeed')?>", "Something went wrong!", "error");
			</script>
		 	
	<?php	$this->session->unset_userdata('save_succeed_ud_article');
		 }
		 	
	?>
	
	

					<div class="widget-content">
						
					
					<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Article Form</a>
						  </li>
						</ul>
						<br>
						
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols edit-profile form-horizontal">		
															
							 
						
								<form id="form_id" enctype="multipart/form-data" action="<?=base_url().$this->router->class.'/update_edit_article'?>" method="post">
									<fieldset id="edit-profile" class="form-horizontal">
										<!-- Start combobox Category -->
										<div class="control-group">											
											<label class="control-label" for="radiobtns">Category</label>
											
                                            <div class="controls">
	                                              <div class="btn-group">          	                                   	
			                                             <select class="category" name="category"  >
			                                             	
			                                             	<?
			                                             		foreach ($category as $value) {
			                                            
			                                             		if($value['id']==$rs['category_id']){			                                             				
															?>																																				
																		<option id="<?=$value['id']?>" value="<?=$value['id']?>"><?=$value['name']?></option>															
															<?		
																	} 
																 }			                                             			
			                                             	
			                                              		foreach ($category as $value) {
			                                              			if($value['id']!=$rs['category_id']){
			                                              	?>
														  				<option id="<?=$value['id']?>" value="<?=$value['id']?>"><?=$value['name']?></option>
														  	<?				
			                                              			}
															  
																	}
			                                              	
			                                              	?>
			                                              	
														</select>										
	                                            	</div>
                                             </div>	<!-- /controls -->			
										</div> <!-- /control-group -->
										<!-- End combobox Category -->

																				
										<!-- Start combobox Sub-Category -->
										<div class="control-group">											
											<label class="control-label" for="radiobtns">Sub Category</label>
											<div id="load_sub">	
	                                            <div class="controls">
		                                              <div class="btn-group load_sub">                                             	
				                                             <select name="sub_category" >
				                                             	
				                                             	
				                                            <?
			                                             		foreach ($sub_category as $value) {
			                                            
			                                             		if($value['id']==$rs['sub_category_id']){			                                             				
															?>																																				
																		<option id="<?=$value['id']?>" value="<?=$value['id']?>"><?=$value['name']?></option>															
															<?		
																	} 
																 }			                                             			
			                                             	
			                                              		foreach ($sub_category as $value) {
			                                              			if($value['id']!=$rs['sub_category_id']){
			                                              	?>
														  				<option id="<?=$value['id']?>" value="<?=$value['id']?>"><?=$value['name']?></option>
														  	<?				
			                                              			}
															  
																	}
			                                              	
			                                              	?>
			                                              	
  	
				                                             	
				                                             	<!-- 
				                                             	
															  				<option value="">Select-Sub Category</option>
															  				
															  					
															  	<?php
				                                              			foreach ($sub_category as $value) {
																?>												
																		
																			<option value="<?=$value['id']?>"><?=$value['name']?></option>			 											
																<?php		  
																		}
				                                              	
				                                              	?> -->

															</select>										
		                                            	</div>
	                                             </div>	<!-- /controls -->	
                                             </div>		
										</div> <!-- /control-group -->
										<!-- End combobox Sub-Category -->
										
									
										<div id="show_edit_img" style="display: none;">
											<div class="control-group">
				
												<label class="control-label" for="firstname">Images</label>
												<div class="controls" style="width:50%;">
													<input type="file" class="file" accept="image/*" name="userfile" />
												</div>
											</div>
										</div>
								<?php
									if ($rs['article_image'] != null){
								?>
										<div id="myDIV">
											<div class="control-group ">
				
												<label class="control-label" for="firstname">Images</label>
												<div class="controls widget-content" >
													<img class="img-responsive fadeInRight-animated thumbnail"  accept="image/*" width="30%" src="<? echo base_url()."../public/upload_images/".$rs['article_image']; ?>">
				
													<input type="text" class="form-control" readonly  name="old_img" id="old_img" value="<?=$rs['article_image']?>"/>
													
													
													<div class="form-inline">
     													 <button type="button" class="btn btn-info change_img">
															Change Image
														</button>
    												
    												<button type="button" class="btn btn-danger remove_img" style="margin-left: 50px;">
															Remove Image
														</button>
    												</div>
												</div>
											</div>
												
										</div>
								
								<?php		
									}else{
								?>
										<div id="show_edit_img" >
											<div class="control-group">
				
												<label class="control-label" for="firstname">Images</label>
												<div class="controls" style="width:50%;">
													<input type="file" class="file" accept="image/*" name="userfile" />
												</div>
											</div>
										</div>
								<?php
									}
								?>
										
										
										
										
										<!-- Start Is Edittor  -->
										<div class="control-group">											
											<label class="control-label" >Title</label>
											<div class="controls ">
												<input placeholder="คำอธิบายรูปภาพ" class="form-control" id="title" name="title" type="text" class="span6 form-control" value="<?=$rs['tooltip'];?>">																					
												
											</div> <!-- /controls -->																
										</div> <!-- /control-group -->
										<!-- End Is Edittor  -->
										
										
										<!-- Start Is Edittor  -->
										<div class="control-group">											
											<label class="control-label" for="username">Name</label>
											<div class="controls ">
												<input class="form-control" id="username" name="name" type="text" class="span6 form-control" id="firstname" value="<?=$rs['article_name'];?>" >																					
													
													    <textarea class="form-control" name="comment" style="width:100%;height: 100%;" ><?echo $rs['article_detail'];?></textarea>
													
											</div> <!-- /controls -->																
										</div> <!-- /control-group -->
										<!-- End Is Edittor  -->
										

									

										<!-- Start Is Homepage Sub-Category -->
										<div class="control-group">											
											<label class="control-label" for="radiobtns">Is Homepage</label>
											
                                            <div class="controls">
	                                              <div class="btn-group">                                             	
			                             
														
														<select class="combobox " name="is_homepage" required>
											
															<?php
																		if($rs['is_homepage']!=null){
																?>
																			<option value="<?=$rs['is_homepage']?>"><?=$rs['is_homepage']?></option>
																<?php
										                                             foreach ($is_homepage as $value) {
										                                             	if($value!=$rs['is_homepage']){
										                        ?>
										                        							<option value="<?=$value?>"><?=$value?></option>
										                        <?php                     		
										                                             	}
																			 		 }		                                              	           					
																		}else{
																?>
																																		
																			<?php
										                                             foreach ($is_homepage as $value) {
																			?>																														
																						<option value="<?=$value?>"><?=$value?></option>																			 											
																		    <?php		  
																			 		 }		                                              	
										                                    ?>													
																<?php			
																		}													
																?>
																
																
															
																					
														</select>	
											
											
											
											
											
																				
	                                            	</div>
                                             </div>	<!-- /controls -->			
										</div> <!-- /control-group -->
										<!-- End Is Homepage Sub-Category -->
										
									
										
										<div class="control-group">											
											<label class="control-label" for="firstname">Show Date</label>
											<div class="controls">
										
											
												<?
													if($rs['showdate']!='0000-00-00'){
												?>
													<input name="show_date" class="easyui-datebox" data-options="formatter:myformatter,parser:myparser" value="<?=$rs['showdate']?>"></input>
												<?	
													}else{
												?>		
													<input name="show_date" class="easyui-datebox" data-options="formatter:myformatter,parser:myparser" value=""></input>
												<?
													}
												?>	
										
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										
										<div class="control-group">											
											<label class="control-label" for="firstname">End Date</label>
											<div class="controls">
												<?
													if($rs['enddate']!='0000-00-00'){
												?>
													<input name="end_date" class="easyui-datebox" data-options="formatter:myformatter,parser:myparser" value="<?=$rs['enddate']?>"></input>
												<?	
													}else{
												?>		
													<input name="end_date" class="easyui-datebox" data-options="formatter:myformatter,parser:myparser" value=""></input>
												<?
													}
												?>	
												
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										
	
	
										<!-- Sprintner -->
										<div class="control-group">											
											<label class="control-label" >Order NO</label>
											<div class="controls ">


																<div class="input-group col-xs-2">
																	<input id="colorful" class="form-control" type="number" name="oder_no" value="<?=$rs['order_no']?>" min="1" max="1000000" />
																</div>
			

											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										<!-- Sprintner-->
										
										
										
											<!-- Start Is Homepage Sub-Category -->
										<div class="control-group">											
											<label class="control-label" for="radiobtns">Status</label>
											
                                            <div class="controls">
	                                              <div class="btn-group">                                             	
			                                             <select name="status" >
														  				<option><?=$rs['status']?></option>
														
																		<?php
																			foreach ($is_homepage_status as $value) {
																				if($value!=$rs['status']){
																		?>				
																					<option ><?=$value?></option>
																		<?		}		
																			}																		
																		?>					 											
														
														</select>										
	                                            	</div>
                                             </div>	<!-- /controls -->			
										</div> <!-- /control-group -->
										<!-- End Is Homepage Sub-Category -->
										
										
										
										
										
										
										
										
										<input style="visibility: hidden;" type="text" id="url1" value="<?php echo base_url(); ?>"/>
										<div class="form-actions">
											<input type="submit" name="btnsave" class="btn btn-success" value="Submit"> 
												<input type="submit"  name="btnsave" class="btn btn-default" value="Cancel">  
										</div> <!-- /form-actions -->
										<hr>
										
									</fieldset>
								<?php echo form_close();?>

								</div>

							</div> 
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
	

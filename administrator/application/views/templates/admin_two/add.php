<script type="text/javascript">
	tinymce.init({
    	selector: "textarea",theme: "modern",height: 300,
    	 relative_urls : false,
         remove_script_host: false,
    	plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak",
         "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
         "table contextmenu directionality emoticons paste textcolor responsivefilemanager code"
   		],
   		
	   toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
	   toolbar2: "| responsivefilemanager | code | link unlink anchor | image media | forecolor backcolor  | print preview  ",
	   image_advtab: true ,
   
	   external_filemanager_path:"<?=base_url()?>public/templates/admin_two/filemanager/",
	   filemanager_title:"Responsive Filemanager" ,
	   external_plugins: { "filemanager" : "<?=base_url()?>public/templates/admin_two/filemanager/plugin.min.js"}
 });
</script>


<script src="<?=base_url().'public/ajaxForm.js'?>"></script>

<script type="text/javascript">
		function myformatter(date){
			var y = date.getFullYear();
			var m = date.getMonth()+1;
			var d = date.getDate();
			return y+'-'+(m<10?('0'+m):m)+'-'+(d<10?('0'+d):d);
		}
		function myparser(s){
			if (!s) return new Date();
			var ss = (s.split('-'));
			var y = parseInt(ss[0],10);
			var m = parseInt(ss[1],10);
			var d = parseInt(ss[2],10);
			if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
				return new Date(y,m-1,d);
			}else {
				return new Date();
			}
		}

</script>
	


<script>
$(function(){
	 $('.category').change(function(){
	 	 var id_detail = $(this).children(":selected").attr("id");
            	
            var url1="<?=base_url().$this->router->class.'/onchang_category'?>";
              $('#load_sub').load(url1,{
              		id_detail:id_detail
                	                    
              });
      });	
});

</script>

	<?php
		 if($this->session->userdata('save_succeed') == "YES"){
		 	$this->session->unset_userdata('save_succeed');
	?>
			<script>
				swal({
                        title: "บันทึกข้อมูลเรียบร้อยแล้ว", 
                        text: "ผมกำลังปิดตัวเองใน  2 วินาที.",  
                        type: "success",   
                        showConfirmButton: false,                                    
                        timer: 2000 
                     }                   
                  );

			</script>
	<?php
			 
		 }else if($this->session->userdata('save_succeed')!=null){
		 	
	?>
		 	<script>
		 		sweetAlert("Oops..<?=$this->session->userdata('save_succeed')?>", "Something went wrong!", "error");
			</script>
		 	
	<?php	$this->session->unset_userdata('save_succeed');
		 }
		 	
	?>


					
					<div class="widget-content">
						

					<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Article Form </a>
						  </li>
						</ul>
						<br>
						
							<div class="tab-content" id='zone-form'>
								<div class="tab-pane active" id="formcontrols edit-profile form-horizontal">		
															
								<form enctype="multipart/form-data" action="<?=base_url().$this->router->class.'/save_article'?>" method="post">
									<fieldset id="edit-profile" class="form-horizontal">
										<!-- Start combobox Category -->
										<div class="control-group">											
											<label class="control-label" for="radiobtns">Category</label>
											
                                            <div class="controls">
	                                              <div class="btn-group">          	                                   	
			                                             <select class="category" name="category"  required>
														  				<option value="">Select-Category</option>

														  	<?php
			                                              			foreach ($category as $value) {
															?>																														
																		<option id="<?=$value['id']?>" value="<?=$value['id']?>"><?=$value['name']?></option>
																			 											
															<?php		  
																	}
			                                              	
			                                              	?>
			                                              	
														</select>										
	                                            	</div>
                                             </div>	<!-- /controls -->			
										</div> <!-- /control-group -->
										<!-- End combobox Category -->
										
										
											
																			
										<!-- Start combobox Sub-Category -->
										<div class="control-group">											
											<label class="control-label" >Sub Category</label>
											<div id="load_sub">	
	                                            <div class="controls">
		                                              <div class="btn-group ">                                             	
				                                             <select name="sub_category" >
				                                             	
															  				<option value="">Select-Sub Category</option>
															  	<?php
				                                              			foreach ($sub_category as $value) {
																?>												
																			<option value="<?=$value['id']?>"><?=$value['name']?></option>			 											
																<?php		  
																		}
				                                              	?>
				                                             
															</select>										
		                                            	</div>
	                                             </div>	<!-- /controls -->	
                                             </div>		
										</div> <!-- /control-group -->
										<!-- End combobox Sub-Category -->
										
										
										<div class="control-group" style="width:800px;">											
											<label class="control-label" for="firstname">Images</label>
											<div class="controls">
												
												<input type="file" class="file" accept="image/*" name="userfile" />
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										
										<!-- Start Is Edittor  -->
										<div class="control-group">											
											<label class="control-label" >Title</label>
											<div class="controls ">
												<input placeholder="คำอธิบายรูปภาพ" class="form-control" id="title" name="title" type="text" class="span6 form-control" >																					
												
											</div> <!-- /controls -->																
										</div> <!-- /control-group -->
										<!-- End Is Edittor  -->
										
										
										<!-- Start Is Edittor  -->
										<div class="control-group">											
											<label class="control-label" for="username">Name</label>
											<div class="controls ">
												<input class="form-control" id="username" name="name" type="text" class="span6 form-control" id="firstname" value="" required>																					
													
													  
													
											</div> <!-- /controls -->																
										</div> <!-- /control-group -->
										<!-- End Is Edittor  -->
										

										<!-- Start Is Edittor  -->
										<div class="control-group">											
											<label class="control-label" for="username">Detail</label>
											<div class="controls ">
																															
													
													    <textarea class="form-control" name="comment" id="comment" style="width:100%;height: 100%;" ></textarea>
													
											</div> <!-- /controls -->																
										</div> <!-- /control-group -->
										<!-- End Is Edittor  -->
										
	
										<!-- Start Is Homepage Sub-Category -->
										<div class="control-group">											
											<label class="control-label" for="radiobtns">Is Homepage</label>
											
                                            <div class="controls">
	                                              <div class="btn-group">                                             	
			                                             <select name="is_homepage" required>
														  				<option value="">Select-Is Homepage</option>
														
																		<?php
																			foreach ($is_homepage as $value) {
																		?>
																			<option ><?=$value?></option>
																		<?php		
																			}
																		
																		?>			 											
														
														</select>										
	                                            	</div>
                                             </div>	<!-- /controls -->			
										</div> <!-- /control-group -->
										<!-- End Is Homepage Sub-Category -->
										
									
										
										<div class="control-group">											
											<label class="control-label" for="firstname">Show Date</label>
											<div class="controls">
												<input name="show_date" class="easyui-datebox" data-options="formatter:myformatter,parser:myparser"></input>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										
										<div class="control-group">											
											<label class="control-label" for="firstname">End Date</label>
											<div class="controls">
												<input name="end_date" class="easyui-datebox" data-options="formatter:myformatter,parser:myparser" ></input>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										

											<!-- Sprintner -->
										<div class="control-group">											
											<label class="control-label" >Order NO</label>
											<div class="controls ">

																<div class="input-group col-xs-2">
																	<input id="colorful" class="form-control" type="number" name="oder_no" value="1" min="1" max="1000000" />
																</div>

											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										<!-- Sprintner-->
										
										
										<!-- Start Is Homepage Sub-Category -->
										<div class="control-group">											
											<label class="control-label" for="radiobtns">Status</label>
											
                                            <div class="controls">
	                                              <div class="btn-group">                                             	
			                                             <select name="status" required>
														  				<option value="">Select-Status</option>
														
																		<?php
																			foreach ($is_homepage_status as $value) {
																		?>
																			<option ><?=$value?></option>
																		<?php		
																			}
																		
																		?>			 											
														
														</select>										
	                                            	</div>
                                             </div>	<!-- /controls -->			
										</div> <!-- /control-group -->
										<!-- End Is Homepage Sub-Category -->
										
					
										<input style="visibility: hidden;" type="text" id="url1" value="<?php echo base_url(); ?>"/>
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" value="Submit">
											<button class="btn">Cancel</button>
										</div> <!-- /form-actions -->
										
									</fieldset>
								</form>

								</div>

							</div> 
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
	
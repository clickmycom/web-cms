<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Get_model extends CI_Model {
    
    public function __construct()
    {
        parent::__construct();
        
    }
    
    public function get_template()
    {
        $query = $this->db->get('layouts_config');
        return $query->row();
    }
		
	public function get_mainmenu(){
		
		$this->db->where('status',1);
		$this->db->where('is_menu',1);
		$this->db->order_by('order_no', "asc");
		$query = $this->db->get('category');
		 
		return $query->result_array();
	}
	
	public function get_image_url()
	{
		$this->db->where('status',1);
		$this->db->order_by('order_no', "asc");
		$query = $this->db->get('cms_slide');
		
		return $query->result_array();
	}
	
	
	public function new_category(){

		$this->db->where('status',1);
		$this->db->where('is_homepage',1);
		
        $this->db->order_by('id', "DESC");
		
		$this->db->limit(1);
		$query = $this->db->get('article');
		
		return $query->result_array();
		
	}
	
	public function get_artical_template_two(){
		
		$maxid = $this->db->query('SELECT MAX(id) AS `maxid` FROM `article`')->row()->maxid;
		
		
		$this->db->where('id !=',$maxid	);
		$this->db->order_by('order_no', "asc");
		$this->db->where('status',1);
		$this->db->where('is_homepage',1);
		
		
		$this->db->limit(8);
		$query = $this->db->get('article');
		
		
		return $query->result_array();
	}
	
	
	
	//*************************
	// เริ่ม   แสดงบทความในหน้าแรก index.php
	//*************************
	public function get_article(){
		
		$this->db->order_by('order_no', "asc");
		$this->db->order_by('id', "desc");
		$this->db->where('showdate <=',date('Y-m-d'));
		$this->db->where('enddate >=',date('Y-m-d'));
		$this->db->or_where('showdate',"0000-00-00");
		
		$this->db->where('status',1);
		$this->db->where('is_homepage',1);
		
		$this->db->limit(9);
		$query = $this->db->get('article');
		return $query->result_array();
		
	}
	
	//*************************
	// จบ   แสดงบทความในหน้าแรก index.php
	//*************************
	
	
	//************************************
	// เริ่ม   แสดงบทความในหน้าแรก index.php  8 article
	//************************************
	public function get_article_8(){
		
		$this->db->order_by('order_no', "asc");
		$this->db->order_by('id', "desc");
		$this->db->where('showdate <=',date('Y-m-d'));
		$this->db->where('enddate >=',date('Y-m-d'));
		$this->db->or_where('showdate',"0000-00-00");
		
		$this->db->where('status',1);
		$this->db->where('is_homepage',1);
		
		$this->db->limit(8);
		$query = $this->db->get('article');
		return $query->result_array();
		
	}
	//************************************
	// จบ   แสดงบทความในหน้าแรก index.php  8 article
	//************************************
	
	
	
	//เช็คสถานะของบความว่า Active หรือไม่ ถ้า Active และ is_homepage เป็น Yes เอาไปแสดงหน้า View
	public function check_status_category()
	{
		
		$this->db->order_by('order_no', "asc");
		$this->db->where('status',1);
		$this->db->where('is_homepage',1);
		$this->db->where('showdate <=',date('Y-m-d'));
		$this->db->where('enddate >=',date('Y-m-d'));
        
		$this->db->or_where('showdate',"0000-00-00");
        $this->db->where('status',1);
        $this->db->where('is_homepage',1);
        
		
		$this->db->limit(8);
		$query = $this->db->get('article');
		
	
		return $query->result_array();
	}	
 	//เช็คสถานะของบความว่า Active หรือไม่ ถ้า Active และ is_homepage เป็น Yes เอาไปแสดงหน้า View
	
	public function get_sub_category(){
		$this->db->where('status',1);
		$this->db->where('is_menu',1);
		$this->db->order_by('order_no', "asc");
		$query = $this->db->get('sub_category');
		
		return $query->result_array(); 
		
	}

	
	function get_category_view(){
		
		   $section 	= $this->session->userdata('section');
		
		   $section_id_head = $this->session->userdata('section_id_category');
		   $section_id_sub = $this->session->userdata('section_id');
		 //echo $section_id = $this->session->userdata('section_menu_name');
		 //echo $section_id = $this->session->userdata('section_menu_head_name');
		 
		 
		if($section == "sub_category_id"){
			$this->db->where('category_id',"$section_id_head");
			$this->db->where('sub_category_id',"$section_id_sub");
			// $this->db->where('category_id',2);
	        $this->db->where('status',1);
			$this->db->or_where('showdate',"0000-00-00");
			$this->db->where('showdate <=',date('Y-m-d'));	
			$this->db->where('enddate >=',date('Y-m-d'));
			$this->db->order_by('id', "DESC");       
			$query = $this->db->get('article');
			return $query->result_array();
				
		}else{
			
			$this->db->where('category_id',"$section_id_head");
	        $this->db->where('status',1);
			$this->db->or_where('showdate',"0000-00-00");
			$this->db->where('showdate <=',date('Y-m-d'));	
			$this->db->where('enddate >=',date('Y-m-d'));
			$this->db->order_by('id', "DESC");       
			$query = $this->db->get('article');
			return $query->result_array();
			
		}
			
	
		
			
			
		//print_r($query->result());	
	}
	
	public function get_detail_view()
	{
		
		$id_detail	= $this->session->userdata('id_detail');
		
		
		$this->db->where('id',"$id_detail");
		$this->db->where('status',1);
		$this->db->or_where('showdate',"0000-00-00");
		$this->db->where('showdate <=',date('Y-m-d'));
		$this->db->where('enddate >=',date('Y-m-d'));
		
       
		$query = $this->db->get('article');
		  return $query->row();
		
	
		
	}
	
	public function get_hot_news(){
		
		$this->db->order_by('hitz', "DESC");
		$this->db->where('status',1);
        $this->db->where('id !=',$this->session->userdata('id_detail'));
		$this->db->or_where('showdate',"0000-00-00");
    
		$this->db->where('showdate <=',date('Y-m-d'));
		$this->db->where('enddate >=',date('Y-m-d'));
		
        $this->db->limit('3');
        $query = $this->db->get('article');
		  return $query->result_array();
	}
	
	
	
	public function get_news_freebies(){
			
		$this->db->where('status',1);
		$this->db->or_where('showdate',"0000-00-00");
		$this->db->where('showdate <=',date('Y-m-d'));
		$this->db->where('enddate >=',date('Y-m-d'));
		$this->db->order_by('id', "DESC");
       
		$query = $this->db->get('article');
		  return $query->result_array();
	}
	
	




	// Start Footer Section 1
	public function gen_footer_box1_category(){

		
	    $this->db->where('is_menu',2);
		$query_category = $this->db->get('category');
			
		return $query_category->row();		
	}

	public function gen_footer_box1_subcategory($id_category_notmenu){
		
		$this->db->where('category_id',$id_category_notmenu);
		$query_sub_category1	= $this->db->get('sub_category');
		
		return $query_sub_category1->row();
	}
	
	public function get_section1($id_category,$id_subcategory){
		
		$this->db->where('category_id',$id_category);
		$this->db->where('sub_category_id ',$id_subcategory);
		$this->db->order_by('order_no', "asc");
		$this->db->where('status',1);
		$this->db->where('is_homepage',1);
		

		// $this->db->where('showdate',"0000-00-00");
		// $this->db->or_where('showdate <=',date('Y-m-d'));
		// $this->db->or_where('enddate >=',date('Y-m-d'));

				
        $this->db->where('status',1);
        $this->db->where('is_homepage',1);
	
		$query = $this->db->get('article');
		
		return $query->result_array();
	
	}
	// End  Footer Section 1
	
	
	

	

	// Start Footer Section 2
	public function gen_footer_box2_category(){
	 	
		$this->db->where('id !=',$this->session->userdata('foot_1'));
		
	    $this->db->where('is_menu',2);
		$query_category = $this->db->get('category');
			
		return $query_category->row();		
	}

	public function gen_footer_box2_subcategory($id_category_notmenu){
		
		$this->db->where('category_id',$id_category_notmenu);
		$query_sub_category2	= $this->db->get('sub_category');
		
		return $query_sub_category2->row();
	}
	
	public function get_section2($id_category,$id_subcategory){
		
		$this->db->where('category_id',$id_category);
		$this->db->where('sub_category_id ',$id_subcategory);
		$this->db->order_by('order_no', "asc");
		$this->db->where('status',1);
		$this->db->where('is_homepage',1);
		

		// $this->db->where('showdate',"0000-00-00");
		// $this->db->or_where('showdate <=',date('Y-m-d'));
		// $this->db->or_where('enddate >=',date('Y-m-d'));

				
        $this->db->where('status',1);
        $this->db->where('is_homepage',1);
	
		$query = $this->db->get('article');
		
		return $query->result_array();
		
	}
	// End  Footer Section 2
	
	
	
	


	
	
	// Start Footer Section 3
	public function gen_footer_box3_category(){

		$this->db->where('id !=',$this->session->userdata('foot_1'));
		$this->db->where('id !=',$this->session->userdata('foot_2'));
	    $this->db->where('is_menu',2);
		$query_category = $this->db->get('category');
			
		return $query_category->row();		
	}

	public function gen_footer_box3_subcategory($id_category_notmenu){
		
		$this->db->where('category_id',$id_category_notmenu);
		$query_sub_category2	= $this->db->get('sub_category');
		
		return $query_sub_category2->row();
	}
	
	public function get_section3($id_category,$id_subcategory){
		
		$this->db->where('category_id',$id_category);
		$this->db->where('sub_category_id ',$id_subcategory);
		$this->db->order_by('order_no', "asc");
		$this->db->where('status',1);
		$this->db->where('is_homepage',1);
		

		// $this->db->where('showdate',"0000-00-00");
		// $this->db->or_where('showdate <=',date('Y-m-d'));
		// $this->db->or_where('enddate >=',date('Y-m-d'));

				
        $this->db->where('status',1);
        $this->db->where('is_homepage',1);
	
		$query = $this->db->get('article');
		
		return $query->result_array();
		
	}
	// End  Footer Section 3
	
	
	


	// Start Footer Section 4
	public function gen_footer_box4_category(){

			$this->db->where('id !=',$this->session->userdata('foot_1'));
			$this->db->where('id !=',$this->session->userdata('foot_2'));
			$this->db->where('id !=',$this->session->userdata('foot_3'));
	        $this->db->where('is_menu',2);
			$query_category = $this->db->get('category');
			
			return $query_category->row();		
		}

	public function gen_footer_box4_subcategory($id_category_notmenu){
		
		$this->db->where('category_id',$id_category_notmenu);
		$query_sub_category2	= $this->db->get('sub_category');
		
		return $query_sub_category2->row();
		
		
	}
	
	public function get_section4($id_category,$id_subcategory){
		
		$this->db->where('category_id',$id_category);
		$this->db->where('sub_category_id ',$id_subcategory);
		$this->db->order_by('order_no', "asc");
		$this->db->where('status',1);
		$this->db->where('is_homepage',1);
		

		// $this->db->where('showdate',"0000-00-00");
		// $this->db->or_where('showdate <=',date('Y-m-d'));
		// $this->db->or_where('enddate >=',date('Y-m-d'));

				
        $this->db->where('status',1);
        $this->db->where('is_homepage',1);
	
		$query = $this->db->get('article');
		
		return $query->result_array();
		
	}
	// End  Footer Section 4
	
	
	
    
}


<link rel="stylesheet" href="<?=$template_paht?>/category/css/style_team_personel.css">

  <!-- Google Fonts -->
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
 
<link rel="stylesheet" type="text/css" href="<?=$template_paht?>/category/css/site_global.css?417434784"/>

<link rel="stylesheet" type="text/css" href="<?=$template_paht?>/category/css/team.css?4240210797" id="pagesheet"/>
<link rel="stylesheet" type="text/css" href="<?=$template_paht?>/category/css/team.css?4240210797" id="pagesheet"/>

<!-- img Hover -->
<link rel="stylesheet" type="text/css" href="<?=$template_paht?>/category/img_hover/style_common.css" id="pagesheet"/>
<link rel="stylesheet" type="text/css" href="<?=$template_paht?>/category/img_hover/style9.css" id="pagesheet"/>

<!-- img Hover -->


<style>
	.player_detail{
    position: relative;
    background: url("http://champion.stylemixthemes.com/wp-content/themes/champion/assets/images/pre_footer_bg.jpg") no-repeat 50% 50%;
    margin: 0 0 20px;
}

.player_detail .player_photo{
    position: relative;
    float: left;
    width: 320px;
}

.player_detail .player_photo img{
    /*max-width: 100%;*/
    width: 100%;
    height: auto;
    display: block;
}

.player_detail .player_info{
    padding: 37px 60px;
    overflow: hidden;
}

.player_detail .player_info a{
	color: #fff;
}

.player_detail .player_info a:hover{
	text-decoration: none;
}

body .player_detail .player_info a.addthis_button_compact{
	color: #fff !important;
}

.player_detail .player_info table{
    width: 100%;
    border-collapse: collapse;
    border-spacing: 0;
    table-layout: fixed;
    margin: 0;
}

.player_detail .player_info table tr:first-child td, .player_detail .player_info table tr:first-child th{
    padding-top: 0;
}

.player_detail .player_info table tr:last-child td, .player_detail .player_info table tr:last-child th{
    padding-bottom: 0;
}

.player_detail .player_info table th{
    text-align: left;
    color:#b2b2b2;
    font-weight: 300;
    text-transform: uppercase;
    font-size: 15px;
    padding: 8px 0;
    border: none;
}

.player_detail .player_info table td{
    text-align: right;
    color:#ffffff;
    font-weight: 400;
    text-transform: uppercase;
    font-size: 15px;
    padding: 8px 0;
    border: none;
}

.t-shirt{
    display: inline-block;
    margin: 0 5px 0 0;
    width: 17px;
    height: 15px;
    background: url("http://champion.stylemixthemes.com/wp-content/themes/champion/assets/images/t-shirt.png") no-repeat 0 0;
}

.player_detail .player_info .addthis_toolbox{
    margin-top: 10px;
}

</style>

<script>
	
$(function(){
		
		
	  $.post('<?=$url_site.'rest/services/getContentEdit'?>',{'id':<?=$this->session->userdata('id_player')?>,'table':'personnel'}).done(function(data){
              //console.log(jQuery.parseJSON(data));
           	temp = jQuery.parseJSON(data); 
            console.log(temp);
            
            if(temp.first_position_id == 1 && temp.role_id == 1){
            	$position	= "Goal Keeper";
            }else if(temp.first_position_id == 2 && temp.role_id == 1){
   				$position	= "Left Forward Back";
   			}else if(temp.first_position_id == 3 && temp.role_id == 1){
   				$position	= "Center Forward Back";
   			
   			}else if(temp.first_position_id == 4 && temp.role_id == 1){
   				$position	= "Right Forward Back";
   			
   			}else if(temp.first_position_id == 5 && temp.role_id == 1){
   				$position	= "Left Defense Midfield Forward";
   				
   			}else if(temp.first_position_id == 6 && temp.role_id == 1){
   				$position	= "Right Defense Midfield Forward";
   				
   			}else if(temp.first_position_id == 7 && temp.role_id == 1){
   				$position	= "Left Offensive Midfielder";
   				
   			}else if(temp.first_position_id == 8 && temp.role_id == 1){
   				$position	= "Right Offensive Midfielder";
   				
   			}else if(temp.first_position_id == 9 && temp.role_id == 1){
   				$position	= "Left Midfield";
   				
   			}else if(temp.first_position_id == 10 && temp.role_id == 1){
   				$position	= "Center Midfield";
   				
   			}else if(temp.first_position_id == 11 && temp.role_id == 1){
   				$position	= "Right Forward";
   				
   			}else if(temp.first_position_id == 12 && temp.role_id == 1){
   				$position	= "Light Wing Forward";
   				
   			}else if(temp.first_position_id == 13 && temp.role_id == 1){
   				$position	= "Right Wing Forward";
   				
   			}else if(temp.first_position_id == 14 && temp.role_id == 1){
   				$position	= "Second Striker";
   			}else if(temp.first_position_id == null && temp.role_id == 2){
   				$position	= "Staff";
   			}else if(temp.first_position_id == null && temp.role_id == 3){
   				$position	= "Coach";
   			}else if(temp.first_position_id == null && temp.role_id == 4){
   				$position	= "Employee";
   			}else if(temp.first_position_id == null && temp.role_id == 5){
   				$position	= "Match Official";
   			}
   			
   			
   			 
  
           	document.getElementById('player_no').innerHTML = temp.unifrom_number;
           	document.getElementById('name').innerHTML = temp.name+"&nbsp"+temp.surname;
           	document.getElementById('height').innerHTML = temp.hight;
           	document.getElementById('position').innerHTML = $position;
           	document.getElementById('birthday').innerHTML = temp.birthdate;
           	document.getElementById('weight').innerHTML = temp.weight;
           	document.getElementById('email').innerHTML = temp.email;
           	$("#name_img").attr("src", '<?echo "$url_site";?>'+temp.picture);
           	
           
     });
     
     

});
	
	
</script>

				<!-- Begin Page Header -->
				<div class="header">
					<div class="container">
							<!-- Page Title -->
							<div style="position: relative;right:15px;">
								<div class="col-sm-6 col-xs-12">
									<h1><?=$section_id = $this->session->userdata('section_menu_name')?></h1>
								</div>
							</div>
							<!-- Breadcrumbs -->
							<div class="col-sm-6">
								<ol class="breadcrumb">
									<li><span class="ion-home breadcrumb-home"></span><a href="<?=$link_home?>">Home</a></li>
									
									<?php
										if($section_id = $this->session->userdata('section_menu_head_name')!=null){
									?>
											<li><?=$section_id = $this->session->userdata('section_menu_head_name')?></li>
									<?	}
									?>
									<li><?=$section_id = $this->session->userdata('section_menu_name')?></li>
								</ol>
							</div><!-- /column -->  
					
					</div><!-- /container -->  
				</div><!-- /page header -->  

			<!-- End Header -->
			
			









<div class="clearfix container" id="page">
	<!-- column -->
		<div class="museBGSize browser_width colelem" id="u3825" style="width: 1140px; padding-right: 0px;"><!-- group -->
	     <div class="clearfix" id="u3825_align_to_page">
	      <div class="clearfix grpelem" id="u3826-4"><!-- content -->
	     
	      </div>
	     </div>
	    </div>
	
<div class="player_detail clearfix">
    <div class="player_photo">
        <img width="640" height="640" src="" id="name_img" class="attachment-player_photo wp-post-image" alt="กำลังดาวโหลด">    
    </div>
    <div class="player_info">
        <table>
            <tbody>
					<tr>
						<th>หมายเลข :</th>
						<td><i class="t-shirt"></i><label id="player_no"></label></td>
					</tr>
					<tr>
						<th>ชื่อ - สกุล  :</th>
						<td><label id="name"></label></td>
					</tr>
					<tr>
						<th>ตำแหน่ง :</th>
						<td><label id="position"></label></td>
					</tr>
					<tr>
						<th>ส่วนสูง:</th>
						<td><label id="height"></label></td>
					</tr>
					<tr>
						<th>น้ำหนัก:</th>
						<td><label id="weight"></label></td>
					</tr>
					<tr>
						<th>วัน/เดือน/ปี เกิด:</th>
						<td><label id="birthday"></label></td>
					</tr>
					<tr>
						<th>อีเมล์ :</th>
						<td><label id="email"></label></td>
					</tr>
					<tr>
						<th><script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js"></script>
						<div class="addthis_toolbox">
							<a class="addthis_button_compact btn btn-danger btn-lg" href="#">Share now <i class="fa fa-share-alt"></i></a>
							<div class="atclear"></div>
						</div></th>
						
					</tr>
					
            </tbody>
        </table>
    </div>
</div>

 


	</div>

	<section>
	<div class="museBGSize browser_width colelem" id="u4437" style="width: 2903px; margin-left: -452px; left: 0px; margin-right: -2255px; padding-right: 0px;"><!-- group -->
     <div class="clearfix" id="u4437_align_to_page">
     	<center>
     		
     	
      <div class="museBGSize grpelem" id="u4439"><!-- simple frame --></div>
      <div class="museBGSize grpelem" id="u4440"><!-- simple frame --></div>
      <div class="museBGSize grpelem" id="u4441"><!-- simple frame --></div>
      <div class="museBGSize grpelem" id="u4442"><!-- simple frame --></div>
      <div class="museBGSize grpelem" id="u4443"><!-- simple frame --></div>
      </center>
     </div>
    </div>
</section>
<div class="verticalspacer" style="min-height: 1px;"></div>
<!-- Scripts -->
<script src="<?=$template_paht?>/category/js/jquery.min.js"></script>
<script src="<?=$template_paht?>/category/js/scripts.js"></script>
<script src="<?=$template_paht?>/category/js/owl.carousel.min.js"></script>
<script src="<?=$template_paht?>/category/js/jquery.ba-outside-events.min.js"></script>
<script src="<?=$template_paht?>/category/js/tab.js"></script>
<script src='<?=$template_paht?>/category/js/bootstrap-datepicker.js'></script>
<script src="<?=$template_paht?>/category/js/jquery.vide.min.js"></script>


 <!-- JS includes -->
<script src="<?=$template_paht?>/category/js/jquery-1.8.3.min.js" type="text/javascript"></script>
  <script src="<?=$template_paht?>/category/js/museutils.js?3865766194" type="text/javascript"></script>
  <script src="<?=$template_paht?>/category/js/jquery.tobrowserwidth.js?3842421675" type="text/javascript"></script>
  <script src="<?=$template_paht?>/category/js/webpro.js?3903299128" type="text/javascript"></script>
  <script src="<?=$template_paht?>/category/js/musewpslideshow.js?138381373" type="text/javascript"></script>
  <script src="<?=$template_paht?>/category/js/musewpdisclosure.js?4285748861" type="text/javascript"></script>
  <script src="<?=$template_paht?>/category/js/jquery.watch.js?4068933136" type="text/javascript"></script>
  <!-- Other scripts -->
  <script type="text/javascript">
	$(document).ready(function() {
		
		
	}); 
</script>












 
  
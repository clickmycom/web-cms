

<link rel="stylesheet" type="text/css"	href="<?=base_url()?>public/templates/matchdayone/css/style_team_personel.css">
<link rel="stylesheet" type="text/css" 	href="<?=base_url()?>public/templates/matchdayone/css/site_global.css?417434784"/>
<link rel="stylesheet" type="text/css" 	href="<?=base_url()?>public/templates/matchdayone/css/team.css" id="pagesheet"/>



<link 	href="<?=base_url()?>public/templates/matchdayone/css/match.css" rel='stylesheet' type='text/css'>
<script src="<?=base_url()?>public/templates/matchdayone/js/jquery.min.js"></script>

 
<link href="<?=base_url()?>public/templates/matchdayone/css/bootstrap.min.css" rel="stylesheet"/>
<link href="<?=base_url()?>public/templates/matchdayone/css/bootstrap-social.css" rel="stylesheet"/>
   
   
<script>

 function getdata_player(id_player){

      	$.post("<?=base_url().$this->router->class.'/view_player/'?>",{'id_player':id_player}).done(function(){
			window.location = "<?=base_url().$this->router->class.'/view_player/'?>";
		});
 }

      
 
$(function(){
	
	$.post('<?=$url_site?>rest/services/getContentPersonnel_all',{club_id:<?=$get_club_id?>}).done(function(data){
      	  if(data!=null){
	      	  temp = jQuery.parseJSON(data); 
	      	  console.log(temp.personnel);
	      	  getPersonnel_view(temp.personnel);
	      	
      	  }
    }).fail(function(){
      		
      	  sweetAlert("Oops..Internet disconnect", "Please try again", "error");
      	 
	      window.location = window.location;
    });
      

       function getPersonnel_view(data){
       		console.log(data);
       		
       		       		
       		$j=0;
       		for(i in data){
       			$pic 		= '<?=$url_site?>'+data[i].picture;
       			$birthday	= data[i].birthdate;
       			$mail		= data[i].email;
       			$height		= data[i].hight;
       			$weight		= data[i].weight;
       			$Number		= data[i].unifrom_number;
       			
       			if(data[i].gender == "ชาย"){
       				$name 		= "&nbsp;&nbsp;นาย"+data[i].name+"&nbsp;&nbsp;"+data[i].surname;
       			}else{
       				$name 		= "&nbsp;&nbsp;นางสาว"+data[i].name+"&nbsp;&nbsp;"+data[i].surname;
       			}
       				
       			
       			
       			if(data[i].role_id == "Coach"){
       				$position	= "Coach";
       			}
       			
       			
       			//แสดงส่วนของรายละเอียดผู้เล่น
       			html_GK = "<div class='popup_anchor'><div class='Thumb popup_element rgba-background clearfix wp-tab-active PamphletThumbSelected' id='u4857'><div class='gradient rounded-corners clearfix grpelem' id='u4858-4'><p>&nbsp;</p></div><div class='clearfix grpelem' id='u4859-4'><p>"+$name+"</p></div></div></div>";
       			if($j == 0 && data[i].role_id == "Coach"){
       				

				html_data_player  = "<div class='Container rgba-background clearfix grpelem' >";
				html_data_player  +=	"<img id='u3974'>";  
				html_data_player  +=  "<div class='museBGSize grpelem' id='u3975'>";
				html_data_player  +=	"<img id='u3975' class='' src='"+$pic+"'>";  
				html_data_player  +=  "</div>";
				html_data_player  +=  "<div class='clearfix grpelem' id='pu3976-29'>";
				  
				html_data_player  +=    "<div class='clearfix colelem' id='u3976-29'>";
				
	
				html_data_player  +=      "<p>";
				html_data_player  +=        "<span id='u3976'>ชื่อ - สกุล&nbsp; :</span>&nbsp;"+$name+"";
				html_data_player  +=      "</p>";
				html_data_player  +=      "<p>";
				html_data_player  +=        "<span id='u3976-4'>ตำแหน่ง :</span>&nbsp;"+$position+"";
				html_data_player  +=      "</p>";
				html_data_player  +=      "<p>";
				html_data_player  +=        "<span id='u3976-7'>ส่วนสูง : </span>"+$height+" cm";
				html_data_player  +=      "</p>";
				html_data_player  +=      "<p>";
				html_data_player  +=        "<span id='u3976-10'>น้ำหนัก :</span>&nbsp;"+$weight+" kg";
				html_data_player  +=      "</p>";
				html_data_player  +=      "<p>";
				html_data_player  +=        "<span id='u3976-13'>วัน/เดือน/ปี เกิด :</span>&nbsp;"+$birthday+"";
				html_data_player  +=      "</p>";
				html_data_player  +=      "<p>";
				html_data_player  +=        "<span id='u3976-16'>อีเมล์ :</span>&nbsp;"+$mail+"";
				html_data_player  +=      "</p>";
			
				html_data_player  +=    "</div>";
				html_data_player  +=  "</div>";
				html_data_player  +="</div>";
						
       				

       				
       				$('#GK_Player_TOP').append(html_GK);
       				$('#data_player').append(html_data_player);
       				$j++;
       			}else if($j>0 && data[i].role_id == "Coach"){
       				
       				
	       			html_data_player  = "<div class='Container rgba-background clearfix grpelem ' >";
					html_data_player  +=	"<img id='u3974'>";     
					html_data_player  +=  "<div class='museBGSize grpelem' id='u3975'>";
					html_data_player  +=	"<img id='u3975' src='"+$pic+"'>";  
					html_data_player  +=  "</div>";
					html_data_player  +=  "<div class='clearfix grpelem' id='pu3976-29'>";
					  
					html_data_player  +=    "<div class='clearfix colelem' id='u3976-29'>";
					
					    
					html_data_player  +=      "<p>";
					html_data_player  +=        "<span id='u3976'>ชื่อ - สกุล&nbsp; :</span>&nbsp;"+$name+"";
					html_data_player  +=      "</p>";
					html_data_player  +=      "<p>";
					html_data_player  +=        "<span id='u3976-4'>ตำแหน่ง :</span>&nbsp;"+$position+"";
					html_data_player  +=      "</p>";
					html_data_player  +=      "<p>";
					html_data_player  +=        "<span id='u3976-7'>ส่วนสูง : </span>"+$height+" cm";
					html_data_player  +=      "</p>";
					html_data_player  +=      "<p>";
					html_data_player  +=        "<span id='u3976-10'>น้ำหนัก :</span>&nbsp;"+$weight+" kg";
					html_data_player  +=      "</p>";
					html_data_player  +=      "<p>";
					html_data_player  +=        "<span id='u3976-13'>วัน/เดือน/ปี เกิด :</span>&nbsp;"+$birthday+"";
					html_data_player  +=      "</p>";
					html_data_player  +=      "<p>";
					html_data_player  +=        "<span id='u3976-16'>อีเมล์ :</span>&nbsp;"+$mail+"";
					html_data_player  +=      "</p>";
				
					html_data_player  +=    "</div>";
					html_data_player  +=  "</div>";
					html_data_player  +="</div>";

       				html_GK_Long = "<div  class='popup_anchor'><div class='Thumb popup_element rgba-background clearfix wp-tab-active ' id='u4018'><div class='gradient rounded-corners clearfix grpelem' id='u4858-4'><p></p></div><div class='clearfix grpelem' id='u4020-4'><p >"+$name+"</p></div></div></div>	<br><br>";
       			
       				$('#GK_Player_LONG').append(html_GK_Long);
       				$('#data_player').append(html_data_player);
       			}
       			
				//ส่วนล่างงงงงง
       			//console.log(data[i].name);
       			$pic = '<?=$url_site?>'+data[i].picture;
       			//html = "<div class='player-profile'><img src='"+$pic+"' class='thumbnail'><span class='number'>"+data[i].unifrom_number+"</span><span class='name'>"+data[i].name+"&nbsp;"+data[i].surname;+"</span></div>";
       			
       			html = 	"<div class='player-profile data-pop-id'>";
	            html += "   <div class='view view-ninth '>";
	            html += " <a  onclick='getdata_player("+data[i].id+")' >";
	            html += "       <img src='"+$pic+"' class='thumbnail detail_pl' />";
	          
	            html += "       <div class='mask mask-2'></div>"; 
	            
	            html += " </a>";
	            html += "   </div>";
 				html += "</div>";
       			
       			if(data[i].first_position_id ==null  && data[i].role_id == "Coach"){
       				
       				$('#all_personnel').append(html);
       			}
       			
       		
       			//ส่วนล่างงงงงง
       			       			
       		}
       		

       		slideNew();
      }
      
      
     
      
      function slideNew(){	
			try {
			
				Muse.Utils.transformMarkupToFixBrowserProblemsPreInit();
				console.log(Muse);
				/* body */
				$('.browser_width').toBrowserWidth();
				/* browser width elements */
				Muse.Utils.prepHyperlinks(true);
				/* body */
				Muse.Utils.initWidget('#pamphletu98', function(elem) {
					new WebPro.Widget.ContentSlideShow(elem, {
						contentLayout_runtime : 'stack',
						event : 'click',
						deactivationEvent : 'none',
						autoPlay : true,
						displayInterval : 3000,
						transitionStyle : 'vertical',
						transitionDuration : 500,
						hideAllContentsFirst : false,
						shuffle : false,
						enableSwipe : true
					});
				});
				/* #pamphletu98 */
				Muse.Utils.initWidget('.MenuBar', function(elem) {
					return $(elem).museMenu();
				});
				/* unifiedNavBar */
				Muse.Utils.initWidget('#pamphletu3951', function(elem) {
					new WebPro.Widget.ContentSlideShow(elem, {
						contentLayout_runtime : 'stack',
						event : 'click',
						deactivationEvent : 'none',
						autoPlay : false,
						displayInterval : 3000,
						transitionStyle : 'horizontal',
						transitionDuration : 500,
						hideAllContentsFirst : false,
						shuffle : false,
						enableSwipe : true
					});
				});
				/* #pamphletu3951 */
				Muse.Utils.initWidget('#pamphletu4778', function(elem) {
					
					new WebPro.Widget.ContentSlideShow(elem, {
						contentLayout_runtime : 'stack',
						event : 'click',
						deactivationEvent : 'none',
						autoPlay : false,
						displayInterval : 3000,
						transitionStyle : 'horizontal',
						transitionDuration : 500,
						hideAllContentsFirst : false,
						shuffle : false,
						enableSwipe : true
					});
				});
				/* #pamphletu4778 */
				Muse.Utils.initWidget('#tab-panelu3827', function(elem) {
					return new WebPro.Widget.TabbedPanels(elem, {
						event : 'click',
						defaultIndex : 0
					});
				});
				/* #tab-panelu3827 */
				Muse.Utils.initWidget('#widgetu248', function(elem) {
					new WebPro.Widget.Form(elem, {
						validationEvent : 'submit',
						errorStateSensitivity : 'high',
						fieldWrapperClass : 'fld-grp',
						formSubmittedClass : 'frm-sub-st',
						formErrorClass : 'frm-subm-err-st',
						formDeliveredClass : 'frm-subm-ok-st',
						notEmptyClass : 'non-empty-st',
						focusClass : 'focus-st',
						invalidClass : 'fld-err-st',
						requiredClass : 'fld-err-st',
						ajaxSubmit : true
					});
				});
				
				/* 100% height page */
				Muse.Utils.showWidgetsWhenReady();
				/* body */
				Muse.Utils.transformMarkupToFixBrowserProblems();
				/* body */
			
			} catch(e) {
				Muse.Assert.fail('Error calling selector function:' + e);
			}
		}
});
	 function errImage(ele){
	 	ele.src= "https://www.zombeewatch.org/static/aboutus/noimage.png";
      	//alert(ele.src);
      }
</script>



<script>
	$(document).ready(function(){
  		$('#id_personel').click();
  		$(".todoitem").click();
	});
	
	
</script>
				
			









 
<div id="page_position_content" style="height: 600px;">
		
		
		<div class="TabbedPanelsWidget  " id="tab-panelu3827">
			<!-- vertical box -->
			<div class="TabbedPanelsTabGroup clearfix colelem" id="u3848">
				<!-- horizontal box -->
				<div class="TabbedPanelsTab clearfix grpelem" id="u3853">
					<!-- horizontal box -->
					<div class=" NoWrap clearfix grpelem" id="u3855-4">
						<!-- content -->
						<p>
							Coach
						</p>
					</div>
					<div class="TabbedPanelsTab clearfix grpelem TabbedPanelsTabLast" id="u4682"><!-- horizontal box -->
				       <div class=" NoWrap clearfix grpelem" id="u4685-4"><!-- content -->
				        
				       </div>
				    </div>
				</div>
			</div>
			<div class="TabbedPanelsContentGroup clearfix colelem" id="u3857">
				<!-- stack box -->
				<div class="TabbedPanelsContent museBGSize clearfix grpelem" id="">
					<!-- group -->
					<div class="PamphletWidget clearfix grpelem" id="pamphletu3951">
						<!-- none box -->
						<div class="popup_anchor" id="u3961popup">							
							<div class="ContainerGroup clearfix" id="u3961">
							
						<!-- 000000000000000000000000000000000 -->				
								<div id="data_player">
									
								</div>
						<!-- 000000000000000000000000000000000 -->

							</div>
						</div>
						<div class="ThumbGroup clearfix grpelem" id="u4008">
							<!-- none box -->
						
							<div id="GK_Player_TOP"></div>
							<div id="GK_Player_LONG"></div>
							
							

							
							
							
						</div>
					</div>
				</div>

			</div>
		</div>
</div>

 
	
    
    
    
    
<!-- Start About -->

	<!-- Start Teams -->
	<div id="teams" class="teams" >
		
			<!-- Start Heading -->
			<div class="heading col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<h4 >Our Teams</h4>
			</div>
			<!-- Start Teams-Frame -->
			<div class="teams-frame col-lg-12">
				<!-- Start Nav-Tabs -->
				<ul class="nav nav-tabs vertical-tab col-md-8" role="tablist">
					<li class="active">
						<a href="#all-team" role="tab" data-toggle="tab">COACH</a>
					</li>
					
				</ul>
				<!-- End Nav-Tabs -->

				<!-- Start Tab Seniors -->
				<div class="tab-pane fade active in" id="all-team" style="height: 95px;">

					<!-- Start Team-Players -->
					<div class="team-players">
						<div id="all_personnel"></div>
					</div>
					<!-- E	nd Team-Players -->

				</div>
				<!-- End Tab Seniors -->

				<!-- Start Tab Juveline -->
				<div class="tab-pane fade" id="GoalKeepers-team" style="height: 95px;">

					<div class="team-players">

						<div id="Goal_personnel"></div>

					</div>
				</div>
				<!-- End Tab Juveline -->

				

				

			
	
			</div>
			<div class="heading col-lg-12 col-md-12 col-sm-12 col-xs-12">
				
			</div>
		
		
	</div>

		<!-- End Teams -->

   

<p>&nbsp;</p>

<!-- Scripts -->
<script src="<?=$template_paht?>js/jquery.min.js"></script>
<!-- <script src="<?=$template_paht?>/category/js/scripts.js"></script> -->
<script src="<?=$template_paht?>js/owl.carousel.min.js"></script>
<!-- <script src="<?=$template_paht?>/category/js/jquery.ba-outside-events.min.js"></script> -->
<!-- <script src="<?=$template_paht?>/category/js/tab.js"></script> -->
<!-- <script src='<?=$template_paht?>/category/js/bootstrap-datepicker.js'></script> -->
<!-- <script src="<?=$template_paht?>/category/js/jquery.vide.min.js"></script> -->
	

 <!-- JS includes -->
<script src="<?=$template_paht?>js/jquery-1.8.3.min.js" type="text/javascript"></script>
<script src="<?=$template_paht?>js/museutils.js?3865766194" type="text/javascript"></script>
<script src="<?=$template_paht?>js/jquery.tobrowserwidth.js?3842421675" type="text/javascript"></script>
<script src="<?=$template_paht?>js/webpro.js?3903299128" type="text/javascript"></script>
<script src="<?=$template_paht?>js/musewpslideshow.js?138381373" type="text/javascript"></script>
<script src="<?=$template_paht?>js/musewpdisclosure.js?4285748861" type="text/javascript"></script>
<script src="<?=$template_paht?>js/jquery.watch.js?4068933136" type="text/javascript"></script>
  <!-- Other scripts -->
  
</script>

<section>
<footer id="containerfooter" class="footerclass" role="contentinfo">
  <div class="container">
  	<div class="row">

			<div class="col-md-3 col-sm-6 footercol1">
				<div class="widget-1 widget-first footer-widget">
					<aside id="virtue_about_with_image-2" class="widget virtue_about_with_image">
						<div class="kad_img_upload_widget">
							<a href="http://www.kadencethemes.com/" target="_self"> <img src="http://www.kadencethemes.com/wp-content/uploads/2014/03/smallerlogo_white.png" alt=""> </a>
						</div>

					</aside>
				</div>
			</div>

			<div class="col-md-3 col-sm-6 footercol2">
				<div class="widget-1 widget-first footer-widget">
					<aside id="text-2" class="widget widget_text">
						<h3>About Us</h3>
						<div class="textwidget">
							We are out to create clean professional class wordpress themes that help everyday people create beautiful websites.
						</div>
					</aside>
				</div>
			</div>

			<div class="col-md-3 col-sm-6 footercol3">
				<div class="widget-1 widget-first footer-widget">
					<aside id="widget_kadence_social-2" class="widget widget_kadence_social">
						<h3>Social</h3>
						<div class="virtue_social_widget clearfix">

							<a href="#" class="facebook_link" title="" target="_blank" data-toggle="tooltip" data-placement="top" data-original-title="Facebook"><i class="icon-facebook"></i></a><a href="https://twitter.com/KadenceThemes" class="twitter_link" title="" target="_blank" data-toggle="tooltip" data-placement="top" data-original-title="Twitter"><i class="icon-twitter"></i></a><a href="https://instagram.com/kadencethemes/" class="instagram_link" title="" target="_blank" data-toggle="tooltip" data-placement="top" data-original-title="Instagram"><i class="icon-instagram"></i></a><a href="https://www.youtube.com/user/kadencethemes" class="youtube_link" title="" target="_blank" data-toggle="tooltip" data-placement="top" data-original-title="YouTube"><i class="icon-youtube"></i></a>
						</div>
					</aside>
				</div>
			</div>

			<div class="col-md-3 col-sm-6 footercol4">
				<div class="widget-1 widget-first footer-widget">
					<aside id="nav_menu-2" class="widget widget_nav_menu">
						<h3>Resources</h3>
						<ul id="menu-resources" class="menu">
							<li class="menu-support menu-item-173">
								<a href="http://www.kadencethemes.com/support/">Support</a>
							</li>
							<li class="menu-terms menu-item-2092">
								<a href="http://www.kadencethemes.com/terms-and-license/">Terms</a>
							</li>
							<li class="menu-affiliate menu-item-37085">
								<a href="http://www.kadencethemes.com/kadence-themes-affiliate/">Affiliate</a>
							</li>
							<li class="menu-login menu-item-174">
								<a href="http://www.kadencethemes.com/my-account/">Login</a>
							</li>
						</ul>
					</aside>
				</div>
			</div>
		</div>
		<!-- Row -->
		        		       
    </div><!-- container -->
</footer>
</section>
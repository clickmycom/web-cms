<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=$title?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<? $this->load->view("$template/layout/import_files/$css_js"); ?>
	
</head>
	
<?php
		
		$a =base_url()."public/upload_images/".$style_bg;
		$b = "background-image: url($a);";
	
		if(preg_match('/.png/',$style_bg) == 1){
			$bg = $b;
		}else if(preg_match('/.jpg/',$style_bg) == 1){
			$bg = $b;
		}else if(preg_match('/.gif/',$style_bg) == 1){
			$bg = $b;
		}else{
			$bg = "background-color:#$style_bg";
		}
?>
	
<style type="text/css" media="screen">
		body {
			
			font-family: 'open sans', "Helvetica Neue", Helvetica, Arial, sans-serif;
			font-size: 12px;
			line-height: 1.42857143;
			color: #3e3e3e;
			<?=$bg?>
		}
</style>




<body>

	<!-- Start div class container -->     

				<? $this->load->view("$template_menu/layout/menu_view.php"); ?>
		
				<? $this->load->view("$content")?>	
			
			<!-- Begin Footer -->
			<footer class="footer" >
				<? $this->load->view("templates/layout_one/layout/footer_view.php");?>
				<? $this->load->view("$copy_right/layout/copyright_view.php"); ?>				
			</footer>
			<!-- End Footer --> 
				
	</div>
	<!-- End div class container --> 



<!-- Javascript Files -->
	
	<script type="text/javascript" src="<?=$template_paht?>/category/js/hover-dropdown.js"></script>
	<script type="text/javascript" src="<?=$template_paht?>/category/js/jquery.easing.min.js"></script>
	<script type="text/javascript" src="<?=$template_paht?>/category/js/jquery.mixitup.min.js"></script>
	<script type="text/javascript" src="<?=$template_paht?>/category/js/scrollReveal.js"></script>
	<script type="text/javascript" src="<?=$template_paht?>/category/js/owl.carousel.min.js"></script>
	<script type="text/javascript" src="<?=$template_paht?>/category/js/jquery.magnific-popup.min.js"></script>
	<script type="text/javascript" src="<?=$template_paht?>/category/js/jquery.snippet.min.js"></script>
	<script type="text/javascript" src="<?=$template_paht?>/category/js/skrollr.min.js"></script>
    
    <script>
		var config = {
			easing: 'hustle',
			mobile:  true,
			delay:  'onload'
		}
		window.sr = new scrollReveal( config );
    </script>
	
	<script type="text/javascript" src="<?=$template_paht?>/category/js/main.js"></script>



<script>
	
	$(function(){
		$('img').error(function(){
        	$(this).attr('src', '<?=base_url();?>public/templates/team/img/thumb.png');
		});
	});
	
	

</script>
</body>
</html>
</body>
</html>
<script>
	function errImage(ele){
        ele.src= "<?=base_url();?>public/templates/team/img/thumb.png";
        //alert(ele.src);
	}
</script>

<style>
.boxed {
    background-color: #fff;
    max-width: 1150px;
    margin: 0 auto !important;
    border-left: solid 1px #e4e9f0;
    border-right: solid 1px #e4e9f0;
    -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.1), inset 0 0 2px rgba(255,255,255,.21);
    -moz-box-shadow: 0 1px 2px rgba(0,0,0,.1), inset 0 0 2px rgba(255,255,255,.21);
    box-shadow: 0 1px 2px rgba(0,0,0,.1), inset 0 0 2px rgba(255,255,255,.21);
   
}
</style>

<div class="boxed">
				<!-- Begin Page Header -->
				<div class="header">
					<div class="container">
							<!-- Page Title -->
							<div style="position: relative;right:15px;">
								<div class="col-sm-6 col-xs-12">
									<h1><?=$section_id = $this->session->userdata('section_menu_name')?></h1>
									
								</div>
							</div>
							<!-- Breadcrumbs -->
							<div class="col-sm-6">
								<ol class="breadcrumb">
									<li><span class="ion-home breadcrumb-home"></span><a href="<?=$link_home?>">Home</a></li>
									
									<?php
										if($section_id = $this->session->userdata('section_menu_head_name')!=null){
									?>
											<li><?=$section_id = $this->session->userdata('section_menu_head_name')?></li>
									<?	}
									?>
									<li><?=$section_id = $this->session->userdata('section_menu_name')?></li>
								</ol>
							</div><!-- /column -->  
					
					</div><!-- /container -->  
				</div><!-- /page header -->  

			<!-- End Header -->
	
		<?php
		
				
				$i=0;
				
				$a=array(
						"#8B7D6B",
						"#8B7765",
						"#8B8970",
						"#8B8B83",
						"#473C8B",
						"#36648B",
						"#4A708B",
						"#6E7B8B",
						"#00868B",
						"#008B8B",
						"#008B45",
						"#BC8F8F"
						
				   );
	
				
				$random_keys=array_rand($a,3);
				foreach ($news as $data_view) {
						
						if($data_view['article_image']!=null){
							$imagedata = getimagesize($url_img.$data_view['article_image']);
							// echo "Image width  is: " . $imagedata[0];
							// echo  "Image height is: " . $imagedata[1];
							$width 	= $imagedata[0];
							$height = $imagedata[1];
						}

						
						if($i==0){																					
		?>
							<!-- Begin Content Section -->
										<section class="" style="background:#fafafa">
											<div class="container">
							
												<!-- Begin Freebie -->
												<div class="row">
													<div class="col-sm-12">														
														<p class="lead text-center flipInY-animated"><b><mark><?=$section_id = $this->session->userdata('section_menu_name')?></mark></b></p>
														<hr style="width:600px">
													</div>
												</div>
												
							
												<div class="row">
													<!-- Content -->
													<div class="col-sm-7 fadeInLeft-animated">
														<h3 class="mb15"><?=$data_view['article_name']?></h3>
														<p>
															
															<?php
																	if($data_view['article_image']!=null){
																		$string = strip_tags($data_view['article_detail'],"<b>");
																		echo $string = word_limiter($string,$height/6.2);
																	}else{
																		$string = strip_tags($data_view['article_detail'],"<b>");
																		echo $string = word_limiter($string,50);
																	}
				
															?>

														</p>
														
														<a data-id_detail="<?=$data_view['id']?>" data-total_view="<?=$data_view['hitz']?>" href="#" class="btn btn-rw btn-primary mt10 detail_id">
															รายละเอียดเพิ่มเติม
														</a>
														<br><br>
													</div><!-- /column -->
													<!-- End Content -->
							
													<!-- Video -->
													<?php
																
																
																if(@$height <= 450 && $data_view['article_image']!=null){
													?>
																	<div >
																		<br>
																		<center><img onerror='errImage(this)' title="<?=$data_view['tooltip']?>" src="<?=$url_img.$data_view['article_image']?>" class="img-responsive fadeInRight-animated thumbnail"  width="400"></center>
																	</div><!-- /column -->				
													<?php
																}else{
													?>
																	<div >
																		
																		<center><img onerror='errImage(this)' title="<?=$data_view['tooltip']?>" src="<?=$url_img.$data_view['article_image']?>" class="img-responsive fadeInRight-animated thumbnail"  width="220"></center>
																	</div><!-- /column -->	
													<?php				
																}
													?>
															 
													
												</div><!-- /row -->
												
												<!-- End Freebie -->
							
											</div><!-- /container -->
										</section><!-- /section -->
							<!-- End Content Section -->
						
		<?php						
						   
						}else{
							
									if($i==1){
		?>								
													<!-- Begin Freebie -->
														<section class="pt60 pb60" style="background:<?php echo $a[$random_keys[$i]];?>">
															<div class="container">
																<!-- Begin Freebie -->
																<div class="row">
																	<!-- Content -->
																	<div class="col-sm-4 fadeInLeft-animated">
																<?php	
																		
																			if(@$height <= 450 && $data_view['article_image']!=null){
																?>
																				<div >
																					<br>
																					<center><img onerror='errImage(this)' title="<?=$data_view['tooltip']?>" src="<?=$url_img.$data_view['article_image']?>" class="img-responsive fadeInRight-animated thumbnail"  width="400"></center>
																				</div><!-- /column -->				
																<?php
																			}else{
																?>
																				<div >
																					
																					<center><img onerror='errImage(this)' title="<?=$data_view['tooltip']?>" src="<?=$url_img.$data_view['article_image']?>" class="img-responsive fadeInRight-animated thumbnail"  width="250"></center>
																				</div><!-- /column -->	
																<?php				
																			}
																?>
													
													
														
																		
																	
																	</div><!-- /column -->
																	<!-- End Content -->
											
																	<!-- Video -->
																	<div class="col-sm-8 text-white">
																		<h3 class="text-white"><?=$data_view['article_name']?></h3>
																		
																		<p style="color:#fff; width: 97%;">
																			
																				
																										
																				<?php
																					if($data_view['article_image']!=null){
																						$string = strip_tags($data_view['article_detail'],"<b>");
																						echo $string = word_limiter($string,$height/6.2);
																					}else{
																						$string = strip_tags($data_view['article_detail'],"<b>");
																						echo $string = word_limiter($string,50);
																					}
					
																				?>
																
																			</p>
																		<a data-id_detail="<?=$data_view['id']?>" data-total_view="<?=$data_view['hitz']?>" href="#" class="btn btn-rw btn-clear mt10 detail_id">
																			รายละเอียดเพิ่มเติม
																		</a>
																	</div><!-- /column -->
																</div><!-- /row -->
																<!-- End Freebie -->
															</div><!-- /container -->
														</section><!-- /section -->
												<!-- End Freebie -->					
		<?						
									}else if($i==2){
															$imagedata = getimagesize($url_img.$data_view['article_image']);

																$width 	= $imagedata[0];
																$height = $imagedata[1];
		?>
													<!-- Begin Freebie -->
													<section class="pt60 pb60" style="background:<?php echo $a[$random_keys[$i]];?>">
														<div class="container">
															<!-- Begin Freebie -->
														<div class="row">
																<!-- Content -->
																<div style="color: #fff;" class="col-sm-7 fadeInLeft-animated">
																	<h3 class="mb15" style="color: #fff;"><?=$data_view['article_name']?></h3>
																	<p style="color:#fff;width: 97%;"">
																		
																								
																				<?php
																					if($data_view['article_image']!=null){
																						$string = strip_tags($data_view['article_detail'],"<b>");
																						echo $string = word_limiter($string,$height/6.2);
																					}else{
																						$string = strip_tags($data_view['article_detail'],"<b>");
																						echo $string = word_limiter($string,50);
																					}
					
																				?>
																	</p>
																	
																		<a data-id_detail="<?=$data_view['id']?>" data-total_view="<?=$data_view['hitz']?>" href="#" class="btn btn-rw btn-clear mt10 detail_id ">
																			รายละเอียดเพิ่มเติม
																		</a>
																</div><!-- /column -->
																<!-- End Content -->
										
																<!-- Video -->
															
																<div class="col-sm-5 mt30-xs">
																<?php																
																			if(@$height <= 450 && $data_view['article_image']!=null){
																?>
																				<div >
																					<br>
																					<center><img onerror='errImage(this)' title="<?=$data_view['tooltip']?>" src="<?=$url_img.$data_view['article_image']?>" class="img-responsive fadeInRight-animated thumbnail"  width="400"></center>
																				</div><!-- /column -->				
																<?php
																			}else{
																?>
																				<div >
																					
																					<center><img onerror='errImage(this)' title="<?=$data_view['tooltip']?>" src="<?=$url_img.$data_view['article_image']?>" class="img-responsive fadeInRight-animated thumbnail"  width="250"></center>
																				</div><!-- /column -->	
																<?php				
																			}
																?>
																</div><!-- /column -->
															</div><!-- /row -->
															<!-- End Freebie -->
														</div><!-- /container -->
													</section><!-- /section -->
											<!-- End Freebie -->	
											
											
														<!-- Begin Recent Freebies -->
														<section class="background-light-grey border-top pt40 mb10">
															<div class="container">		
																<div class="row">
																	<div class="col-lg-12">
																		<div class="heading mb30">
																			<h4><span class="ion-android-social-user mr15"></span>Recent Freebies</h4>
															                <div class="owl-controls">
															                    <div id="customNav" class="owl-nav"></div>
															                </div>
																		</div>
																		<div id="owl-carousel-thumb" class="owl-carousel">
																	
		<?php							}else if($i>=3){
		?>									

																																		
																			
														<div class="thumbnail">
											                <div class="caption">
											                	<div class="thumbnail-caption-holder">
												                    <h4 class="text-white"><?=$data_view['article_name']?></h4>
												                    <a data-id_detail="<?=$data_view['id']?>" data-total_view="<?=$data_view['hitz']?>" href="#" class="btn btn-rw btn-clear detail_id"><span class="ion-android-search"></span> รายละเอียด</a>
											                    </div>
											                </div>
											                <img onerror='errImage(this)' title="<?=$data_view['tooltip']?>" style="width:100%;height:150px;" src="<?=$url_img.$data_view['article_image']?>" alt="...">
											            </div>
																
		<?php						}					
							}
		
								
						$i++;	
				} // end for loop	
													

		
		?>
			
																</div>
											
															</div><!-- /container -->
														</section><!-- /section -->
														<!-- End Recent Freebies -->
			

	<!-- End Boxed or Fullwidth Layout -->
</div>
	





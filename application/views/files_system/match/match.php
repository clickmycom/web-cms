
<link 	href="<?=base_url()?>public/files_system/match/css/match.css" rel='stylesheet' type='text/css'>

<script src="<?=base_url()?>public/files_system/match/js/jquery.min.js"></script>


<script>

var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();
var hh	= today.getHours();
var mi	= today.getMinutes();
var ss 	= today.getSeconds();
var sss	= today.getMilliseconds();

if(dd<10) {
    dd='0'+dd
} 

if(mm<10) {
    mm='0'+mm
}
if(mi<10){
	mi='0'+mi
}
if(ss<10){
	ss='0'+ss
} 

today = yyyy+'-'+mm+'-'+dd+' '+hh+':'+mi+':'+ss;


$(function(){
//console.log(today.trim());

	$.post('<?=$url_site?>rest/services/getMatch',{club_id:<?=$get_club_id?>}).done(function(data){
      	  if(data!=null){
	      	  temp = jQuery.parseJSON(data); 
	      	 // console.log(temp);
	      	  getMatch(temp);
	      	
      	  }
    }).fail(function(){
      		
      	  sweetAlert("Oops..Internet disconnect", "Please try again", "error");
      	 
	      window.location = window.location;
    });
    
    
    function getMatch(data){
    	//console.log(data);
		
	 	
		
		$j=0;
		for(i in data){
	
			// **********
			// cover date
			// **********
			var str = data[i].match_day;
			var day = str.replace(".000", "");
			
			var n = day.search(" ");
			var sub = day.substring(0, n);
			
			var n_day = sub.search("-");
			
			var d = new Date(sub);
    		var new_date = d.toLocaleString();
    
    		var n2 = new_date.search(" ");
    		var date = new_date.substring(0, n);
		
			// **********
			// cover date
			// **********
			
			// -------------------------------
			// **********
			// cover time
			// **********
			var sub_time = day.substring(n,16);
			
			// **********
			// cover time
			// **********
			// -------------------------------
			
			//console.log(sub_time);
			
			
			//แปลง คำ
			var name_club2 = data[i].club_name2;
			var day = str.replace("Football Club", "FC");
			
			
			if($j == 0 ){
				if(data[i].match_day.trim() >= today.trim()){
					
					
					html =   "<div class='team'>";
					html += 	 "<figure>";
					html += 		 "<img style='border: solid 1px #fff;' src='<?=base_url();?>public/templates/team/img/background.jpg'  onerror='errImage(this)' class='team-background'>";
					html += 		 "<img src='<?=$url_site?>"+data[i].club_logo1+"'  onerror='errImage(this)' class='team-logo'>";
					html += 		 "<figcaption>";
					html += 			""+data[i].club_name1.replace("Football Club", "FC").trim()+"";
					html += 		 "</figcaption>";
					html += 	 "</figure>";
					html +=  "</div>";
					html +=  "<div class='match-details'>";
					html += 	 "<header class='match-name'>";
					html += 		 "<h5>"+data[i].tour_name+"</h5>";
					html += 	 "</header>";
					html += 	 "<div class='score' >";
					html += 		 "<span class='color'>V</span>";
					html += 		 "<span >S</span>";
					html += 	 "</div>";
					html += 	 "<footer class='schedule'>";
					html += 		 "<span class='team-name' style='font-size: 11px;'>"+date+" </span>";
					html += 		 "<span class='time' style='font-size: 11px;'>ณ เวลา "+sub_time+"</span>";
					html += 	 "</footer>";
					html +=  "</div>";
					html +=  "<div class='team'>";
					html += 	 "<figure>";
					html += 		 "<img style='border: solid 1px #fff;' src='<?=base_url();?>public/templates/team/img/background.jpg' onerror='errImage(this)' class='team-background'>";
					html += 		 "<img src='<?=$url_site?>"+data[i].club_logo2+"' onerror='errImage(this)' class='team-logo'>";
					html += 		 "<figcaption>";
					html += 			 ""+data[i].club_name2.replace("Football Club", "FC").trim()+"";
					html += 		 "</figcaption>";
					html += 	 "</figure>";
					html +=  "</div>";
					
					
				$j++;
				$('#Start_Result').append(html);
				}
			}else{
				if(data[i].match_day.trim() >= today.trim()){
					console.log(data);		
					//console.log(data[i].match_day);
					html_comming =  " <li class='match'>";
					html_comming +=  " 	<div class='team text-left'>";
					html_comming +=  " 		<img src='<?=$url_site?>"+data[i].club_logo1+"' onerror='errImage(this)'><span>"+data[i].club_name1.replace("Football Club", "FC").trim()+"</span>";
					html_comming +=  " 	</div>";
					html_comming +=  " 	<div class='schedule'>";
					html_comming +=  " 		<span class='team-name'>"+data[i].tour_name.trim()+"</span>";
					html_comming +=  " 		<span class='time'>"+date+"</span>";
					html_comming +=  " 	</div>";
					html_comming +=  " 	<div class='team right text-right'>";
					html_comming +=  " 		<span>"+data[i].club_name2.replace("Football Club", "FC").trim()+"</span>";
					html_comming +=  " 		<img src='<?=$url_site?>"+data[i].club_logo2+"' onerror='errImage(this)'>";
					html_comming +=  " 	</div>";
					html_comming +=  " </li>";
					$('.start-result').append(html_comming);	

					
						
				}
			}
			
				
		}
		

    }

});
	 
	 
 
</script>

<div style="height: 15px;"></div>
 <!-- Start About -->
  <div id="about" class="about">   
    <!-- Start Tab-Content -->
    <div class="tab-content">
    	
      <div class="container">
      	
        <div  id="matches">
          <!-- Start Tab Seniors -->
          <div  id="seniors-matches">
	
            <!-- Start Result -->
            <div class="result col-lg-6 col-lg-offset-0 ">
             
             	<div id="Start_Result">
             		<!-- ************************* -->
             		<!-- Display Show Start Result -->
             		<!-- ************************* -->
             	</div>
              
            </div>
            <!-- End Result -->

            <!-- Start Upcoming -->
            <div class="upcoming col-lg-6 col-lg-offset-0 col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 clearfix ">
             
                
          	
          		
              
              <ul class="matches custom-list start-result">
                
                
               
               </ul>
               
               
              
              <a class="button text-left prev"><i class="fa fa-angle-left"></i><span>ย้อนกลับ</span></a>
              <div class="match-week">
                <div> 
                  <span class="team-name">โปรแกรมแข่งขัน</span>
                </div>
              </div>
              <a class="button text-right next"><span>ถัดไป</span><i class="fa fa-angle-right"></i></a>
            </div>
            <!-- End Upcoming -->

          </div>
          <!-- End Tab Seniors -->

        </div>
        <!-- End Trainings -->

      </div>
    </div>
    <!-- End Tab-Content -->
    
  </div>
  <!-- End About -->


<script src="<?=base_url()?>public/files_system/match/js/scripts.js"></script>
<script src="<?=base_url()?>public/files_system/match/js/owl.carousel.min.js"></script>

<script src="<?=base_url()?>public/files_system/match/js/bootstrap-datepicker.js"></script>


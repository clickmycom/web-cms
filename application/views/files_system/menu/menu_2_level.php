

<link href="<?=base_url()?>public/files_system/menu/css/bootstrap.min.css" rel="stylesheet"/>


<script>
$(function(){
	$('.menu2').on('click',function(){
		
		section_link			= $(this).data('link_data');
		
		if(section_link == null){
			section_link 		= "category_view";
			
		}else{
			var link = ["category_view", "index", "category_single","category_view/"];
    		var a = link.indexOf(section_link);
		}
		
    	//alert(a);
		section 				= $(this).data('section');
		section_id 				= $(this).data('val_id');
		section_menu_name		= $(this).data('menu_name');
		section_id_category		= $(this).data('val_category_id');
		section_menu_head_name	= $(this).data('menu_head_name');
		
		//alert(a);
		//console.log(section+" "+section_id+" "+section_menu_name+" "+section_id_category+" "+section_menu_head_name);
		if(a!=-1){
			$.post("<?=base_url().$this->router->class.'/category_view/'?>",{'section_id_category':section_id_category,'section':section,'section_id':section_id,'section_menu_name':section_menu_name,'section_menu_head_name':section_menu_head_name}).done(function(){
					
						window.location = "<?=base_url().$this->router->class?>/"+section_link+"";
			});
		}else{
			window.location.href = "http://"+section_link+"";
		}
		

	});
	
	
	$('.menu_single').on('click',function(){
		
		
		section_link			= $(this).data('link_data');
	
		if(section_link == null){
			section_link 		= "category_view";
			
		}else{
			var link = ["category_view", "index", "category_single","category_view/"];
    		var a = link.indexOf(section_link);
    		
		}
		
    	
		
		//alert(section_link);
		section 				= $(this).data('section');
		section_id 				= $(this).data('val_id');
		section_menu_name		= $(this).data('menu_name');
		section_id_category		= $(this).data('val_category_id');
		section_menu_head_name	= $(this).data('menu_head_name');
		
		section_article_page	= $(this).data('article_page');
		
	
		if(a!=-1){
			$.post("<?=base_url().$this->router->class.'/category_single/'?>",{'section_article_page':section_article_page,'section_id_category':section_id_category,'section':section,'section_id':section_id,'section_menu_name':section_menu_name,'section_menu_head_name':section_menu_head_name}).done(function(){
			
				
					window.location = "<?=base_url().$this->router->class?>/"+section_link+"";
				
			
			});
		}else{
			window.location.href = "http://"+section_link+"";
		}

	});
	

	$('.detail_id').on('click',function(){
		id_detail		= $(this).data('id_detail');
		total_view		= $(this).data('total_view');
		

		$.post("<?=base_url().$this->router->class.'/detail_view/'?>",{'id_detail':id_detail,'total_view':total_view}).done(function(){
			window.location = "<?=base_url().$this->router->class.'/detail_view/'?>";
		});		
	});
	
	
	

});
	
</script>

 <ul class="nav navbar-nav ">
                  	  <li>                 

<!-- เริ่ม  ส่วนของเมนูด้านบน -->
                            				<?php
														$check_menu=null;  //ตัวแปรนี้ใช้สำหรับตรวจสอบว่า เมนูนั่นซ้ำกับเมนูที่เป็น dropdown หรือไม่
			                                            foreach ($menu as $menu_name) {
			                                            		foreach ($sub_category as $sub_menu) {
			                                            			
																	// เริ่ม  เช็คว่าเมนูเป็น dropdown menu หรือไม่
			                                            			if($menu_name['id'] == $sub_menu['category_id'] ){	                                            				
			                                            				 $check_menu=$menu_name['id']; //เช็คเมนูที่ซ้ำกับ dropdown 				
			                                 ?>		                                            				
			                            
			                                 						<!-- Start Dropdown for mobile -->
			                                 						
																		<li class="dropdown show_drop_moblie">
						                                                 
						                                                  <a  title="<?=$menu_name['tooltip']?>" data-menu_name="<?=$menu_name['name']?>"  data-val_category_id="<?=$menu_name['id']?>" data-section="category_id" data-val_category_id="<?=$menu_name['id']?>" data-val_id="<?=$menu_name['id']?>"data-toggle="dropdown" data_menu="<?=$menu_name['name']?>" class="dropdown-toggle " value='<? echo $menu_name['id']?>'> <?php echo $menu_name['name']; ?> <b class="caret"></b></a>
						                                                  <ul class="dropdown-menu">			                                                  	
						                    <?php	
							                                				// วนลูป เพื่อแสดง sub menu
							                                				foreach ($sub_category as $sub_menu) {
																				 if($menu_name['id'] == $sub_menu['category_id']){
																					if($sub_menu['link']!=null && $sub_menu['article_id']==null){
											?>
																								<li><a title="<?=$sub_menu['tooltip']?>"  data-link_data="<?=$sub_menu['link']?>" data-article_page="<?=$sub_menu['article_id']?>" class="menu2"  id='d2'  data-menu_name="<?=$sub_menu['name']?>"  data-val_category_id="<?=$menu_name['id']?>" data-menu_head_name="<?=$menu_name['name']?>"  data-section="sub_category_id" data-val_id="<?=$sub_menu['id']?>"  value='<?$sub_menu['id']?>'     ><?php echo $sub_menu['name']; ?></a></li>												
											<?php									}else if($sub_menu['link']!=null && $sub_menu['article_id']!=null){
											?>	
																								<li><a title="<?=$sub_menu['tooltip']?>" data-link_data="<?=$sub_menu['link']?>" data-article_page="<?=$sub_menu['article_id']?>" data-article_page="<?=$sub_menu['article_id']?>" class="menu_single"  id='d2'  data-menu_name="<?=$sub_menu['name']?>"  data-val_category_id="<?=$menu_name['id']?>" data-menu_head_name="<?=$menu_name['name']?>"  data-section="sub_category_id" data-val_id="<?=$sub_menu['id']?>"  value='<?$sub_menu['id']?>'     ><?php echo $sub_menu['name']; ?></a></li>
											<?php									}else{
											?>
																								<li><a title="<?=$sub_menu['tooltip']?>" id='d2' data-menu_name="<?=$sub_menu['name']?>"  data-val_category_id="<?=$menu_name['id']?>" data-menu_head_name="<?=$menu_name['name']?>"  data-section="sub_category_id" data-val_id="<?=$sub_menu['id']?>" class="menu2" value='<?$sub_menu['id']?>'     href='#'><?php echo $sub_menu['name']; ?></a></li>						 	
											
											<?php										}	

																 				}	
																			}
																			// วนลูป เพื่อแสดง sub menu				                                									                                
						                    ?>                  		
						                                                  </ul>
					                                            		</li>	
					                                            		
					                                            		<!-- End Dropdown for mobile -->

					                                            		
					                                            	<!-- Start Dropdown for Desktop -->
			                                 							
					                                            	<li class="dropdown show_drop_desktop">
						                                                 
						                    <?php							if($menu_name['link']!=null){
						                    ?>
						                    									<a title="<?=$menu_name['tooltip']?>" data-link_data="<?=$menu_name['link']?>"   data-article_page="<?=$menu_name['article_id']?>" data-menu_name="<?=$menu_name['name']?>" data-val_category_id="<?=$menu_name['id']?>" data-section="category_id"  data-val_id="<?=$menu_name['id']?>" data-toggle="dropdown" class="dropdown-toggle menu2 " value='<? echo $menu_name['id']?>'> <?php echo $menu_name['name']; ?> <b class="caret"></b></a>										
						                    <?php							}else{
						                    ?>
						                  	  									<a title="<?=$menu_name['tooltip']?>" data-menu_name="<?=$menu_name['name']?>" data-val_category_id="<?=$menu_name['id']?>" data-section="category_id"  data-val_id="<?=$menu_name['id']?>" data-toggle="dropdown" class="dropdown-toggle menu2 " value='<? echo $menu_name['id']?>'> <?php echo $menu_name['name']; ?> <b class="caret"></b></a>
						                    <?php
																			}
											?>
						                                                  <ul class="dropdown-menu">			                                                  	
						                    <?php	
							                                				// วนลูป เพื่อแสดง sub menu
							                                				foreach ($sub_category as $sub_menu) {
																				 if($menu_name['id'] == $sub_menu['category_id']){
																				 	
																						if($sub_menu['link']!=null && $sub_menu['article_id']==null){
											?>
																								<li><a title="<?=$sub_menu['tooltip']?>" data-link_data="<?=$sub_menu['link']?>" class="menu2"  id='d2'  data-menu_name="<?=$sub_menu['name']?>"  data-val_category_id="<?=$menu_name['id']?>" data-menu_head_name="<?=$menu_name['name']?>"  data-section="sub_category_id" data-val_id="<?=$sub_menu['id']?>"  value='<?$sub_menu['id']?>'     ><?php echo $sub_menu['name']; ?></a></li>												
											<?php										}else if($sub_menu['link']!=null && $sub_menu['article_id']!=null){
											?>	
																								<li><a title="<?=$sub_menu['tooltip']?>" data-link_data="<?=$sub_menu['link']?>" data-article_page="<?=$sub_menu['article_id']?>" class="menu_single"  id='d2'  data-menu_name="<?=$sub_menu['name']?>"  data-val_category_id="<?=$menu_name['id']?>" data-menu_head_name="<?=$menu_name['name']?>"  data-section="sub_category_id" data-val_id="<?=$sub_menu['id']?>"  value='<?$sub_menu['id']?>'     ><?php echo $sub_menu['name']; ?></a></li>
											<?php										}else{
											?>
																								<li><a title="<?=$sub_menu['tooltip']?>" id='d2' data-menu_name="<?=$sub_menu['name']?>"  data-val_category_id="<?=$menu_name['id']?>" data-menu_head_name="<?=$menu_name['name']?>"  data-section="sub_category_id" data-val_id="<?=$sub_menu['id']?>" class="menu2" value='<?$sub_menu['id']?>'     href='#'><?php echo $sub_menu['name']; ?></a></li>						 							
											<?php										}																	 		
															 						 
																				 }
																			}
																			// วนลูป เพื่อแสดง sub menu				                                									                                
						                    ?>                  		
						              				
						                                                  </ul>
					                                            		</li>
					                                            		
					                                            		<!-- E Dropdown for Desktop -->
					                                         				                                            					                                            				                                            			                                            	
											<?php						break;
																	}
																	// จบ  เช็คว่าเมนูเป็น dropdown menu หรือไม่
																}
															
															
																//กรณีที่  Munu นั้นๆ ไม่มี Sub menu
																if($menu_name['id'] != $check_menu){
											?>
											
											<?php
																	if($menu_name['link']!=null && $menu_name['article_id']==null){
											?>										
																		<li>
							                                            		<a title="<?=$menu_name['tooltip']?>" data-link_data="<?=$menu_name['link']?>" data-menu_name="<?=$menu_name['name']?>" class='button1 menu2' data-val_id="<?=$menu_name['id']?>"  id="d3" data-section="category_id"  value='<?$menu_name['id']?>' >
																					<?php echo $menu_name['name']; ?>
																				</a>
																		</li>
											<?php
																	}else if($menu_name['link']!=null && $menu_name['article_id']!= null){
											?>
																		<li>
							                                            		<a title="<?=$menu_name['tooltip']?>" data-link_data="<?=$menu_name['link']?>" data-article_page="<?=$menu_name['article_id']?>"   data-menu_name="<?=$menu_name['name']?>" class='button1 menu_single' data-val_id="<?=$menu_name['id']?>"  id="d3" data-section="category_id"  value='<?$menu_name['id']?>' >
																					<?php echo $menu_name['name']; ?>
																				</a>
																		</li>
											
											<?php					}else{
											?>								
																		<li>
						                                            		<a title="<?=$menu_name['tooltip']?>" data-link_data="<?=$menu_name['link']?>" data-menu_name="<?=$menu_name['name']?>" class='button1 menu2' data-val_id="<?=$menu_name['id']?>"  id="d3"data-section="category_id"  value='<? echo $menu_name['id']?>' >
																				<?php echo $menu_name['name']; ?>
																			</a>
																		</li>	
											<?php					}
																}
														}
											?>
                                <!-- จบ  ส่วนของเมนูด้านบน -->
             </li>                            	
  </ul>
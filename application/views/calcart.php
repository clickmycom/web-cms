<table class="table table-hover">
    <thead>
        <tr>
            <th>Product</th>
            <th>Quantity</th>
            <th class="text-center">Price</th>
            <th class="text-center">Total</th>
            <th> </th>
        </tr>
    </thead>
    <tbody>
        <? foreach($this->cart->contents() as $item){?>
        <tr>
            <td class="col-sm-8 col-md-6">
            <div class="media">
                <a class="thumbnail pull-left" href="#"> <img class="media-object" src="http://icons.iconarchive.com/icons/custom-icon-design/flatastic-2/72/product-icon.png" style="width: 100px; height: 100px;"> </a>
                <div class="media-body">
                    <h4 class="media-heading"><a href="#"> <?=$item['name']?></a></h4>
                    <h5 class="media-heading">
                        <a href="#">
                            <? if($this->cart->has_options($item['rowid'])){
                                 foreach ($this->cart->product_options($item['rowid']) as $option_name => $option_value){?>
                                    <strong><?=$option_name?>:</strong><?=$option_value?><br />
                            <?}}?>
                        </a>
                    </h5>
                    <span>Status: </span><span class="text-success"><strong>In Stock</strong></span>
                </div>
            </div></td>
            <td class="col-sm-1 col-md-1" style="text-align: center">
            <input type="number" class="form-control qty" id="InputQty" data-rowid="<?=$item['rowid']?>" value="<?=$item['qty']?>" min="0">
            </td>
            <td class="col-sm-1 col-md-1 text-center"><strong>฿<?=$item['price']?></strong></td>
            <td class="col-sm-1 col-md-1 text-center"><strong>฿<?=$item['subtotal']?></strong></td>
            <td class="col-sm-1 col-md-1">
            <button type="button" class="btn btn-danger">
                <span class="glyphicon glyphicon-remove"></span> Remove
            </button></td>
        </tr>
        <? }?>
        <tr>
            <td>   </td>
            <td>   </td>
            <td>   </td>
            <td><h5>Subtotal</h5></td>
            <td class="text-right"><h5><strong>฿<?=number_format($this->cart->total(),2)?></strong></h5></td>
        </tr>
        <tr>
            <td>   </td>
            <td>   </td>
            <td>   </td>
            <td><h5>Estimated shipping</h5></td>
            <td class="text-right"><h5><strong>฿0.00</strong></h5></td>
        </tr>
        <tr>
            <td>   </td>
            <td>   </td>
            <td>   </td>
            <td><h3>Total</h3></td>
            <td class="text-right"><h3><strong>฿<?=number_format($this->cart->total(),2)?></strong></h3></td>
        </tr>
        <tr>
            <td>   </td>
            <td>   </td>
            <td>   </td>
            <td>
            <button type="button" class="btn btn-default">
                <span class="glyphicon glyphicon-shopping-cart"></span> Continue Shopping
            </button></td>
            <td>
            <button type="button" class="btn btn-success">
                Checkout <span class="glyphicon glyphicon-play"></span>
            </button></td>
        </tr>
    </tbody>
</table>
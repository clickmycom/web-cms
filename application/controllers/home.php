<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
    
    public function __construct()
    {
        parent::__construct();
        $this->template 			= 'templates/'.$this->get_model->get_template()->template;
		$this->template_menu		= 'templates/'.$this->get_model->get_template()->menu;
		$this->template_footer		= 'templates/'.$this->get_model->get_template()->footer;
		$this->copy_right			= 'templates/'.$this->get_model->get_template()->copy_right;
		$this->category_view 		= 'templates/'.$this->get_model->get_template()->category_view;
		$this->article_view 		= 'templates/'.$this->get_model->get_template()->article_view;
		$this->color_footer			= $this->get_model->get_template()->footer;
		$this->style_bg				= $this->get_model->get_template()->backgroud_color;
		$this->control_match		= $this->get_model->get_template()->show_nextmatch;
		
        $this->template_paht 		= base_url().'public/templates/'.$this->get_model->get_template()->template;	
		$this->url_img				= base_url().'public/upload_images/';
		$this->index_text			= $this->get_model->get_template()->index_text;
        $this->title 				= $this->get_model->get_template()->site_name;
		$this->index_text			= $this->get_model->get_template()->index_text;
		$this->menu 				= $this->get_model->get_mainmenu();
		$this->slide				= $this->get_model->get_image_url();
		$this->category				= $this->get_model->get_artical_template_two();
		$this->category_one			= $this->get_model->check_status_category();
		
		//$this->category_new			= $this->get_model->new_category();
		$this->sub_category			= $this->get_model->get_sub_category();
		
		$this->news					= $this->get_model->get_article();
		
		$this->link_home			= base_url();
		$this->get_url_site			= "http://192.168.1.123/sas/";
		//$this->category_view		= 'templates/'.$this->get_model->get_template()->category_view.'./category_view.php';
		$this->get_club_id			= $this->get_model->get_template()->club_id;
		$this->asset_matchdayone	= base_url()."public/templates/matchdayone/";

		
		
    }


   
	
	public function index()
	{
			
		//echo $this->session->userdata('id_detail');
		// print_r();
		
	    $data['template_paht'] 		= $this->template_paht;
		$data['template_menu']		= $this->template_menu;
		$data['template_footer']	= $this->template_footer;
		$data['copy_right']			= $this->copy_right;
		$data['template']			= $this->template;
		$data['css_js']				= 'index.php';
		$data['content'] 			= $this->template."/site/index.php";
		
		$data['color_footer']		= $this->color_footer;
        $data['title'] 				= $this->title;
		$data['index_text']			= $this->index_text;
		$data['menu']				= $this->menu;
       	$data['slide']				= $this->slide;
		
		//$data['category_new']		= $this->category_new;
		$data['category_sub']		= $this->category;
		$data['news']				= $this->news;
		$data['news_8']				= $this->get_model->get_article_8();
		
		$data['category']			= $this->category;
		$data['index_text']			= $this->index_text;
        $data['sub_category']		= $this->sub_category;
		$data['link_home']			= $this->link_home;
		$data['url_img']			= $this->url_img;
		$data['category_one']		= $this->category_one;
		
		
		$data['footer_view_box1']	= null;
		$data['footer_view_box2']	= null;
		$data['footer_view_box3']	= null;
		$data['footer_view_box4']	= null;

			
		$data['style_bg']				= $this->style_bg;	
		$data['get_club_id']			= $this->get_club_id;
		$data['url_site']				= $this->get_url_site;
		
		
		$data['footer_view_box1'] = @$this->footer_colum_1();
		$data['footer_view_box2'] = @$this->footer_colum_2();
		$data['footer_view_box3'] = @$this->footer_colum_3();
		$data['footer_view_box4'] = @$this->footer_colum_4();
		
		if($this->control_match == "Yes"){
			$this->load->view("files_system/match/match",$data);
		}
		
		$this->load->view($this->template.'/layout/layout.php',$data);
		
		//$this->load->view($this->template."/site/coach_view");
		
	}

	public function category_view(){

		if(@$_POST){
			$arr['section'] 				= $this->input->post('section');
		    $arr['section_id'] 				= $this->input->post('section_id');
			$arr['section_menu_name'] 		= $this->input->post('section_menu_name');
			$arr['section_menu_head_name']	= $this->input->post('section_menu_head_name');
			$arr['section_id_category']		= $this->input->post('section_id_category');
		    $this->session->set_userdata($arr);
		}	
		
		$this->session->userdata('section_id_category');
		 //echo  $this->session->userdata('section_id');
		 //echo  $this->session->userdata('section_menu_name');
		//exit;
		
		$this->articel 				= $this->get_model->get_category_view();
		
		$data['url_img']			= $this->url_img;	
		$data['template_paht'] 		= $this->template_paht;
        $data['title'] 				= $this->title;
		$data['menu']				= $this->menu;
    
		$data['category']			= $this->category;
	
        $data['sub_category']		= $this->sub_category;
		//$data['articel']			= $this->articel;
		$data['link_home']			= $this->link_home;
		$data['color_footer']		= $this->color_footer;
		
		$data['template_paht'] 		= $this->template_paht;
		$data['template_menu']		= $this->template_menu;
		$data['template_footer']	= $this->template_footer;
		$data['copy_right']			= $this->copy_right;
		$data['template']			= $this->template;
		$data['css_js']				= 'category.php';
		$data['content'] 			= $this->category_view."/site/category_view.php";
		
				
		$data['footer_view_box1']	= "";
		$data['footer_view_box2']	= "";
		$data['footer_view_box3']	= "";
		$data['footer_view_box4']	= "";
		
		$data['footer_view_box1'] = @$this->footer_colum_1();
		$data['footer_view_box2'] = @$this->footer_colum_2();
		$data['footer_view_box3'] = @$this->footer_colum_3();
		$data['footer_view_box4'] = @$this->footer_colum_4();
			
		$data['get_club_id']			= $this->get_club_id;
		$data['url_site']				= $this->get_url_site;
		
		$data['style_bg']				= $this->style_bg;	
		$data['get_club_id']			= $this->get_club_id;
		$data['url_site']				= $this->get_url_site;
		
		$data['news']				= $this->news;
		
		if($this->control_match == "Yes"){
			$this->load->view("files_system/match/match",$data);
		}
		
		$this->load->view($this->category_view.'/layout/layout.php',$data);
		//$this->load->view($this->category_view,$data);
			
	}
	
	
	public function detail_view()
	{
		
		if(@$_POST){
		    $arr['id_detail']               = $this->input->post('id_detail');
			$this->session->set_userdata($arr);
		}
		
		//echo  $this->session->userdata('total_view');
		

		$this->load->helper('text');
		$this->article_name			= $this->get_model->get_detail_view()->article_name;
		$this->article_image		= $this->get_model->get_detail_view()->article_image;
		$this->article_detail		= $this->get_model->get_detail_view()->article_detail;
		
		$this->hot_news				= $this->get_model->get_hot_news();
		$this->freebies				= $this->get_model->get_news_freebies();
		
		$data['template_paht'] 		= $this->template_paht;
        $data['title'] 				= $this->title;
		$data['menu']				= $this->menu;
       	$data['slide']				= $this->slide;
		$data['category']			= $this->category;
		$data['color_footer']		= $this->color_footer;
		$data['index_text']			= $this->index_text;
        $data['sub_category']		= $this->sub_category;			
		$data['link_home']			= $this->link_home;
		
		$data['article_name']		= $this->article_name;	
		$data['article_image']		= $this->article_image;	
		$data['article_detail']		= $this->article_detail;
		
		$data['hot_news']			= $this->hot_news;
		$data['freebies']			= $this->freebies;
		$data['url_img']			= $this->url_img;
		
		
		
		$data['template_paht'] 		= $this->template_paht;
		$data['template_menu']		= $this->template_menu;
		$data['template_footer']	= $this->template_footer;
		$data['copy_right']			= $this->copy_right;
		$data['template']			= $this->template;
		$data['css_js']				= 'detail.php';
		$data['content'] 			= $this->article_view."/site/detail_view.php";
		
		$data['hitz']				= $this->check_article_hitz($this->session->userdata('id_detail'));
		
		$data['footer_view_box1']	= "";
		$data['footer_view_box2']	= "";
		$data['footer_view_box3']	= "";
		$data['footer_view_box4']	= "";
		
		$data['footer_view_box1'] = @$this->footer_colum_1();
		$data['footer_view_box2'] = @$this->footer_colum_2();
		$data['footer_view_box3'] = @$this->footer_colum_3();
		$data['footer_view_box4'] = @$this->footer_colum_4();
		

		$data['style_bg']				= $this->style_bg;	
		$data['get_club_id']			= $this->get_club_id;
		$data['url_site']				= $this->get_url_site;
		
		
		if($this->control_match == "Yes"){
			$this->load->view("files_system/match/match",$data);
		}

		$this->load->view($this->article_view.'/layout/layout.php',$data);
		

		
		//$this->load->view("templates/layout_one/detail_view",$data);
	}


	

	public function team_personnel(){
		
		
		$data['url_img']			= $this->url_img;	
		$data['template_paht'] 		= $this->template_paht;
        $data['title'] 				= $this->title;
		$data['menu']				= $this->menu;
       	$data['slide']				= $this->slide;
		$data['category']			= $this->category;
		$data['index_text']			= $this->index_text;
        $data['sub_category']		= $this->sub_category;
		
		$data['link_home']			= $this->link_home;
		$data['color_footer']		= $this->color_footer;
		
		$data['template_paht'] 		= $this->template_paht;
		$data['template_menu']		= $this->template_menu;
		$data['template_footer']	= $this->template_footer;
		$data['copy_right']			= $this->copy_right;
		$data['template']			= $this->template;
		$data['css_js']				= 'category.php';
		$data['content'] 			= $this->category_view."/site/team_personnel_view.php";
		
		
		
		$data['footer_view_box1']	= "";
		$data['footer_view_box2']	= "";
		$data['footer_view_box3']	= "";
		$data['footer_view_box4']	= "";
		
		$data['footer_view_box1'] = @$this->footer_colum_1();
		$data['footer_view_box2'] = @$this->footer_colum_2();
		$data['footer_view_box3'] = @$this->footer_colum_3();
		$data['footer_view_box4'] = @$this->footer_colum_4();
		
		$data['style_bg']				= $this->style_bg;	
		$data['get_club_id']			= $this->get_club_id;
		$data['url_site']				= $this->get_url_site;
		
		
	
		//$this->load->view($this->category_view.'/layout/layout.php',$data);
		$this->load->view("matchdayone/team_personnel_view",$data);
		//$this->load->view($this->category_view,$data);
			
	}

	public function view_player(){
		
		
		if(@$_POST){
			$arr['id_player'] 				= $this->input->post('id_player');
			$arr['table_name'] 				= $this->input->post('table_name');
			
			$this->session->set_userdata($arr);
		}	
		
		
		//echo  $this->session->userdata('table_name');
		
		
		$data['url_img']			= $this->url_img;	
		$data['template_paht'] 		= $this->template_paht;
        $data['title'] 				= $this->title;
		$data['menu']				= $this->menu;
       	$data['slide']				= $this->slide;
		$data['category']			= $this->category;
		$data['index_text']			= $this->index_text;
        $data['sub_category']		= $this->sub_category;
		
		$data['link_home']			= $this->link_home;
		$data['color_footer']		= $this->color_footer;
		
		$data['template_paht'] 		= $this->template_paht;
		$data['template_menu']		= $this->template_menu;
		$data['template_footer']	= $this->template_footer;
		$data['copy_right']			= $this->copy_right;
		$data['template']			= $this->template;
		$data['css_js']				= 'category.php';
		$data['content'] 			= $this->category_view."/site/detial_player_view.php";
		
		
		
		$data['footer_view_box1']	= "";
		$data['footer_view_box2']	= "";
		$data['footer_view_box3']	= "";
		$data['footer_view_box4']	= "";
		
		$data['footer_view_box1'] = @$this->footer_colum_1();
		$data['footer_view_box2'] = @$this->footer_colum_2();
		$data['footer_view_box3'] = @$this->footer_colum_3();
		$data['footer_view_box4'] = @$this->footer_colum_4();
		
			
		$data['style_bg']				= $this->style_bg;	
		$data['get_club_id']			= $this->get_club_id;
		$data['url_site']				= $this->get_url_site;
		
		
		
		$this->load->view($this->category_view.'/layout/layout.php',$data);
		
	}
	



	
	public function coach(){
		
		
		//$data['url_img']			= $this->url_img;	
		//$data['template_paht'] 		= $this->template_paht;
        //$data['title'] 				= $this->title;
		//$data['menu']				= $this->menu;
       	//$data['slide']				= $this->slide;
		//$data['category']			= $this->category;
		//$data['index_text']			= $this->index_text;
       // $data['sub_category']		= $this->sub_category;
		
		//$data['link_home']			= $this->link_home;
		//$data['color_footer']		= $this->color_footer;
		
		$data['template_paht'] 		= $this->asset_matchdayone;
		//$data['template_menu']		= $this->template_menu;
		//$data['template_footer']	= $this->template_footer;
		//$data['copy_right']			= $this->copy_right;
		//$data['template']			= $this->template;
		//$data['css_js']				= 'category.php';
		//$data['content'] 			= $this->category_view."/site/coach_view.php";
		
		
		
		
	
		
		//$data['style_bg']				= $this->style_bg;	
		$data['get_club_id']			= $this->get_club_id;
		$data['url_site']				= $this->get_url_site;
		
		
		
		//$this->load->view($this->category_view.'/layout/layout.php',$data);
		//$this->load->view($this->category_view,$data);
		//$this->load->view($this->template."/site/coach_view");
		
		//echo $this->asset_matchdayone;
		
		$this->load->view("matchdayone/coach_view",$data);
		
	}
    
	
	
	
	public function staff(){
		
		
		$data['url_img']			= $this->url_img;	
		$data['template_paht'] 		= $this->template_paht;
        $data['title'] 				= $this->title;
		
		
		$data['link_home']			= $this->link_home;
		$data['color_footer']		= $this->color_footer;
		
		$data['template_paht'] 		= $this->template_paht;
		$data['template_menu']		= $this->template_menu;
		$data['template_footer']	= $this->template_footer;
		$data['copy_right']			= $this->copy_right;
		$data['template']			= $this->template;
		
		
		
		
		
		
		$data['style_bg']				= $this->style_bg;	
		$data['get_club_id']			= $this->get_club_id;
		$data['url_site']				= $this->get_url_site;
		
		$this->load->view("matchdayone/staff_view",$data);
		//$this->load->view($this->category_view,$data);
			
	}



	public function category_single(){
		if(@$_POST){
			$arr['section'] 				= $this->input->post('section');
		    $arr['section_id'] 				= $this->input->post('section_id');
			$arr['section_menu_name'] 		= $this->input->post('section_menu_name');
			$arr['section_menu_head_name']	= $this->input->post('section_menu_head_name');
			$arr['section_id_category']		= $this->input->post('section_id_category');
			$arr['id_detail']				= $this->input->post('section_article_page');
			//$arr['id_detail']               = $this->input->post('id_detail');				
		    $this->session->set_userdata($arr);
		}	
		
		//echo  $this->session->userdata('id_detail');
		 //echo  $this->session->userdata('section_id');
		 //echo  $this->session->userdata('section_menu_name');
		
		 //echo  $this->session->userdata('section_article_page');
		//exit;
		
		$this->articel 				= $this->get_model->get_category_view();
		
		$this->article_name			= $this->get_model->get_detail_view()->article_name;
		$this->article_image		= $this->get_model->get_detail_view()->article_image;
		$this->article_detail		= $this->get_model->get_detail_view()->article_detail;
		
		
		
		//print_r($articel);
		$data['article_name']		= $this->article_name;
		$data['article_image']		= $this->article_image;
		$data['article_detail']		= $this->article_detail;
		$data['url_img']			= $this->url_img;	
		$data['template_paht'] 		= $this->template_paht;
        $data['title'] 				= $this->title;
		$data['menu']				= $this->menu;
       	$data['slide']				= $this->slide;
		$data['category']			= $this->category;
		$data['index_text']			= $this->index_text;
        $data['sub_category']		= $this->sub_category;
		$data['articel']			= $this->articel;
		$data['link_home']			= $this->link_home;
		$data['color_footer']			= $this->color_footer;
		
		$data['template_paht'] 		= $this->template_paht;
		$data['template_menu']		= $this->template_menu;
		$data['template_footer']	= $this->template_footer;
		$data['copy_right']			= $this->copy_right;
		$data['template']			= $this->template;
		$data['css_js']				= 'category.php';
		$data['content'] 			= $this->category_view."/site/category_single_view.php";
		
				
		$data['footer_view_box1']	= "";
		$data['footer_view_box2']	= "";
		$data['footer_view_box3']	= "";
		$data['footer_view_box4']	= "";
		
		$data['footer_view_box1'] = @$this->footer_colum_1();
		$data['footer_view_box2'] = @$this->footer_colum_2();
		$data['footer_view_box3'] = @$this->footer_colum_3();
		$data['footer_view_box4'] = @$this->footer_colum_4();
		
			
		$data['get_club_id']			= $this->get_club_id;
		$data['url_site']				= $this->get_url_site;
		
		$data['style_bg']				= $this->style_bg;	
		
		
		if($this->control_match == "Yes"){
			$this->load->view("files_system/match/match",$data);
		}
		
		$this->load->view($this->category_view.'/layout/layout.php',$data);
		//$this->load->view($this->category_view,$data);
	}


	//*******************//
	//Start Section Footer
	//*******************//
	
	public function footer_colum_1(){
		
		// Start Footer Section 1
		$this->gen_footer_box1_category		= $this->get_model->gen_footer_box1_category();
		
		if($this->gen_footer_box1_category){
			 $sec1_category 		= $this->gen_footer_box1_category->id;
				
			$arr['foot_1']	= $sec1_category;
			$this->session->set_userdata($arr);
					
			if($this->get_model->gen_footer_box1_subcategory($sec1_category)){
				 $sec1_subcategory = $this->get_model->gen_footer_box1_subcategory($sec1_category)->id;
					
				if($this->get_model->get_section1($sec1_category,$sec1_subcategory)){
								
					$data['footer_view_box1']	= $this->get_model->get_section1($sec1_category,$sec1_subcategory);
				}else{
					
				}			
			}
		}
		// End Footer Section 1
		return $data['footer_view_box1'];
	}
	
	public function footer_colum_2(){
		// Start Footer Section 2
		$this->gen_footer_box2_category		= $this->get_model->gen_footer_box2_category();
		if($this->gen_footer_box2_category){
			 $sec2_category 		= $this->gen_footer_box2_category->id;
				
			$arr['foot_2']	= $sec2_category;
			$this->session->set_userdata($arr);
					
			if($this->get_model->gen_footer_box2_subcategory($sec2_category)){
				 $sec2_subcategory = $this->get_model->gen_footer_box2_subcategory($sec2_category)->id;
							
				if($this->get_model->get_section2($sec2_category,$sec2_subcategory)){			
					$data['footer_view_box2']	= $this->get_model->get_section2($sec2_category,$sec2_subcategory);
					
				}			
			}	
		}
		// End Footer Section 2
		
		return $data['footer_view_box2'];
	}
	
	public function footer_colum_3(){
		
		// Start Footer Section 3
		$this->gen_footer_box3_category		= $this->get_model->gen_footer_box3_category();
		if($this->gen_footer_box3_category){
			 $sec3_category 		= $this->gen_footer_box3_category->id;
			 $arr['foot_3']	= $sec3_category;
			 $this->session->set_userdata($arr);
			 		
			if($this->get_model->gen_footer_box3_subcategory($sec3_category)){
				 $sec3_subcategory = $this->get_model->gen_footer_box3_subcategory($sec3_category)->id;
							
				if($this->get_model->get_section3($sec3_category,$sec3_subcategory)){			
					$data['footer_view_box3']	= $this->get_model->get_section3($sec3_category,$sec3_subcategory);
					
				}			
			}	
		}
		// End Footer Section 3
		
		return $data['footer_view_box3'];
	}
	
	public function footer_colum_4(){
		
		// Start Footer Section 4
		$this->gen_footer_box4_category		= $this->get_model->gen_footer_box4_category();
		if($this->gen_footer_box4_category){
			 $sec4_category 		= $this->gen_footer_box4_category->id;
			 		
			if($this->get_model->gen_footer_box4_subcategory($sec4_category)){
				 $sec4_subcategory = $this->get_model->gen_footer_box4_subcategory($sec4_category)->id;
							
				if($this->get_model->get_section4($sec4_category,$sec4_subcategory)){			
					$data['footer_view_box4']	= $this->get_model->get_section4($sec4_category,$sec4_subcategory);
				}			
			}	
		}
		// End Footer Section 4
		
		return $data['footer_view_box4'];
	}
	
	//*******************//
	//End Section Footer
	//*******************//
	
	
	
	
	//**************************//
	//Start Section Check Sessions
	//**************************//
	
	
	public function check_article_hitz($id2){
		
		//print_r($this->session->userdata('mox123'));
		
	 	//$arr['id_detail']               = $this->input->post('id_detail');
	 	
	 	//print_r($this->session->all_userdata());
	 	//echo $this->session->userdata('id_detail');exit;
	 	
	 	// $temp = array();
		
		// if($this->session->userdata('sessCountPage')){
			// $temp = $this->session->userdata("sessCountPage");
// 			
			// if(!isset($temp[$id2])){
				// $this->db->query("update article set hitz = (hitz + 1) where id = ".$id2);
				// $temp[$id2] = $id2;
				// $this->session->set_userdata('sessCountPage',$temp);
			// }
		// }else{
			// $temp[$id2] = $id2;
			// $this->session->set_userdata('sessCountPage',$temp);
		// }
		
		
		
		
		
		if($this->session->userdata('arrticle_view')){
			$array = $this->session->userdata('arrticle_view');
			if(in_array($id2, $array)){
				//echo "Aleready seen ID : $id2   ";
			}
			else{
				$this->db->query("update article set hitz = (hitz + 1) where id = ".$id2);
				$array[] = $id2;
				$this->session->set_userdata('arrticle_view',$array);
				
				
			}
			
		}

		$this->db->select('hitz');
		$this->db->where('id',$id2);
		$query = $this->db->get('article');
		$total_hitz		=	$query->row()->hitz;
		$data['hitz'] = $total_hitz;
		return  $data['hitz'];
		
				
	//	print_r($this->session->userdata('arrticle_view'));
		// print_r($_SESSION);
		
		
		// print_r($this->session->userdata('sessCountPage'));
	 	//exit();
		
		
		
		
		
		// $query 		= 	$this->db->get('cms_sessions');
		// $num 		= 	$query->num_rows();
// 		
// 		
// 		
		// $this->db->select('hitz');
		// $this->db->where('id',$id);
		// $query = $this->db->get('article');
		// $total_hitz		=	$query->row()->hitz;
// 		
		// echo "$id";
		// if($num==0){
// 		
			// $data['hitz']	=	++$total_hitz;
			// $this->db->update('article',$data);
// 			
// 			
		// }else{
			// $data['hitz'] = $total_hitz;
		// }
// 		
		// return  $data['hitz'];
		
	}
	
	//**************************//
	//End Section Check Sessions
	//**************************//
	

	
	

}
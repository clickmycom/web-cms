<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends CI_Controller {
    
    public function __construct()
    {
        parent::__construct();
        $this->cart_view 			= 'templates/'.$this->get_model->get_template()->template.'/cart_view.php';
        $this->template_paht 		= base_url().'public/templates/'.$this->get_model->get_template()->template;
		$this->index_text			= $this->get_model->get_template()->index_text;
        $this->title 				= $this->get_model->get_template()->site_name;
		$this->menu 				= $this->get_model->get_mainmenu();
		$this->slide				= $this->get_model->get_image_url();
		$this->category				= $this->get_model->check_status_category(); 
    }
    
	public function index()
	{
	    $data['template_paht'] 		= $this->template_paht;
        $data['title'] 				= $this->title;
		$data['menu']				= $this->menu;
       	$data['slide']				= $this->slide;
		$data['category']			= $this->category;
		$data['index_text']			= $this->index_text;

        $data['cart_table'] = $this->router->class;

	   	$this->load->view($data['cart_table'],$data);
	}
    function calcart(){
        //show cart after change qty
        $this->load->view($this->router->method);
    }
    function add2cart(){
            
        //id is article.id
        //qty is select qty
        //price is article price [article_price.price]
        //name is article.name
        //options if have  option input as array
        
        $data['id'] = 1;
        $data['qty'] = 1;
        $data['price'] = 1;
        $data['name'] = 'Apple';
        $data['options'] = array('Size' => 'L', 'Color' => 'Red');
        
        $this->cart->insert($data);
    }
    function updateCart(){
            
        $data['qty'] = $this->input->post('qty');
        $data['rowid'] = $this->input->post('rowid');
        
        $this->cart->update($data);
    }
	
	
	
	
	
    
}
